#version 440 core

struct Ambient 
{
    float intensity;
};

struct Diffuse 
{
    float minLighting;
    float intensity;
    vec3 direction;
    vec3 color;
};

struct Specular 
{
    uint powder;
    float intensity;
    vec3 color;    
};

struct Fog
{
    float minDistance;
    float maxDistance;
    vec3 color;
};

struct Material
{
    float specularPowder;
    float specularIntensity;
    vec3 specularColor;
};

//Out
out vec4 oFragColor;

//In
in vec2 ioTextRectSize;
in vec3 ioNormal;
in vec2 ioUv;
in vec3 ioFragColorPos;
in vec3 ioSurfaceToSun;
in vec3 ioSurfaceToView;


//Textures
uniform sampler2D textureAtlas;
uniform sampler2D textureSpecularAtlas;

//Lighting
uniform Diffuse uDiffuse;
uniform Specular uSpecular;
uniform Ambient uAmbient;
uniform vec3 uSunPosition;
uniform vec3 uViewPosition;

//Fog
uniform Fog uFog;

//Material
uniform Material uMaterial;

vec3 calcSpecular( vec3 normal,  float specularMapK )
{
    //vec3 viewDirection = normalize( uViewPosition - ioFragColorPos );
    //float reflectDirection = dot( lightDirection, normal );  
    //float specular = max( reflectDirection, 0.0 );    
    //specular = pow( specular, 1024 );
    //specular *= uSpecular.intensity + uMaterial.specularIntensity;
    //specular *= specularMapK;
    vec3 surfaceToSun = normalize( ioSurfaceToSun );
    vec3 surfaceToView = normalize( ioSurfaceToView );
    vec3 halfVector = normalize(surfaceToView + surfaceToSun);
    float k = pow( dot(normal, halfVector), uMaterial.specularPowder );    
    k *= uSpecular.intensity + uMaterial.specularIntensity;

    return ( uMaterial.specularColor * uSpecular.color ) * k;
}

float calcDiffuse( vec3 normal, float specularMapK )
{
    float dotProduct = dot( normal, normalize( uDiffuse.direction ) );
    dotProduct *= uDiffuse.intensity;
    dotProduct *= specularMapK;
    return max( uDiffuse.minLighting, dotProduct );
}

vec2 calcUv()
{
    return vec2( ioTextRectSize.x + ioUv.x, ioTextRectSize.y + ioUv.y );
}

float getFogFactor( float d )
{
    if ( d >= uFog.maxDistance ) return 1;
    if ( d <= uFog.minDistance ) return 0;

    return 1 - ( uFog.maxDistance - d ) / ( uFog.maxDistance - uFog.minDistance );
}

vec3 calcFog( vec3 color )
{
    float d = distance( uViewPosition, ioFragColorPos );
    return mix( color, uFog.color, getFogFactor( d ) );
}

float attenuation( float d, float l, float q )
{
    return 1.f / ( 1.f + l * d + q * ( d * d ) );
}

void main()
{
    vec2 uv = calcUv() * vec2( 1.f, 1.f );
    float specularMapK = texture( textureSpecularAtlas, uv ).r;

    vec3 normal = normalize( ioNormal );

    vec3 ambient = vec3( uAmbient.intensity );
    vec3 diffuse = uDiffuse.color * calcDiffuse( normal, specularMapK );
    vec3 specular = calcSpecular( normal, specularMapK );

    oFragColor = texture( textureAtlas, uv );
    oFragColor.rgb *= ambient + diffuse + specular;
    oFragColor.rgb = calcFog( oFragColor.rgb );
}