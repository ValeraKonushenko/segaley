#version 440 core
layout ( location = 0 ) in vec3 aPos;
layout ( location = 1 ) in vec3 aNormal;
layout ( location = 2 ) in vec2 aTextRectSize;
layout ( location = 3 ) in vec2 aUv;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

out vec3 ioTexCoords;

void main()
{
    ioTexCoords = aPos;
    vec4 pos = uProjectionMatrix * mat4( mat3( uViewMatrix ) ) * vec4( aPos, 1.f );
    gl_Position = pos.xyww;
}  