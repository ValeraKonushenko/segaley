#include "Program.h"
#include "DebugActions.h"

#include <memory>
#include <stdexcept>
#include <sstream>
#include <Windows.h>

int main()
{
    try
    {
        DebugActions::launch();        
        auto program = std::make_unique< Program >();
        program->launch();
    }
    catch( const std::runtime_error& ex )
    {
        std::stringstream ss;
        ss << "Has occurred STD error:" << ex.what() << std::endl;
        MessageBoxA( nullptr, ss.str().c_str(), "Fatal error", MB_OK );
    }
    catch( const std::exception& ex )
    {
        std::stringstream ss;
        ss << "Has occurred STD error: " << ex.what() << std::endl;
        MessageBoxA( nullptr, ss.str().c_str(), "Fatal error", MB_OK );
    }
    catch( ... )
    {
        std::stringstream ss;
        ss << "Has occurred undefined error" << std::endl;
        MessageBoxA( nullptr, ss.str().c_str(), "Fatal error", MB_OK );
    }

    return 0;
}