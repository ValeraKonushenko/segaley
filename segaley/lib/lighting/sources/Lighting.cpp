#include "Lighting.h"

#include "Thrower.h"
#include "Settings.h"

void Lighting::loadSettings() noexcept( false )
{
	auto settings = Settings::instance();

	ambient = settings->graphics.ambient;
	diffuse = settings->graphics.diffuse;
	specular = settings->graphics.specular;

	fog = settings->graphics.fog;
	sun = settings->graphics.sun;
}

void Lighting::saveSettings() noexcept( false )
{
	MAKE_THROW( "not finished" );
}