#pragma once
#include "glm/glm.hpp"
#include "Model.h"
#include "Color.h"

class Lighting final : public Model
{
public:
	struct Ambient final
	{
		float intensity = 0.0f;
	} ambient;

	struct Diffuse final
	{
		float minLighting = 0.0f;
		float intensity = 0.0f;
		glm::vec3 direction = glm::vec3( 0.f );
		RGBf color;
	} diffuse;

	struct Specular final
	{
		size_t pow = 0u;
		float intensity = .0f;
		RGBf color;
	} specular;

	struct Fog final
	{
		float minDistance = 0.f;
		float maxDistance = 0.f;
		RGBf color;
	} fog;

	struct Sun final 
	{
		glm::vec3 position = glm::vec3( 0.f );
	} sun;

	void loadSettings() noexcept( false ) override;
	void saveSettings() noexcept( false ) override;
};