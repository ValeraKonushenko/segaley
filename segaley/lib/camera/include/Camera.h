#pragma once

#include "WindowSize.h"
#include "Keyboard.h"
#include "Model.h"

#include <GLM/glm.hpp>

struct RotateTransormation
{
	float yaw = 0.f;
	float pitch = 0.f;
};

class Camera final : public Model
{
public:
	struct Location
	{
		RotateTransormation rotate;
		glm::fvec3 position = glm::fvec3( 0.f, 0.f, 0.f );
	};

	struct Property
	{
		Location cameraLocation;
		float fow = 0.f;
		float translateSmooth = 0.f;
		float translateSpeed = 0.f;
		float translateTopSpeed = 0.f;
		float translateBoostSpeed = 0.f;
		float translateSlowSpeed = 0.f;
		float currentSpeed = 0.f;
		Location defaultLocation;
	};

	struct ContolKeys
	{
		Keyboard::Key boostMode = Keyboard::Key::LShift;
		Keyboard::Key slowMode = Keyboard::Key::LControl;
		Keyboard::Key toRight = Keyboard::Key::D;
		Keyboard::Key toLeft = Keyboard::Key::A;
		Keyboard::Key toUp = Keyboard::Key::Space;
		Keyboard::Key toDown = Keyboard::Key::C;
		Keyboard::Key toForward = Keyboard::Key::W;
		Keyboard::Key toBackward = Keyboard::Key::S;
		Keyboard::Key reset = Keyboard::Key::R;
	};

public:
	Camera() = default;
	~Camera() = default;
	Camera( const Camera& ) = default;
	Camera( Camera&& ) = default;
	Camera& operator=( const Camera& ) = default;
	Camera& operator=( Camera&& ) = default;

	glm::mat4 getViewMatrix( const WindowSize& wndSize ) const;
	glm::mat4 getPerspective( float ratio ) const;
	
	void setPosition( glm::fvec3 value );
	glm::fvec3 getPosition() const;
	void move( glm::fvec3 value );
	void moveForward( float offset );
	void moveBackward( float offset );
	void moveRight( float offset );
	void moveLeft( float offset );
	void moveUp( float offset );
	void moveDown( float offset );

	void setProperty( Property property );
	Property getProperty() const;
	RotateTransormation getRotate() const;
	ContolKeys& getContolKeys();
	void setContolKeys( const ContolKeys& keys );
	const ContolKeys& getContolKeys() const ;

	void setYaw( float rx );
	void setPitch( float ry );
	void yaw( float rx );
	void pitch( float ry );
	float getYaw() const;
	float getPitch() const;

	void resetLocation();

	glm::vec3 getForward() const;
	glm::vec3 getUp() const;

	float getFow() const;
	void setFow( float radian ) noexcept( false );
	void setTranslateSmooth( float smooth );
	float getTranslateSmooth() const;
	void setTranslateSpeed( float speed );
	float getTranslateSpeed() const;
	void setTranslateTopSpeed( float topSpeed );
	float getTranslateTopSpeed() const;
	void setTranslateBoostSpeed( float boostSpeed );
	float getTranslateBoostSpeed() const;
	void setTranslateSlowSpeed( float slowSpeed );
	float getTranslateSlowSpeed() const;
	void setLocation( const Location& location ) noexcept( false );
	Location getLocation() const;
	void setDefaultLocation( const Location& location );
	Location getDefaultLocation() const;

	void update();
	void loadSettings() noexcept( false ) override;
	void saveSettings() noexcept( false ) override;


private:
	void updateControl() noexcept( false );
	void updateImpulse();
	void updateCurrentSpeed();

	bool checkMaxSpeed( glm::vec3 vec );

	ContolKeys keys_;
	Property property_;
	glm::vec3 impulse_ = glm::vec3( 0.f, 0.f, 0.f );
};