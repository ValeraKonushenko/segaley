#include "Camera.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/glm.hpp"
#include "Settings.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <sstream>

glm::mat4 Camera::getViewMatrix( const WindowSize& wndSize ) const
{
	auto view = glm::mat4( 1.0f );

	view = glm::rotate( view, getYaw(), glm::vec3( 1.f, 0.f, 0.f ) );
	view = glm::rotate( view, getPitch(), glm::vec3( 0.f, 1.f, 0.f ) );
	view = glm::translate( view, property_.cameraLocation.position );
	
	return view;
}

glm::mat4 Camera::getPerspective( float ratio ) const
{
	static auto settings = Settings::instance();
	return glm::perspective( getFow(), ratio, settings->general.viewBox.minDeep, settings->general.viewBox.maxDeep );
}

void Camera::setPosition( glm::fvec3 value )
{
	property_.cameraLocation.position = -value;
}

glm::fvec3 Camera::getPosition() const
{
	return -property_.cameraLocation.position;
}

void Camera::move( glm::fvec3 value )
{
	if( !checkMaxSpeed( impulse_ + value ) )
		impulse_ += value;
}

void Camera::moveForward( float offset )
{	
	auto tmp = offset * getForward();
	if( !checkMaxSpeed( tmp ) )
		move( tmp );
}

void Camera::moveBackward( float offset )
{	
	auto tmp = offset * -getForward();
	if ( !checkMaxSpeed( tmp ) )
		move( tmp );
}

void Camera::moveRight( float offset )
{
	auto tmp = offset * glm::cross( getForward(), getUp() );
	if ( !checkMaxSpeed( tmp ) )
		move( tmp );
}

void Camera::moveLeft( float offset )
{
	auto tmp = offset * -glm::cross( getForward(), getUp() );
	if ( !checkMaxSpeed( tmp ) )
		move( tmp );
}

void Camera::moveUp( float offset )
{
	auto tmp = offset * -getUp();	
	if ( !checkMaxSpeed( tmp ) )
		move( tmp );
}

void Camera::moveDown( float offset )
{
	auto tmp = offset * getUp();
	if ( !checkMaxSpeed( tmp ) )
		move( tmp );
}

RotateTransormation Camera::getRotate() const
{
	return property_.cameraLocation.rotate;
}

Camera::ContolKeys& Camera::getContolKeys()
{
	return keys_;
}

void Camera::setContolKeys( const Camera::ContolKeys& keys )
{
	keys_ = keys;
}

const Camera::ContolKeys& Camera::getContolKeys() const
{
	return keys_;
}

void Camera::setYaw( float rx )
{
	static const auto pi2 = static_cast< float >( M_PI_2 );
	property_.cameraLocation.rotate.yaw = rx;

	if ( property_.cameraLocation.rotate.yaw > pi2 )
		property_.cameraLocation.rotate.yaw = pi2;
	if ( property_.cameraLocation.rotate.yaw < -pi2 )
		property_.cameraLocation.rotate.yaw = -pi2;
}

void Camera::setPitch( float ry )
{
	static const auto pi2 = static_cast< float >( M_PI ) * 2.f;

	property_.cameraLocation.rotate.pitch = ry;
	if( property_.cameraLocation.rotate.pitch > pi2 )
		property_.cameraLocation.rotate.pitch = 0.f;
	if ( property_.cameraLocation.rotate.pitch < 0.f )
		property_.cameraLocation.rotate.pitch = pi2;
}

void Camera::yaw( float rx )
{
	setYaw( rx + getYaw() );
}

void Camera::pitch( float ry )
{
	setPitch( ry + getPitch() );
}

float Camera::getYaw() const
{
	return property_.cameraLocation.rotate.yaw;
}

float Camera::getPitch() const
{
	return property_.cameraLocation.rotate.pitch;
}

void Camera::resetLocation()
{
	setPosition( property_.defaultLocation.position );
	setYaw( property_.defaultLocation.rotate.yaw );
	setPitch( property_.defaultLocation.rotate.pitch );
	impulse_ = glm::vec3( 0.f );
}

void Camera::setProperty( Property property_ )
{
	property_ = property_;
}

Camera::Property Camera::getProperty() const
{
	auto tmp = property_;
	tmp.cameraLocation.position = -tmp.cameraLocation.position;
	return tmp;
}

glm::vec3 Camera::getForward() const
{
	const auto& r = property_.cameraLocation.rotate;
	return {
		-sin( r.pitch ) * cos( r.yaw ),
		sin( r.yaw ),
		cos( r.pitch ) * cos( r.yaw )
	};
}

glm::vec3 Camera::getUp() const
{
	return {
		0.f,
		1.f,
		0.f
	};
}

float Camera::getFow() const
{
	return property_.fow;
}

void Camera::setFow( float radian ) noexcept( false )
{
	static constexpr float min = glm::radians( 45.f );
	static constexpr float max = glm::radians( 160.f );
	if ( radian < min || radian > max )
	{
		std::stringstream ss;
		ss << "FOW can't be less then "
			<< min << "rad (" << glm::degrees( min ) << "deg)"
			<< " or greater then "
			<< max << "rad (" << glm::degrees( max ) << "deg)";
		MAKE_THROW( ss.str() );
	}

	property_.fow = radian;
}

void Camera::setTranslateSmooth( float smooth )
{
	property_.translateSmooth = smooth;
}

float Camera::getTranslateSmooth() const
{
	return property_.translateSmooth;
}

void Camera::setTranslateSpeed( float speed )
{
	property_.translateSpeed = speed;
}

float Camera::getTranslateSpeed() const
{
	return property_.translateSpeed;
}

void Camera::setTranslateTopSpeed( float topSpeed )
{
	property_.translateTopSpeed = topSpeed;
}

float Camera::getTranslateTopSpeed() const
{
	return property_.translateTopSpeed;
}

void Camera::setTranslateBoostSpeed( float boostSpeed )
{
	property_.translateBoostSpeed = boostSpeed;
}

float Camera::getTranslateBoostSpeed() const
{
	return property_.translateBoostSpeed;
}

void Camera::setTranslateSlowSpeed( float slowSpeed )
{
	property_.translateSlowSpeed = slowSpeed;
}

float Camera::getTranslateSlowSpeed() const
{
	return property_.translateSlowSpeed;
}

void Camera::update()
{
	updateControl();
	updateImpulse();
	updateCurrentSpeed();
}

void Camera::loadSettings() noexcept( false )
{
	auto settings = Settings::instance();
	setFow( settings->camera.fow );
	setTranslateSmooth( settings->camera.translateSmooth );
	setTranslateSpeed( settings->camera.translateSpeed );
	setTranslateTopSpeed( settings->camera.translateTopSpeed );
	setTranslateBoostSpeed( settings->camera.translateBoostSpeed );
	setTranslateSlowSpeed( settings->camera.translateSlowSpeed );
	setDefaultLocation( settings->camera.defaultLocation );
	setContolKeys( settings->inputDevices.camera );
}

void Camera::saveSettings() noexcept( false )
{
	MAKE_THROW( "not finished" );
}


void Camera::updateControl() noexcept( false )
{
	float multiplier = 1.f;
	if ( Keyboard::isKeyDown( keys_.boostMode ) )
		multiplier = property_.translateBoostSpeed;
	if ( Keyboard::isKeyDown( keys_.slowMode ) )
		multiplier = property_.translateSlowSpeed;
	
	if ( Keyboard::isKeyDown( keys_.toRight ) )
		moveRight( property_.translateSpeed * multiplier );
	if ( Keyboard::isKeyDown( keys_.toLeft ) )
		moveLeft( property_.translateSpeed * multiplier );
	if ( Keyboard::isKeyDown( keys_.toForward ) )
		moveForward( property_.translateSpeed * multiplier );
	if ( Keyboard::isKeyDown( keys_.toBackward ) )
		moveBackward( property_.translateSpeed * multiplier );
	
	if ( Keyboard::isKeyDown( keys_.toUp ) )
		moveUp( property_.translateSpeed * multiplier );
	if ( Keyboard::isKeyDown( keys_.toDown ) )
		moveDown( property_.translateSpeed * multiplier );

	if ( Keyboard::isKeyDown( keys_.reset ) )
	{
		resetLocation();
	}	
}

void Camera::updateImpulse()
{
	property_.cameraLocation.position += impulse_;
	impulse_ *= glm::vec3( property_.translateSmooth );
}

void Camera::updateCurrentSpeed()
{
	property_.currentSpeed = glm::length( impulse_ );
}

bool Camera::checkMaxSpeed( glm::vec3 vec )
{
	if ( glm::length( impulse_ ) >= property_.translateTopSpeed )
		return true;

	return false;
}

void Camera::setLocation( const Location& location ) noexcept( false )
{
	setPosition( location.position );
	setPitch( location.rotate.pitch );
	setYaw( location.rotate.yaw );
}

Camera::Location Camera::getLocation() const
{
	return property_.cameraLocation;
}

void Camera::setDefaultLocation( const Location& location )
{
	property_.defaultLocation = location;
}

Camera::Location Camera::getDefaultLocation() const
{
	return property_.defaultLocation;
}
