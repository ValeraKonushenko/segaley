#include "BaseReader.h"

#include "boost/algorithm/string/trim.hpp"

namespace objLoader
{

void BaseReader::readDataFromSource( const std::vector< std::string >& data, DataType dataType )
{
	for ( const auto& row : data )
		if ( BaseReader::defineTypeOfData( row ) == dataType )
			source_.emplace_back( row );
}

DataType BaseReader::defineTypeOfData( std::string_view row )
{
	//Can't be less the 2 chars because max len. of data-sign is 2 symbols.
	//E.g.: vt or vn
	auto substr = row.substr( 0u, 2u );
	if ( substr.front() == '#' )
		return DataType::None;

	auto type = std::string( substr.data(), substr.size() );
	boost::trim( type );

	if ( type == "o" )
		return DataType::ObjectName;
	if ( type == "v" )
		return DataType::Vertex;
	if ( type == "vt" )
		return DataType::TextureVertices;
	if ( type == "vn" )
		return DataType::VertexNormals;
	if ( type == "s" )
		return DataType::Smoothing;
	if ( type == "f" )
		return DataType::Face;

	return DataType::None;
}

void BaseReader::push( const std::string& row )
{
	source_.emplace_back( row );
}

size_t BaseReader::countOfNotParsedData() const
{
	return source_.size();
}

void BaseReader::clearSourceData()
{
	source_.clear();
}

bool BaseReader::isEmpty() const
{
	return source_.empty();
}

};