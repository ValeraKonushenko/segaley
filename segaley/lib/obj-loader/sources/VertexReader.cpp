#include "VertexReader.h"

#include "ObjLoader.h"

#include <regex>

namespace objLoader
{

const size_t VertexReader::rightCountOfPoints = 3u;

void VertexReader::readDataFromSource( const std::vector< std::string >& data )
{
	BaseReader::readDataFromSource( data, DataType::Vertex );
}

void VertexReader::process() noexcept( false )
{
	//Check only one time. Before, this line was at the for. 
	//This line inside the loop had a BIG impact on a performance ~ x90.
	VertexReader::throwIfInvalidVertexPeaces( source_.front() );
	for ( auto& data : source_ )
	{
		vertices_.emplace_back( getVertexFromRow( data ) );
	}
}

void VertexReader::clear()
{
	source_.clear();
	vertices_.clear();
}

void VertexReader::moveVerticesTo( std::vector<glm::vec3>& destination )
{
	destination.clear();
	destination = std::move( vertices_ );
}

const std::vector< glm::vec3 >& VertexReader::data() const
{
	return vertices_;
}

std::string_view VertexReader::getStringByErrorCode( ErrorCode errorCode )
{
	switch ( errorCode )
	{
	case objLoader::VertexReader::ErrorCode::InvalidCountOfVertiesPoints:
		return "Was got error count of vertices";
		break;
	}

	return "Invalid error code";
}

void VertexReader::throwIfInvalidVertexPeaces( const std::string& data ) noexcept( false )
{
	std::regex regex( R"(-?\d+\.\d+)" );
	const std::ptrdiff_t matchCount = std::distance(
		std::sregex_iterator( data.begin(), data.end(), regex ),
		std::sregex_iterator() );

	if ( matchCount != VertexReader::rightCountOfPoints )
		throw ErrorCode::InvalidCountOfVertiesPoints;
}

glm::vec3 VertexReader::getVertexFromRow( const std::string& row ) noexcept( false )
{
	glm::vec3 vertex;
	
	std::string matches = "1234567890-+";
	auto offset = row.find_first_of( matches );
	std::stringstream ss( row.substr( offset ) );

	ss >> vertex.x;
	ss >> vertex.y;
	ss >> vertex.z;
	
	return std::move( vertex );
}

};