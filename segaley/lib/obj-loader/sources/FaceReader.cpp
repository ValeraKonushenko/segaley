#include "FaceReader.h"

#include "ObjLoader.h"

#include <regex>

namespace objLoader
{

const size_t FaceReader::Face::rightCountOfPoint = 3u;

std::string_view FaceReader::getStringByErrorCode( ErrorCode errorCode )
{
	switch ( errorCode )
	{
	case FaceReader::ErrorCode::InvalidCountOfFacesPoints:
		return "Invalid count of faces' points. Maybe you forget to import model with a flag "
			   "'triangulate faces'. Also can be problem with incorrect export without normals or UVs.";
		break;
	case FaceReader::ErrorCode::TheEndOfLineWasMetPrematurely:
		return "Invalid object-data. The end of the line was met prematurely. The problem can be bound"
			   " with invalid export properties";
		break;
	}

	return "Invalid error code.";
}

void FaceReader::readDataFromSource( const std::vector< std::string >& data )
{
	BaseReader::readDataFromSource( data, DataType::Face );
}

void FaceReader::process() noexcept( false )
{
	//Check only one time. Before, this line was at the for. 
	//This line inside the loop had a BIG impact on a performance ~ x90.
	FaceReader::throwIfInvalidFacePeaces( source_.front() );
	for ( auto& data : source_ )
	{
		faces_.emplace_back( getFaceFromRow( data ) );
	}
}

void FaceReader::clear()
{
	source_.clear();
	faces_.clear();
}

void FaceReader::moveFacesTo( std::vector<Face>& destination )
{
	destination.clear();
	destination = std::move( faces_ );
}

const std::vector< FaceReader::Face >& FaceReader::data() const
{
	return faces_;
}

void FaceReader::throwIfInvalidFacePeaces( const std::string& data ) noexcept( false )
{
	//f  v1/vt1/vn1   v2/vt2/vn2   v3/vt3/vn3
	std::regex regex( R"(\d+\/\d+\/\d+)" );
	//                    ^^^^^^^^^^^^
	//                    v  / vt / vn
	const std::ptrdiff_t matchCount = std::distance(
		std::sregex_iterator( data.begin(), data.end(), regex ),
		std::sregex_iterator() );

	if ( matchCount != Face::rightCountOfPoint )
		throw ErrorCode::InvalidCountOfFacesPoints;
}

FaceReader::Face FaceReader::getFaceFromRow( const std::string& row ) const
{
	auto getOffsetWithNextNumber = []( size_t& offset, std::string_view str ) -> size_t
	{
		while ( isdigit( str[ offset ] ) )
		{
			offset++;
			if ( str[ offset ] == '\0' )
				throw ErrorCode::TheEndOfLineWasMetPrematurely;
		}
		
		while ( !isdigit( str[ offset ] ) )
		{
			offset++;
			if ( str[ offset ] == '\0' )
				throw ErrorCode::TheEndOfLineWasMetPrematurely;
		}

		return offset;
	};

	Face face;
	size_t offset = 0u;
	for ( auto& p : face.point )
	{
		p.v = atoi( row.data() + getOffsetWithNextNumber( offset, row ) );
		p.vt = atoi( row.data() + getOffsetWithNextNumber( offset, row ) );
		p.vn = atoi( row.data() + getOffsetWithNextNumber( offset, row ) );
	}
	return std::move( face );
}

};