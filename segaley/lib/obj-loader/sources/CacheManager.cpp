#include "CacheManager.h"

#include <fstream>

namespace objLoader
{

const std::filesystem::path CacheManager::extension = ".cache";
const std::filesystem::path CacheManager::directory = "cachedObjects";

std::string_view CacheManager::getStringByErrorCode( ErrorCode code )
{
	switch ( code )
	{
	case ErrorCode::CantCreateDirectory:
		return "Can't create a directory for cache storing. Maybe: not enough permissions";
		break;
	case ErrorCode::CantCreateCacheFile:
		return "Can't create a file for cache storing. Maybe: not enough permissions";
		break;
	case ErrorCode::NothingToSave:
		return "Was passed an empty array. Nothing to save";
		break;
	case ErrorCode::FileNotFound:
		return "The path to passed the file name was not found";
		break;
	}

	return "Invalid error code";
}

void CacheManager::saveToCache( const std::vector<VertexData>& data, std::filesystem::path filename ) noexcept( false )
{
	if ( data.empty() )
		throw ErrorCode::NothingToSave;

	createDirectory();

	auto rawData = reinterpret_cast< const char* >( data.data() );

	std::fstream cache( getPathToFile( filename ), std::ios::out | std::ios::binary );
	if ( !cache.is_open() )
		throw ErrorCode::CantCreateCacheFile;

	cache.write( rawData, data.size() * sizeof( VertexData ) );
	cache.close();
}

void CacheManager::saveToCache( const ObjectCollection& data ) noexcept( false )
{
	if ( data.isEmpty() )
		throw ErrorCode::NothingToSave;

	createDirectory( data.getName() );

	for ( size_t i = 0u; i < data.size(); i++ )
	{
		auto object = data.at( i );

		auto rawData = reinterpret_cast< const char* >( object.data().data() );
	
		std::fstream cache( getPathToCollectionFile( data, object.getName() ), std::ios::out | std::ios::binary );
		if ( !cache.is_open() )
			throw ErrorCode::CantCreateCacheFile;
	
		cache.write( rawData, data.at( i ).data().size() * sizeof( VertexData ) );
		cache.close();
	}
}

void CacheManager::loadFromCache( std::vector<VertexData>& data, std::filesystem::path filename ) noexcept( false )
{
	data.clear();

	auto path = getPathToFile( filename );

	std::fstream cache( path, std::ios::in | std::ios::binary );
	if ( !cache.is_open() )
		throw ErrorCode::FileNotFound;

	auto fileSize = std::filesystem::file_size( path );
	auto countOfUnits = fileSize / sizeof( VertexData );
	data.resize( countOfUnits );
	cache.read( reinterpret_cast< char* >( data.data() ), fileSize );
	cache.close();
}

void CacheManager::loadFromCache( ObjectCollection& data ) noexcept( false )
{
	data.clearObjects();

	auto path = getPathToCollection( data.getName() );
	for ( const auto& file : std::filesystem::recursive_directory_iterator( path ) )
	{
		std::fstream cache( file, std::ios::in | std::ios::binary );
		if ( !cache.is_open() )
			throw ErrorCode::FileNotFound;

		auto fileSize = file.file_size();
		auto countOfUnits = fileSize / sizeof( VertexData );
		std::vector< VertexData > rawData( countOfUnits );
		cache.read( reinterpret_cast< char* >( rawData.data() ), fileSize );
		cache.close();

		DrawableObj tmp;
		tmp.setVertices( rawData );
		tmp.setName( file.path().stem().string() );

		data.push( std::move( tmp ) );
	}

}

bool CacheManager::hasInCache( std::filesystem::path filename ) const
{
	auto path = getPathToFile( filename );

	if ( !std::filesystem::exists( path ) )
		return false;

	return true;
}

bool CacheManager::hasCollectionInCache( const std::string& name ) const
{
	auto path = getPathToCollection( name );

	if ( !std::filesystem::exists( path ) )
		return false;

	return true;
}

std::filesystem::path CacheManager::getPathToFile( std::filesystem::path filename ) const
{
	auto path = std::filesystem::current_path();
	path += "\\";
	path += CacheManager::directory;
	path += "\\";
	path += filename.string() + CacheManager::extension.string();

	return path;
}

std::filesystem::path CacheManager::getPathToCollectionFile( const ObjectCollection& data, const std::string& name ) const
{
	auto path = std::filesystem::current_path();
	path += "\\";
	path += CacheManager::directory;
	path += "\\";
	path += data.getName();
	path += "\\";
	path += name + CacheManager::extension.string();

	return path;
}

std::filesystem::path CacheManager::getPathToCollection( const std::string& name ) const
{
	auto path = std::filesystem::current_path();
	path += "\\";
	path += CacheManager::directory;
	path += "\\";
	path += name;

	return path;
}

void CacheManager::createDirectory( std::string internalFolder ) noexcept( false )
{
	namespace fs = std::filesystem;


	auto path = fs::current_path();
	path += "\\";
	path += CacheManager::directory;
	fs::create_directory( path );

	if ( !fs::exists( path ) )
		throw ErrorCode::CantCreateDirectory;

	if ( !internalFolder.empty() )
	{
		path += "\\";
		path += internalFolder;
		fs::create_directory( path );
	}
}

}