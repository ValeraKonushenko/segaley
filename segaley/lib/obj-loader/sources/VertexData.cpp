#include "VertexData.h"

#include <utility>

VertexData::VertexData( glm::fvec3 physics/* = glm::fvec3( 0.f, 0.f, 0.f )*/,
				glm::fvec3 normal/* = glm::fvec3( 0.f, 0.f, 0.f )*/,
				glm::fvec2 textureSize,/* = glm::fvec2( 0.f, 0.f )*/
				glm::fvec2 uv/* = glm::fvec2( 0.f, 0.f )*/
) :
	physics_( physics ),
	normal_( normal ),
	textureSize_( textureSize ),
	uv_( uv )
{
}

size_t VertexData::bsize()
{
	return sizeof( physics_ ) + sizeof( normal_ ) + sizeof( textureSize_ ) + sizeof( uv_ );
}
