#include "ObjectCollection.h"

#include "ObjLoader.h"
#include "Settings.h"
#include "CacheManager.h"
#include "Thrower.h"

void ObjectCollection::load( std::filesystem::path path, bool rewriteCache ) noexcept( false )
{
    setName( path.stem().string() );

    auto& paths = Settings::instance()->paths;
    
    objLoader::CacheManager cacheManager;

    if ( cacheManager.hasCollectionInCache( getName() ) && !rewriteCache )
        cacheManager.loadFromCache( *this );
    else 
    {
        objLoader::ObjLoader loader;
        loader.load( paths.toRawModels + path.string() );
        objects_ = loader.getObjects();
        cacheManager.saveToCache( *this );
    }
}

void ObjectCollection::draw( const DrawData& data ) noexcept( false )
{
    if ( !objects_ )
        return;

    glCullFace( GL_BACK );
    for ( auto& object : objects_.value() )
        object.draw( data );
    glCullFace( GL_FRONT );
}

void ObjectCollection::prepare()
{
    if ( !objects_ )
        return;

    for ( auto& object : objects_.value() )
        object.prepare();
}

void ObjectCollection::setName( const std::string& name )
{
    name_ = name;
}

std::string& ObjectCollection::getName()
{
    return name_;
}

const std::string& ObjectCollection::getName() const
{
    return name_;
}

size_t ObjectCollection::size() const
{
    if ( !objects_ )
        return 0u;

    return objects_->size();
}

DrawableObj& ObjectCollection::at( size_t i ) noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an element from an empty object-list" );

    return objects_->at( i );
}

const DrawableObj& ObjectCollection::at( size_t i ) const noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an element from an empty object-list" );

    return objects_->at( i );
}

DrawableObj& ObjectCollection::operator[]( size_t i ) noexcept( false )
{
    return at( i );
}

const DrawableObj& ObjectCollection::operator[]( size_t i ) const noexcept( false )
{
    return at( i );
}

bool ObjectCollection::isEmpty() const
{
    return !objects_ || objects_->empty();
}

void ObjectCollection::clear()
{
    objects_.reset();
    name_ = "None";
}

void ObjectCollection::clearObjects()
{
    objects_.reset();
}

void ObjectCollection::push( DrawableObj&& obj )
{
    if ( !objects_ )
        objects_ = std::vector< DrawableObj >();

    objects_->emplace_back( obj );
}

std::vector< DrawableObj >::iterator ObjectCollection::begin() noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an end iterator from the empty array" );

    return objects_->begin();
}

std::vector< DrawableObj >::iterator ObjectCollection::end() noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an end iterator from the empty array" );

    return objects_->end();
}

std::vector< DrawableObj >::const_iterator ObjectCollection::begin() const noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an end iterator from the empty array" );

    return objects_->begin();
}

std::vector< DrawableObj >::const_iterator ObjectCollection::end() const noexcept( false )
{
    if ( !objects_ )
        MAKE_THROW( "Impossible to get an end iterator from the empty array" );

    return objects_->end();
}
