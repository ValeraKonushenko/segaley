#include "VertexNormalReader.h"

#include "ObjLoader.h"

#include <regex>

namespace objLoader
{

const size_t VertexNormalReader::rightCountOfPoints = 3u;

void VertexNormalReader::readDataFromSource( const std::vector< std::string >& data )
{
	BaseReader::readDataFromSource( data, DataType::VertexNormals );
}

void VertexNormalReader::process() noexcept( false )
{
	//Check only one time. Before, this line was at the for. 
	//This line inside the loop had a BIG impact on a performance ~ x90.
	VertexNormalReader::throwIfInvalidVertexNormalPeaces( source_.front() );

	for ( auto& data : source_ )
	{
		vertices_.emplace_back( getVertexNormalFromRow( data ) );
	}
}

void VertexNormalReader::clear()
{
	source_.clear();
	vertices_.clear();
}

void VertexNormalReader::moveVertexNormalTo( std::vector< glm::vec3 >& destination )
{
	destination.clear();
	destination = std::move( vertices_ );
}

const std::vector< glm::vec3 >& VertexNormalReader::data() const
{
	return vertices_;
}

std::string_view VertexNormalReader::getStringByErrorCode( ErrorCode errorCode )
{
	switch ( errorCode )
	{
	case objLoader::VertexNormalReader::ErrorCode::InvalidCountOfVertexNormalPoints:
		return "Was got error count of vertex normals";
		break;
	}

	return "Invalid error code";
}

void VertexNormalReader::throwIfInvalidVertexNormalPeaces( const std::string& data ) noexcept( false )
{
	std::regex regex( R"(-?\d+\.\d+)" );
	const std::ptrdiff_t matchCount = std::distance(
		std::sregex_iterator( data.begin(), data.end(), regex ),
		std::sregex_iterator() );

	if ( matchCount != VertexNormalReader::rightCountOfPoints )
		throw ErrorCode::InvalidCountOfVertexNormalPoints;
}

glm::vec3 VertexNormalReader::getVertexNormalFromRow( const std::string& row ) noexcept( false )
{
	glm::vec3 vertex;

	std::string matches = "1234567890-+";
	auto offset = row.find_first_of( matches );
	std::stringstream ss( row.substr( offset ) );

	ss >> vertex.x;
	ss >> vertex.y;
	ss >> vertex.z;

	return std::move( vertex );
}

};