#include "ObjLoader.h"

#include "UvReader.h"
#include "VertexNormalReader.h"
#include "VertexReader.h"

#include <string>
#include <fstream>
#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include <algorithm>


namespace objLoader
{

const std::filesystem::path ObjLoader::supportedExtension = ".obj";

std::string ObjLoader::getStringByErrorCode( ErrorCode errorCode )
{
	switch ( errorCode )
	{
	case ObjLoader::ErrorCode::InvalidPath:
		return "Was got an invalid path. Maybe permission denied or just no such file.";
		break;
	case ObjLoader::ErrorCode::CantOpenFile:
		return "Can't open a file by a some reason.";
		break;
	case ObjLoader::ErrorCode::CantWorkWithoutData:
		return "Can't work without loaded data to the internal buffer.";
		break;
	case ObjLoader::ErrorCode::NotSupportedMoreThenOneObjectPerFile:
		return "At this version of the Object Loader not supported more then one object-model per a file.";
		break;
	case ObjLoader::ErrorCode::InvalidFileExtension:
		return "You can load only *" + ObjLoader::supportedExtension.string() + " files";
		break;
	}

	return "Invalid error code.";
}

void ObjLoader::load( std::filesystem::path path ) noexcept( false )
{
	if ( !std::filesystem::exists( path ) )
		throw ErrorCode::InvalidPath;

	if ( path.extension() != ObjLoader::supportedExtension )
		throw ErrorCode::InvalidFileExtension;

	std::cout << "Try to load .obj model" << std::endl;
	loadAndParseDataFromFile( path );
	std::cout << "Data from file loaded to RAM" << std::endl;
	parseData();
	std::cout << "Parsed" << std::endl;
	processParsedData();
	std::cout << "Processed" << std::endl;
}

std::vector< DrawableObj > ObjLoader::getObjects()
{
	std::vector< DrawableObj > objects;

	for ( auto& model : models_ )
	{
		DrawableObj object;
		object.data() = std::move( model.vertexData );
		object.setName( model.name );

		objects.emplace_back( std::move( object ) );
	}

	return std::move( objects );
}

void ObjLoader::loadAndParseDataFromFile( std::filesystem::path path ) noexcept( false )
{
	auto data = loadDataFromFile( path );

	while ( !data.eof() )
	{
		std::string tmp;
		std::getline( data, tmp );
		
		if ( !tmp.empty() )
		{
			switch ( BaseReader::defineTypeOfData( tmp ) )
			{
			case DataType::Vertex:
				vertexReader.push( tmp );
				break;
			case DataType::TextureVertices:
				uvReader.push( tmp );
				break;
			case DataType::VertexNormals:
				vertexNormalReader.push( tmp );
				break;
			case DataType::Face:
				faceReader.push( tmp );
				break;				
			case DataType::ObjectName:
			{
				if ( !models_.empty() )
					models_.back().end = faceReader.countOfNotParsedData();

				Model model;
				model.name = getObjectName( tmp );
				model.begin = faceReader.countOfNotParsedData();

				models_.emplace_back( std::move( model ) );

				break;
			}
			}
		}
	}

	models_.back().end = faceReader.countOfNotParsedData();
}

void ObjLoader::loadFaces() noexcept( false )
{
	if ( faceReader.isEmpty() )
		throw ErrorCode::CantWorkWithoutData;

	faceReader.process();
	faceReader.clearSourceData();
}

void ObjLoader::loadVertices() noexcept( false )
{
	if ( vertexReader.isEmpty() )
		throw ErrorCode::CantWorkWithoutData;

	vertexReader.process();
	vertexReader.clearSourceData();
}

void ObjLoader::loadUvs() noexcept( false )
{
	if ( uvReader.isEmpty() )
		throw ErrorCode::CantWorkWithoutData;

	uvReader.process();
	uvReader.clearSourceData();
}

void ObjLoader::loadVertexNormals() noexcept( false )
{
	if ( vertexNormalReader.isEmpty() )
		throw ErrorCode::CantWorkWithoutData;

	vertexNormalReader.process();
	vertexNormalReader.clearSourceData();
}

void ObjLoader::parseData() noexcept( false )
{
	std::cout << "=======Loading " << "objectName_" << "=======" << std::endl;
	loadFaces();
	std::cout << "1/4 Faces loaded" << std::endl;
	loadVertices();
	std::cout << "2/4 Vertices loaded" << std::endl;
	loadUvs();
	std::cout << "3/4 UV-s loaded" << std::endl;
	loadVertexNormals();
	std::cout << "4/4 Vertex normals loaded" << std::endl;
}

void ObjLoader::processParsedData() noexcept( false )
{
	for ( auto& model : models_ )
	{
		for ( size_t i = model.begin; i < model.end; i++ )
		{
			for ( auto& p : faceReader.data()[ i ].point )
			{
				model.vertexData.emplace_back( VertexData(
					vertexReader.data().at( p.v - 1u ),
					vertexNormalReader.data().at( p.vn - 1u ),
					uvReader.data().at( p.vt - 1u ),
					{}
				) );
			}
		}
	}
	/*for ( auto& face : faceReader.data() )
	{
		for ( auto& p : face.point )
		{
			vertexData_.emplace_back( VertexData(
				vertexReader.data().at( p.v - 1u ),
				vertexNormalReader.data().at( p.vn - 1u ),
				uvReader.data().at( p.vt - 1u ),
				{}
			) );
		}
	}*/
}

std::stringstream ObjLoader::loadDataFromFile( std::filesystem::path path ) noexcept( false )
{
	std::ifstream file( path.c_str() );
	if ( !file.is_open() )
		throw ErrorCode::CantOpenFile;

	std::stringstream data;
	std::copy( std::istreambuf_iterator<char>( file ),
		 std::istreambuf_iterator<char>(),
		 std::ostreambuf_iterator<char>( data ) );

	file.close();
	
	return std::move( data );
}

std::string objLoader::ObjLoader::getObjectName( const std::string& str ) const
{
	auto spaceIndex = str.find_first_of( ' ' );
	auto symbolIndex = str.find_first_not_of( ' ', spaceIndex );
	return std::move( str.substr( symbolIndex ) );
}

};