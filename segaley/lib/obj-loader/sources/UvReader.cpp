#include "UvReader.h"

#include "ObjLoader.h"

#include <regex>

namespace objLoader
{

const size_t UvReader::rightCountOfPoints = 2u;

void UvReader::readDataFromSource( const std::vector< std::string >& data )
{
	BaseReader::readDataFromSource( data, DataType::TextureVertices );
}

void UvReader::process() noexcept( false )
{
	//Check only one time. Before, this line was at the for. 
	//This line inside the loop had a BIG impact on a performance ~ x90.
	UvReader::throwIfInvalidUvPeaces( source_.front() );

	for ( auto& data : source_ )
	{
		vertices_.emplace_back( getUvFromRow( data ) );
	}
}

void UvReader::clear()
{
	source_.clear();
	vertices_.clear();
}

void UvReader::moveUvsTo( std::vector< glm::vec2 >& destination )
{
	destination.clear();
	destination = std::move( vertices_ );
}

const std::vector< glm::vec2 >& UvReader::data() const
{
	return vertices_;
}

std::string_view UvReader::getStringByErrorCode( ErrorCode errorCode )
{
	switch ( errorCode )
	{
	case objLoader::UvReader::ErrorCode::InvalidCountOfUvPoints:
		return "Was got error count of UV-s";
		break;
	}

	return "Invalid error code";
}

void UvReader::throwIfInvalidUvPeaces( const std::string& data ) noexcept( false )
{
	std::regex regex( R"(-?\d+\.\d+)" );
	const std::ptrdiff_t matchCount = std::distance(
		std::sregex_iterator( data.begin(), data.end(), regex ),
		std::sregex_iterator() );

	if ( matchCount != UvReader::rightCountOfPoints )
		throw ErrorCode::InvalidCountOfUvPoints;
}

glm::vec2 UvReader::getUvFromRow( const std::string& row ) noexcept( false )
{
	glm::vec2 vertex;

	std::string matches = "1234567890-+";
	auto offset = row.find_first_of( matches );
	std::stringstream ss( row.substr( offset ) );

	ss >> vertex.x;
	ss >> vertex.y;

	return std::move( vertex );
}

};