#pragma once

#include "glm/glm.hpp"

//TODO: use doxygen format
//This class describes one draw-unit - vertex.
//It mean not physics vertex like a corner of
//a triangle or a square, but drawable-vertex.
//It include in this concept:
//-Physics vertex ( x, y, z )
//-Normal vertex ( x, y, z )
//-Texture position on atlas ( x, y )
//-Texture rect size ( width, height )
//-Texture rect position ( x, y )
class VertexData final
{
public:
	VertexData( glm::fvec3 physics = glm::fvec3( 0.f, 0.f, 0.f ),
					glm::fvec3 normal = glm::fvec3( 0.f, 0.f, 0.f ),
					glm::fvec2 textureSize = glm::fvec2( 0.f, 0.f ),
					glm::fvec2 uv = glm::fvec2( 0.f, 0.f )
	);

	VertexData( const VertexData& ) = default;
	VertexData( VertexData&& ) = default;
	VertexData& operator=( const VertexData& ) = default;
	VertexData& operator=( VertexData&& ) = default;

	~VertexData() = default;

	glm::fvec3 physics_;
	glm::fvec3 normal_;
	glm::fvec2 textureSize_;
	glm::fvec2 uv_;

	static size_t bsize();
private:
};