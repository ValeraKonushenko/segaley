#pragma once

#include "GLM/glm.hpp"
#include "VertexData.h"
#include "FaceReader.h"
#include "UvReader.h"
#include "VertexNormalReader.h"
#include "VertexReader.h"
#include "DrawableObj.h"

#include <filesystem>
#include <string_view>
#include <vector>

namespace objLoader
{

class FaceReader;
class VertexReader;
class UvReader;
class VertexNormalReader;

class ObjLoader final
{
public:
	enum class ErrorCode
	{
		InvalidPath,
		CantOpenFile,
		CantWorkWithoutData,
		NotSupportedMoreThenOneObjectPerFile,
		InvalidFileExtension
	};

	static const std::filesystem::path supportedExtension;

public:
	static std::string getStringByErrorCode( ErrorCode errorCode );

	void load( std::filesystem::path path ) noexcept( false );
	std::vector< DrawableObj > getObjects();

private:
	void loadAndParseDataFromFile( std::filesystem::path path ) noexcept( false );

	void loadFaces() noexcept( false );
	void loadVertices() noexcept( false );
	void loadUvs() noexcept( false );
	void loadVertexNormals() noexcept( false );
	void parseData() noexcept( false );

	void processParsedData() noexcept( false );
	std::stringstream loadDataFromFile( std::filesystem::path path ) noexcept( false );

	std::string getObjectName( const std::string& str ) const;

private:
	struct Model
	{
		std::string name;
		std::vector< VertexData > vertexData;
		size_t begin = 0u;
		size_t end = 0u;
	};

private:
	FaceReader faceReader;
	VertexReader vertexReader;
	UvReader uvReader;
	VertexNormalReader vertexNormalReader;

	std::vector< Model > models_;
};

};