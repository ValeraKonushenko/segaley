#pragma once

#include "DataType.h"

#include <vector>
#include <string>

namespace objLoader
{

class BaseReader
{
public:
	BaseReader() = default;
	virtual ~BaseReader() = default;
	BaseReader( const BaseReader& ) = default;
	BaseReader( BaseReader&& ) = default;
	BaseReader& operator=( const BaseReader& ) = default;
	BaseReader& operator=( BaseReader&& ) = default;

	virtual void readDataFromSource( const std::vector< std::string >& data, DataType dataType );
	virtual void process() noexcept( false ) = 0;
	virtual void clear() = 0;

	void clearSourceData();
	size_t countOfNotParsedData() const;

	void push( const std::string& row );
	bool isEmpty() const;

public:
	static DataType defineTypeOfData( std::string_view row );

protected:
	std::vector< std::string > source_;
};

};