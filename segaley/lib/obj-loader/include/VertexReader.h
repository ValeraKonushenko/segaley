#pragma once

#include "GLM/glm.hpp"
#include "BaseReader.h"

#include <array>

namespace objLoader
{

class VertexReader final : public BaseReader
{
public:
	enum class ErrorCode 
	{
		InvalidCountOfVertiesPoints
	};

public:
	static const size_t rightCountOfPoints;

	VertexReader() = default;
	~VertexReader() = default;
	VertexReader( const VertexReader& ) = default;
	VertexReader( VertexReader&& ) = default;
	VertexReader& operator=( const VertexReader& ) = default;
	VertexReader& operator=( VertexReader&& ) = default;

	void readDataFromSource( const std::vector< std::string >& data );
	void process() noexcept( false ) override;
	void clear() override;
	void moveVerticesTo( std::vector< glm::vec3 >& destination );
	const std::vector< glm::vec3 >& data() const;

public:
	static std::string_view getStringByErrorCode( ErrorCode errorCode );
	static void throwIfInvalidVertexPeaces( const std::string& row ) noexcept( false );

private:
	glm::vec3 getVertexFromRow( const std::string& row ) noexcept( false );

private:
	std::vector< glm::vec3 > vertices_;
};

};