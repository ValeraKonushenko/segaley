#pragma once

#include "DrawableObj.h"

#include <filesystem>
#include <optional>

class ObjectCollection final
{
public:
	void load( std::filesystem::path path, bool rewriteCache = false ) noexcept( false );
	void draw( const DrawData& data ) noexcept( false );
	void prepare();
	
	void setName( const std::string& name );
	std::string& getName();
	const std::string& getName() const;

	size_t size() const;

	DrawableObj& at( size_t i ) noexcept( false );
	const DrawableObj& at( size_t i ) const noexcept( false );
	DrawableObj& operator[]( size_t i ) noexcept( false );
	const DrawableObj& operator[]( size_t i ) const noexcept( false );
	
	bool isEmpty() const;

	void clear();
	void clearObjects();

	void push( DrawableObj&& obj );

	std::vector< DrawableObj >::iterator begin() noexcept( false );
	std::vector< DrawableObj >::iterator end() noexcept( false );
	std::vector< DrawableObj >::const_iterator begin() const noexcept( false );
	std::vector< DrawableObj >::const_iterator end() const noexcept( false );

private:
	std::optional< std::vector< DrawableObj > > objects_;
	std::string name_ = "None";
};
