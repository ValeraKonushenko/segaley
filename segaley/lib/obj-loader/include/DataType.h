#pragma once

namespace objLoader
{

enum class DataType
{
	None,
	ObjectName,
	Vertex,
	TextureVertices,
	VertexNormals,
	Smoothing,
	Face
};

}
