#pragma once

#include "BaseReader.h"

#include <array>

namespace objLoader
{

class FaceReader final : public BaseReader
{
public:
	struct Face
	{
		struct Point
		{
			size_t v;
			size_t vt;
			size_t vn;
		};

		std::array< Point, 3 > point;

		static const size_t rightCountOfPoint;
	};

	enum class ErrorCode
	{
		InvalidCountOfFacesPoints,
		TheEndOfLineWasMetPrematurely
	};

public:
	FaceReader() = default;
	~FaceReader() = default;
	FaceReader( const FaceReader& ) = default;
	FaceReader( FaceReader&& ) = default;
	FaceReader& operator=( const FaceReader& ) = default;
	FaceReader& operator=( FaceReader&& ) = default;

	void readDataFromSource( const std::vector< std::string >& data );
	void process() noexcept( false ) override;
	void clear() override;
	void moveFacesTo( std::vector< Face >& destination );
	const std::vector< Face >& data() const;

public:
	static std::string_view getStringByErrorCode( ErrorCode errorCode );
	static void throwIfInvalidFacePeaces( const std::string& data ) noexcept( false );

private:
	Face getFaceFromRow( const std::string& row ) const;

private:
	std::vector< Face > faces_;
};

};