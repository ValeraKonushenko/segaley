#pragma once

#include "VertexData.h"
#include "ObjectCollection.h"

#include <vector>
#include <filesystem>
#include <string>
#include <string_view>

namespace objLoader
{

class CacheManager final
{
public:
	enum class ErrorCode 
	{
		CantCreateDirectory,
		CantCreateCacheFile,
		NothingToSave,
		FileNotFound
	};

	static std::string_view getStringByErrorCode( ErrorCode code );

public:
	static const std::filesystem::path extension;
	static const std::filesystem::path directory;

public:
	CacheManager() = default;
	~CacheManager() = default;
	CacheManager( const CacheManager& ) = default;
	CacheManager( CacheManager&& ) = default;
	CacheManager& operator=( const CacheManager& ) = default;
	CacheManager& operator=( CacheManager&& ) = default;

	void saveToCache( const std::vector< VertexData >& data, std::filesystem::path filename ) noexcept( false );
	void saveToCache( const ObjectCollection& data ) noexcept( false );
	void loadFromCache( std::vector< VertexData >& data, std::filesystem::path filename ) noexcept( false );
	void loadFromCache( ObjectCollection& data ) noexcept( false );
	bool hasInCache( std::filesystem::path filename ) const;
	bool hasCollectionInCache( const std::string& name ) const;

private:
	std::filesystem::path getPathToFile( std::filesystem::path filename ) const;
	std::filesystem::path getPathToCollection( const std::string& name ) const;
	std::filesystem::path getPathToCollectionFile( const ObjectCollection& data, const std::string& name ) const;
	void createDirectory( std::string internalFolder = "" ) noexcept( false );
};

}