#pragma once

#include "GLM/glm.hpp"
#include "BaseReader.h"

#include <array>

namespace objLoader
{

class UvReader final : public BaseReader
{
public:
	enum class ErrorCode
	{
		InvalidCountOfUvPoints
	};

public:
	static const size_t rightCountOfPoints;

	UvReader() = default;
	~UvReader() = default;
	UvReader( const UvReader& ) = default;
	UvReader( UvReader&& ) = default;
	UvReader& operator=( const UvReader& ) = default;
	UvReader& operator=( UvReader&& ) = default;

	void readDataFromSource( const std::vector< std::string >& data );
	void process() noexcept( false ) override;
	void clear() override;
	void moveUvsTo( std::vector< glm::vec2 >& destination );
	const std::vector< glm::vec2 >& data() const;

public:
	static std::string_view getStringByErrorCode( ErrorCode errorCode );
	static void throwIfInvalidUvPeaces( const std::string& row ) noexcept( false );

private:
	glm::vec2 getUvFromRow( const std::string& row ) noexcept( false );

private:
	std::vector< glm::vec2 > vertices_;
};

};