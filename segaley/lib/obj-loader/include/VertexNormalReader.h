#pragma once

#include "GLM/glm.hpp"
#include "BaseReader.h"

#include <array>

namespace objLoader
{

class VertexNormalReader final : public BaseReader
{
public:
	enum class ErrorCode
	{
		InvalidCountOfVertexNormalPoints
	};

public:
	static const size_t rightCountOfPoints;

	VertexNormalReader() = default;
	~VertexNormalReader() = default;
	VertexNormalReader( const VertexNormalReader& ) = default;
	VertexNormalReader( VertexNormalReader&& ) = default;
	VertexNormalReader& operator=( const VertexNormalReader& ) = default;
	VertexNormalReader& operator=( VertexNormalReader&& ) = default;

	void readDataFromSource( const std::vector< std::string >& data );
	void process() noexcept( false ) override;
	void clear() override;
	void moveVertexNormalTo( std::vector< glm::vec3 >& destination );
	const std::vector< glm::vec3 >& data() const;

public:
	static std::string_view getStringByErrorCode( ErrorCode errorCode );
	static void throwIfInvalidVertexNormalPeaces( const std::string& row ) noexcept( false );

private:
	glm::vec3 getVertexNormalFromRow( const std::string& row ) noexcept( false );

private:
	std::vector< glm::vec3 > vertices_;
};

};