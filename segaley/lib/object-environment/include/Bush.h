#pragma once
#include "Rectangle.h"

#include <ctime>

namespace figure
{

class Bush final : public DrawableObj
{
public:
	Bush() = default;
	~Bush() = default;
	Bush( Bush&& ) = default;
	Bush& operator=( Bush&& ) = default;

	void setSize( FSize size );
	FSize getSize() const;

	void rebuild() noexcept( false ) override;

	void draw( const DrawData& data ) noexcept( false ) override;
	void instanceDraw( const DrawData& data, size_t count ) noexcept( false ) override;
	void drawOptimization( bool isTurnOn );
	bool hasDrawOptimization() const;
	void animation( clock_t time );
	void setAmplitude( float amplitude );
	float getAmplitude() const;
	void setFrequncy( float frequency );
	float getFrequncy() const;

	void setTextureRect( FFRect rect );
	FFRect getTextureRect() const;

private:
	FSize size_ = { 100.f, 100.f };
	bool isTurnOnDrawOptimization_ = false;
	clock_t time_ = 0;
	float amplitude_ = 10.f;
	float frequency_ = 700.f;
	FFRect rect_;
};

}