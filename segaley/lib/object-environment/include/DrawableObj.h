#pragma once

#include "VertexData.h"
#include "Vbo.h"
#include "Vao.h"
#include "Rect.h"
#include "Object.h"
#include "Material.h"

#include <vector>
#include <string_view>

enum class DisplayMode
{
	Display,
	Hide
};

class ShaderProgram;

struct DrawData
{
	ShaderProgram& shaderProgram;
	glm::mat4& vMat;
	glm::mat4& pMat;
};

//TODO: use doxygen
//  This class is interface for all drawable objects on a scene. If you want to create own 
//drawable object - you should to inherit from this class. After that override needed 
//function and put own object to needed functions to draw it.
class DrawableObj : public Object
{
public:
	using ContainerT = std::vector< VertexData >;
	Material material;

public:
	DrawableObj() = default;
	DrawableObj( bool isPrepare ) noexcept( false );

	DrawableObj( const DrawableObj& );
	DrawableObj( DrawableObj&& ) noexcept( false );
	virtual DrawableObj& operator=( const DrawableObj& );
	virtual DrawableObj& operator=( DrawableObj&& ) noexcept( false );

	virtual ~DrawableObj() = default;

	virtual void swap( DrawableObj& other );
	virtual void draw( const DrawData& data ) noexcept( false );
	virtual void instanceDraw( const DrawData& data, size_t count ) noexcept( false );

	//TODO: use doxygen
	//  Build system: it is system which must to rebuild or recalculate all needed data. E.g. 
	//add some vertices data to the vertices-container. Or generate something else to others 
	//containers.
	//  This method will be called while calling of the method DrawableObj::draw.
	//  Also this class DrawableObj has a flag which responsible for rebuilding. And if you 
	//think that necessary to rebuild your data, you can call method DrawableObj::setRebuild
	//with flag 'true' after that at while redrawing inside method DrawableObj::draw
	//will be called method rebuild.
	virtual void rebuild() noexcept( false );
	void setRebuild( bool _isNeedRebuild = true );
	bool isNeedRebuild() const;
	void setForceRebuild( bool force = true );
	bool isForceRebuild() const;

	ContainerT& data();
	const ContainerT& data() const;

	void prepare() noexcept( false );

	void clear() override;
	bool isEmpty() const;
	size_t getVerticesAmount() const;
	size_t getTrianglesAmount() const;
	void setVertices( const ContainerT& vertices );

	bool isTransparent() const;
	void setTransparency( bool is );

	void setName( const std::string& name );
	std::string& getName();
	const std::string& getName() const;

	void setTextureSize( glm::fvec2 size );
	glm::fvec2 getTextureSize() const;

	void setUv( glm::fvec2 uv );
	void moveUv( glm::fvec2 uv );
	glm::fvec2 getUv() const;

private:
	inline void setUpShader( const DrawData& data ) const;

	Vbo vbo_;
	Vao vao_;
	DisplayMode displayMode_ = DisplayMode::Display;
	ContainerT vertices_;
	bool isNeedRebuild_ = true;
	bool isTransparent_ = false;
	bool isForceRebuild_ = false;
	std::string name_ = "None";
	glm::fvec2 textureSize_ = glm::fvec2( 0.f, 0.f );
	glm::fvec2 uv_ = glm::fvec2( 0.f, 0.f );
};