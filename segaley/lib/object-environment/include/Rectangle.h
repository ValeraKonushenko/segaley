#pragma once

#include "DrawableObj.h"
#include "GLM/glm.hpp"

namespace figure
{

class Rectangle : public DrawableObj
{
public:
	Rectangle();
	Rectangle( bool isPrepare ) noexcept( false );

	Rectangle( const Rectangle& ) = default;
	Rectangle( Rectangle&& ) noexcept( false ) = default;
	Rectangle& operator=( const Rectangle& ) = default;
	Rectangle& operator=( Rectangle&& ) noexcept( false ) = default;

	~Rectangle() = default;

	void draw( const DrawData& data ) noexcept( false );

	void swap( DrawableObj& other ) noexcept override;
	void rebuild() noexcept( false ) override;

	void setWidth( float width );
	float getWidth() const;

	void setHeight( float height );
	float getHeight() const;

	void setSize( FSize size );
	FSize getSize() const;

	void setTextureRect( FFRect rect );
	FFRect getTextureRect() const;

private:
	FSize size_;
	FFRect rect_;
};

}