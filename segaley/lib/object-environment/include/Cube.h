#pragma once

#include "Rectangle.h"
#include "GLM/glm.hpp"

namespace figure
{

class Cube : public DrawableObj
{
public:
	Cube();
	Cube( bool isPrepare ) noexcept( false );

	Cube( const Cube& ) = default;
	Cube( Cube&& ) noexcept( false ) = default;
	Cube& operator=( const Cube& ) = default;
	Cube& operator=( Cube&& ) noexcept( false ) = default;

	~Cube() = default;

	void swap( DrawableObj& other ) noexcept override;
	void rebuild() noexcept( false ) override;

	void setSize( glm::vec3 sizeXyz );
	glm::vec3 getSize() const;

	void setTextureRect( FFRect rect );
	FFRect getTextureRect() const;

private:
	glm::vec3 size_;
	FFRect rect_;
};

}