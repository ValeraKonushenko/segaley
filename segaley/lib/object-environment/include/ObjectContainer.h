#pragma once
#include "Object.h"
#include "DrawableObj.h"

#include <vector>
#include <GLM/glm.hpp>

class ObjectContainer final
{
public:
	ObjectContainer() = default;
	~ObjectContainer() = default;
	ObjectContainer( ObjectContainer&& ) = delete;
	ObjectContainer& operator=( ObjectContainer&& ) = delete;

	void push( DrawableObj& object );
	void erase( DrawableObj& object ) noexcept( false );
	void draw( DrawData drawData, glm::vec3 cameraPosition );

private:
	void sort( glm::vec3 cameraPosition );

private:
	std::vector< DrawableObj* > objects_;
};
