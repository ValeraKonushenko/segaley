#pragma once
#include "glm/glm.hpp"

class Object
{
public:
	struct Transformations
	{
		glm::fvec3 scale = glm::fvec3( 1.f, 1.f, 1.f );
		glm::fvec3 translate = glm::fvec3( 0.f, 0.f, 0.f );
		std::pair< float, glm::fvec3 > rotate = { 0.f, glm::fvec3( 0.f, 0.f, 0.f ) };

		void clear();
	};

public:
	Object() = default;
	~Object() = default;
	Object( const Object& ) = default;
	Object( Object&& ) = default;
	Object& operator=( const Object& ) = default;
	Object& operator=( Object&& ) = default;

	glm::vec3 getImpulse() const;
	void makeImpulse( glm::vec3 impulse );

	virtual void swap( Object& other );

	virtual void update();
	virtual void clear();

	void scale( glm::fvec3 value );
	void setScale( glm::fvec3 value );
	glm::fvec3 getScale() const;

	void setPosition( glm::fvec3 value );
	glm::fvec3 getTranslate() const;
	void move( glm::fvec3 value );

	void setRotate( std::pair< float, glm::fvec3 > value );
	std::pair< float, glm::fvec3 > getRotate() const;

	Transformations getTrasformations() const;
	void setTrasformations( const Transformations& other );

protected:
	glm::mat4 calcModelMatrix() const;
	bool checkMaxSpeed( glm::vec3 vec );

private:
	//TODO: move to settings
	float resistance_ = 1.99f;//Default value
	//TODO: move to settings
	float maxSpeed = 11.7f;//Default value
	Transformations transformations_;
	glm::vec3 impulse_ = glm::vec3( 0.f, 0.f, 0.f );
};