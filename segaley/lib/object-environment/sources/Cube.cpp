#include "Cube.h"

namespace figure
{

Cube::Cube() : 
	size_( 100.f, 100.f, 100.f )
{
}

Cube::Cube( bool isPrepare ) noexcept( false ) :
	size_( 100.f, 100.f, 100.f ),
	DrawableObj( isPrepare )
{
}

void Cube::swap( DrawableObj& other ) noexcept
{
	auto& obj = *dynamic_cast< Cube* >( &other );

	auto tmpSize = obj.size_;
	obj.size_ = size_;
	size_ = tmpSize;

	DrawableObj::swap( other );
}

void Cube::rebuild() noexcept( false )
{
	auto rect = getTextureRect();
	auto x = size_.x, y = size_.y, z = size_.z;
	
	setVertices( ContainerT( {
		//Front
		VertexData( { 0.f,	0.f,	0.f }, { 0.f, 0.f, -1.f },   { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
		VertexData( { x,	0.f,	0.f }, { 0.f, 0.f, -1.f },   { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		0.f }, { 0.f, 0.f, -1.f },   { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
														   
		VertexData( { 0.f,	y,		0.f }, { 0.f, 0.f, -1.f },   { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	0.f,	0.f }, { 0.f, 0.f, -1.f },   { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		0.f }, { 0.f, 0.f, -1.f },   { rect.size.width, rect.size.height	},   { rect.position.x, rect.position.y } ),

		//Back
		VertexData( { x,	0.f,	 z },  { 0.f, 0.f,  1.f },   { rect.size.width, 0.f					},  { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	 z },  { 0.f, 0.f,  1.f },   { 0.f,				  0.f				},  { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		 z },  { 0.f, 0.f,  1.f },   { 0.f,				  rect.size.height	},  { rect.position.x, rect.position.y } ),
										 				    
		VertexData( { x,	0.f,	 z },  { 0.f, 0.f,  1.f },   { rect.size.width, 0.f					},  { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		 z },  { 0.f, 0.f,  1.f },   { 0.f,				  rect.size.height	},  { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		 z },  { 0.f, 0.f,  1.f },   { rect.size.width, rect.size.height	},  { rect.position.x, rect.position.y } ),

		//Left
		VertexData( { 0.f,	0.f,	0.f }, {-1.f, 0.f, 0.f },    { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		z	}, {-1.f, 0.f, 0.f },    { 0.f,				  rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	z   }, {-1.f, 0.f, 0.f },    { 0.f,				  0.f               },   { rect.position.x, rect.position.y } ),
																    
		VertexData( { 0.f,	y,		z	}, {-1.f, 0.f, 0.f },    { 0.f,				  rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	0.f }, {-1.f, 0.f, 0.f },    { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		0.f	}, {-1.f, 0.f, 0.f },    { rect.size.width, rect.size.height	},   { rect.position.x, rect.position.y } ),
		
		//Right
		VertexData( { x,	0.f,	0.f }, {  1.f, 0.f, 0.f },  { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	0.f,	z   }, {  1.f, 0.f, 0.f },  { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		z	}, {  1.f, 0.f, 0.f },  { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
						  						  
		VertexData( { x,	0.f,	0.f }, {  1.f, 0.f, 0.f },  { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		z	}, {  1.f, 0.f, 0.f },  { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		0.f	}, {  1.f, 0.f, 0.f },  { rect.size.width, rect.size.height		},   { rect.position.x, rect.position.y } ),

		//Top
		VertexData( { 0.f,	y,		0.f }, {  0.f, 1.f, 0.f },  { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		0.f }, {  0.f, 1.f, 0.f },  { rect.size.width, rect.size.height		},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	y,		z	}, {  0.f, 1.f, 0.f },  { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
												  	   
		VertexData( { 0.f,	y,		z	}, {  0.f, 1.f, 0.f },  { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		0.f }, {  0.f, 1.f, 0.f },  { rect.size.width, rect.size.height		},   { rect.position.x, rect.position.y } ),
		VertexData( { x,	y,		z   }, {  0.f, 1.f, 0.f },  { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),

		//Bottom
		VertexData( { x,	0.f,	0.f }, { 0.f,-1.f, 0.f },   { rect.size.width, rect.size.height		},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	0.f }, { 0.f,-1.f, 0.f },   { 0.f,				 rect.size.height	},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	z	}, { 0.f,-1.f, 0.f },   { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
																   
		VertexData( { x,	0.f,	0.f }, { 0.f,-1.f, 0.f },   { rect.size.width, rect.size.height		},   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,	0.f,	z	}, { 0.f,-1.f, 0.f },   { 0.f,				 0.f                },   { rect.position.x, rect.position.y } ),
		VertexData( { x,	0.f,	z   }, { 0.f,-1.f, 0.f },   { rect.size.width, 0.f					},   { rect.position.x, rect.position.y } ),
	} ) );

	setRebuild( false );

	DrawableObj::rebuild();
}

void Cube::setSize( glm::vec3 sizeXyz )
{
	size_ = sizeXyz;
}

glm::vec3 Cube::getSize() const
{
	return size_;
}

void Cube::setTextureRect( FFRect rect )
{
	rect_ = rect;
}

FFRect Cube::getTextureRect() const
{
	return rect_;
}

}