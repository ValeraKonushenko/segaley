#include "DrawableObj.h"

#include "Thrower.h"
#include "GLFW.h"
#include "ShaderProgram.h"
#include "glm/gtc/type_ptr.hpp"
#include "utils.h"

#include <utility>

DrawableObj::DrawableObj( bool isPrepare ) noexcept( false )
{
	prepare();
}

DrawableObj::DrawableObj( const DrawableObj& other )
{
	*this = other;
}

DrawableObj::DrawableObj( DrawableObj&& other ) noexcept( false )
{
	*this = std::move( other );
}

DrawableObj& DrawableObj::operator=( const DrawableObj& other )
{	
	Object::operator=( other );
	material = other.material;
	vbo_ = other.vbo_;
	vao_ = other.vao_;
	displayMode_ = other.displayMode_;
	vertices_ = other.vertices_;
	isNeedRebuild_ = other.isNeedRebuild_;
	isTransparent_ = other.isTransparent_;
	name_ = other.name_;

	return *this;
}

DrawableObj& DrawableObj::operator=( DrawableObj&& other ) noexcept( false )
{
	Object::operator=( std::move( other ) );
	material = other.material;
	vbo_ = std::move( other.vbo_ );
	vao_ = std::move( other.vao_ );
	displayMode_ = other.displayMode_;
	vertices_ = std::move( other.vertices_ );	
	isNeedRebuild_ = other.isNeedRebuild_;
	isTransparent_ = other.isTransparent_;
	name_ = std::move( other.name_ );

	return *this;
}

void DrawableObj::swap( DrawableObj& other )
{
	Object::swap( other );
	auto tmpDisplayMode = other.displayMode_;
	auto tmpVertices = std::move( other.vertices_ );

	other = std::move( *this );
	
	displayMode_ = tmpDisplayMode;
	vertices_ = tmpVertices;
}

void DrawableObj::draw( const DrawData& data ) noexcept( false )
{		
	vao_.bind();
	vbo_.bind();

	setUpShader( data );

	if ( isNeedRebuild() )
		rebuild();

	auto modelMatrix = calcModelMatrix();

	data.shaderProgram.uniform( "uMaterial.specularIntensity", material.specular.intensity );
	data.shaderProgram.uniform( "uMaterial.specularPowder", material.specular.powder );
	data.shaderProgram.uniform( "uMaterial.specularColor", material.specular.color );
	data.shaderProgram.uniform( "uModelMatrix", modelMatrix );
	data.shaderProgram.uniform( "uViewMatrix", data.vMat );
	data.shaderProgram.uniform( "uProjectionMatrix", data.pMat );
	glDrawArrays( GL_TRIANGLES, 0, static_cast< GLsizei >( getVerticesAmount() ) );

	vao_.unbind();
}

void DrawableObj::instanceDraw( const DrawData& data, size_t count ) noexcept( false )
{
	vao_.bind();
	vbo_.bind();

	setUpShader( data );

	if ( isNeedRebuild() || isForceRebuild() )
		rebuild();

	auto modelMatrix = calcModelMatrix();

	data.shaderProgram.uniform( "uMaterial.specularIntensity", material.specular.intensity );
	data.shaderProgram.uniform( "uMaterial.specularPowder", material.specular.powder );
	data.shaderProgram.uniform( "uMaterial.specularColor", material.specular.color );
	data.shaderProgram.uniform( "uModelMatrix", modelMatrix );
	data.shaderProgram.uniform( "uViewMatrix", data.vMat );
	data.shaderProgram.uniform( "uProjectionMatrix", data.pMat );
	glDrawArraysInstanced( GL_TRIANGLES, 0, static_cast< GLsizei >( getVerticesAmount() ), static_cast< GLsizei >( count ) );

	vao_.unbind();
}

void DrawableObj::rebuild() noexcept( false )
{
	vbo_.bind();
	vbo_.setData( data() );
}

void DrawableObj::setRebuild( bool _isNeedRebuild )
{
	isNeedRebuild_ = _isNeedRebuild;
}

bool DrawableObj::isNeedRebuild() const
{
	return isNeedRebuild_;
}

void DrawableObj::setForceRebuild( bool force )
{
	isForceRebuild_ = force;
}

bool DrawableObj::isForceRebuild() const
{
	return isForceRebuild_;
}

DrawableObj::ContainerT& DrawableObj::data()
{
	return vertices_;
}

const DrawableObj::ContainerT& DrawableObj::data() const
{
	return vertices_;
}

void DrawableObj::prepare() noexcept( false )
{
	vao_.generate();
	vbo_.generate();
}

void DrawableObj::clear()
{
	Object::clear();
	displayMode_ = DisplayMode::Display;
	vertices_.clear();
}

bool DrawableObj::isEmpty() const
{
	return vertices_.empty();
}

size_t DrawableObj::getVerticesAmount() const
{
	return vertices_.size();
}

size_t DrawableObj::getTrianglesAmount() const
{
	return getVerticesAmount() / 3u;
}

void DrawableObj::setVertices( const ContainerT& vertices )
{
	setRebuild();
	vertices_ = vertices;
}

bool DrawableObj::isTransparent() const
{
	return isTransparent_;
}

void DrawableObj::setTransparency( bool is )
{
	isTransparent_ = is;
}

void DrawableObj::setName( const std::string& name )
{
	name_ = name;
}

std::string& DrawableObj::getName()
{
	return name_;
}

const std::string& DrawableObj::getName() const
{
	return name_;
}

void DrawableObj::setTextureSize( glm::fvec2 size )
{
	for ( auto& v : data() )
	{
		if ( !utils::isEqual( v.textureSize_.x, 0.f ) )
			v.textureSize_.x /= utils::isEqual( textureSize_.x, 0.f ) ? 1.f : textureSize_.x;

		if ( !utils::isEqual( v.textureSize_.y, 0.f ) )
			v.textureSize_.y /= utils::isEqual( textureSize_.y, 0.f ) ? 1.f : textureSize_.y;

		v.textureSize_ *= size;
	}

	textureSize_ = size;

	setRebuild();
}

glm::fvec2 DrawableObj::getTextureSize() const
{
	return textureSize_;
}

void DrawableObj::setUv( glm::fvec2 uv )
{
	for ( auto& v : data() )
	{
		v.uv_ = uv;
	}

	setRebuild();
}

void DrawableObj::moveUv( glm::fvec2 uv )
{
	for ( auto& v : data() )
	{
		v.uv_ += uv;
	}
}

glm::fvec2 DrawableObj::getUv() const
{
	return uv_;
}

inline void DrawableObj::setUpShader( const DrawData& data ) const
{
	data.shaderProgram.vertexAttribPtr( 0, 3, 10, 0 );
	data.shaderProgram.vertexAttribPtr( 1, 3, 10, 3 );
	data.shaderProgram.vertexAttribPtr( 2, 2, 10, 6 );
	data.shaderProgram.vertexAttribPtr( 3, 2, 10, 8 );
}