#include "Rectangle.h"
#include "Rectangle.h"
#include "Vao.h"
#include "Vbo.h"

namespace figure
{

Rectangle::Rectangle() :
	size_{ 100.f, 100.f }
{
}

Rectangle::Rectangle( bool isPrepare ) noexcept( false ) :
	DrawableObj( isPrepare ),
	size_{ 100.f, 100.f }
{
}

void Rectangle::draw( const DrawData& data ) noexcept( false )
{
	glDisable( GL_CULL_FACE );
	DrawableObj::draw( data );
	glEnable( GL_CULL_FACE );
}

void Rectangle::swap( DrawableObj& other ) noexcept
{
	auto& obj = *dynamic_cast< Rectangle* >( &other );

	auto tmpSize = obj.size_;
	obj.size_ = size_;
	size_ = tmpSize;

	DrawableObj::swap( other );
}

void Rectangle::rebuild() noexcept( false )
{
	auto rect = getTextureRect();
	setVertices( ContainerT( {
		VertexData( { 0.f,			0.f,			0.f }, { 0.f, 1.f, 0.f },   { 0.f,				0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,  0.f,			0.f }, { 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,			size_.height,	0.f }, { 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),
																		   
		VertexData( { 0.f,			size_.height,	0.f }, { 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,	0.f,			0.f }, { 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,	size_.height,	0.f }, { 0.f, 1.f, 0.f },   { rect.size.width,	rect.size.height },   { rect.position.x, rect.position.y } )
	} ) );

	setRebuild( false );

	DrawableObj::rebuild();
}

void Rectangle::setWidth( float width )
{
	setRebuild();
	size_.width = width;
}

float Rectangle::getWidth() const
{
	return size_.width;
}

void Rectangle::setHeight( float height )
{
	setRebuild();
	size_.height = height;
}

float Rectangle::getHeight() const
{
	return size_.height;
}

void Rectangle::setSize( FSize size )
{
	size_ = size;
}

FSize Rectangle::getSize() const
{
	return size_;
}

void Rectangle::setTextureRect( FFRect rect )
{
	rect_ = rect;
}

FFRect Rectangle::getTextureRect() const
{
	return rect_;
}

}