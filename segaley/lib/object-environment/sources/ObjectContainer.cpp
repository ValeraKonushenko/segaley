#include "ObjectContainer.h"

#include "Thrower.h"

#include <algorithm>
#include <string>

void ObjectContainer::push( DrawableObj& object )
{
	objects_.push_back( &object );
}

void ObjectContainer::erase( DrawableObj& object ) noexcept( false )
{
	using namespace std;
	auto it = find_if( objects_.begin(), objects_.end(), [ &object ]( auto obj )
	{
		return &object == obj;
	} );

	if ( it == objects_.end() )		
		MAKE_THROW( "No such object: " + to_string( reinterpret_cast< long long >( &object ) ) );
	
	objects_.erase( it );
}

void ObjectContainer::draw( DrawData drawData, glm::vec3 cameraPosition )
{
	sort( cameraPosition );
	
	for ( auto& obj : objects_ )
		obj->draw( drawData );
}

void ObjectContainer::sort( glm::vec3 cameraPosition )
{
	std::vector< std::pair< float, DrawableObj* > > clearObjects;
	std::vector< DrawableObj* > otherObjects;
	clearObjects.reserve( objects_.size() );
	
	for ( auto& obj : objects_ )
	{
		if ( obj->isTransparent() )
		{
			auto distance = glm::distance( obj->getTranslate(), cameraPosition );
			auto tmp = std::make_pair( distance, obj );
			clearObjects.emplace_back( tmp );
		}
		else
			otherObjects.emplace_back( obj );
	}

	std::sort( clearObjects.begin(), clearObjects.end(), []( const auto& a, const auto& b )
	{
		return a.first > b.first;
	} );

	objects_.clear();
	objects_ = std::move( otherObjects );
	objects_.reserve( clearObjects.size() + objects_.size() );

	for ( auto& obj : clearObjects )	
		objects_.emplace_back( obj.second );	

}
