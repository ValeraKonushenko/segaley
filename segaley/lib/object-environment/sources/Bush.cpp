#include "Bush.h"
#include <iostream>

namespace figure
{

void Bush::setSize( FSize size )
{
	size_ = size;
}

FSize Bush::getSize() const
{
	return size_;
}

void figure::Bush::rebuild() noexcept( false )
{
	auto rect = getTextureRect();

	auto offset = std::max( size_.width, size_.height ) * 0.5f;
	
	auto motion = cosf( static_cast< float >( time_ ) / frequency_ ) * amplitude_;

	setVertices( ContainerT( {
		//The first leave
		VertexData( { 0.f,			0.f,			offset },			{ 0.f, 1.f, 0.f },   { 0.f,				0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,  0.f,			offset },			{ 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { 0.f,			size_.height,	offset + motion },			{ 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),

		VertexData( { 0.f,			size_.height,	offset + motion },			{ 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,	0.f,			offset },			{ 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { size_.width,	size_.height,	offset + motion },			{ 0.f, 1.f, 0.f },   { rect.size.width,	rect.size.height },   { rect.position.x, rect.position.y } ),

		//The second leave
		VertexData( { offset,		0.f,			0.f },			{ 0.f, 1.f, 0.f },   { 0.f,				0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { offset,		size_.height,	motion },			{ 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),
		VertexData( { offset,		0.f,			size_.width },	{ 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
						  
		VertexData( { offset,		0.f,			size_.width },	{ 0.f, 1.f, 0.f },   { rect.size.width,	0.f              },   { rect.position.x, rect.position.y } ),
		VertexData( { offset,		size_.height,	motion },			{ 0.f, 1.f, 0.f },   { 0.f,				rect.size.height },   { rect.position.x, rect.position.y } ),
		VertexData( { offset,		size_.height,	size_.width + motion},	{ 0.f, 1.f, 0.f },   { rect.size.width,	rect.size.height },   { rect.position.x, rect.position.y } )
	} ) );

 	setRebuild( false );

	DrawableObj::rebuild();
}

void figure::Bush::draw( const DrawData& data ) noexcept( false )
{
	if ( !isTurnOnDrawOptimization_ )//TODO: move it to xxxShader::use()
	{
		glDepthMask( GL_FALSE );
		glDisable( GL_CULL_FACE );
	}

	DrawableObj::draw( data );

	if ( !isTurnOnDrawOptimization_ )
	{
		glEnable( GL_CULL_FACE );
		glDepthMask( GL_TRUE );
	}
}

void figure::Bush::instanceDraw( const DrawData& data, size_t count ) noexcept( false )
{
	if ( !isTurnOnDrawOptimization_ )//TODO: move it to xxxShader::use()
	{
		glDisable( GL_CULL_FACE );
		glDisable( GL_BLEND );
	}

	DrawableObj::instanceDraw( data, count );

	if ( !isTurnOnDrawOptimization_ )
	{
		glEnable( GL_CULL_FACE );
		glEnable( GL_BLEND );
	}
}

void Bush::drawOptimization( bool isTurnOn )
{
	isTurnOnDrawOptimization_ = isTurnOn;
}

bool Bush::hasDrawOptimization() const
{
	return isTurnOnDrawOptimization_;
}

void Bush::animation( clock_t time )
{
	time_ = time;
}

void Bush::setAmplitude( float amplitude )
{
	amplitude_ = amplitude;
}

float Bush::getAmplitude() const
{
	return amplitude_;
}

void Bush::setFrequncy( float frequency )
{
	frequency_ = frequency;
}

float Bush::getFrequncy() const
{
	return frequency_;
}

void Bush::setTextureRect( FFRect rect )
{
	rect_ = rect;
}

FFRect Bush::getTextureRect() const
{
	return rect_;
}

}