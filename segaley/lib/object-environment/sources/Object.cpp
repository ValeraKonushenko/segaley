#include "Object.h"
#include "glm/gtc/type_ptr.hpp"

glm::vec3 Object::getImpulse() const
{
	return impulse_;
}

void Object::makeImpulse( glm::vec3 impulse )
{
	impulse_ += impulse;
}

void Object::swap( Object& other )
{
	auto tmpTransformations = other.transformations_;

	other = std::move( *this );

	this->transformations_ = tmpTransformations;
}

void Object::update()
{
	transformations_.translate += impulse_;
	impulse_ *= glm::vec3( resistance_ );
}

void Object::clear()
{
	transformations_.clear();
}

void Object::Transformations::clear()
{
	scale = glm::fvec3( 1.f, 1.f, 1.f );
	translate = glm::fvec3( 0.f, 0.f, 0.f );
	rotate = { 0.f, glm::fvec3( 0.f, 0.f, 0.f ) };
}

void Object::scale( glm::fvec3 value )
{
	transformations_.scale += value;
}

void Object::setScale( glm::fvec3 value )
{
	transformations_.scale = value;
}

glm::fvec3 Object::getScale() const
{
	return transformations_.scale;
}

void Object::setPosition( glm::fvec3 value )
{
	transformations_.translate = value;
}

glm::fvec3 Object::getTranslate() const
{
	return transformations_.translate;
}

void Object::move( glm::fvec3 value )
{
	transformations_.translate += value;
}

void Object::setRotate( std::pair<float, glm::fvec3> value )
{
	transformations_.rotate = value;
}

std::pair< float, glm::fvec3 > Object::getRotate() const
{
	return transformations_.rotate;
}

Object::Transformations Object::getTrasformations() const
{
	return transformations_;
}

void Object::setTrasformations( const Transformations& other )
{
	transformations_ = other;
}

bool Object::checkMaxSpeed( glm::vec3 vec )
{
	if ( glm::length( impulse_ ) >= maxSpeed )
		return true;

	return false;
}

glm::mat4 Object::calcModelMatrix() const
{
	glm::mat4 modelMat = glm::mat4( 1.f );
	modelMat = glm::translate( modelMat, transformations_.translate );
	
	if ( transformations_.rotate.first != 0.f )
		modelMat = glm::rotate( modelMat, transformations_.rotate.first, transformations_.rotate.second );

	modelMat = glm::scale( modelMat, transformations_.scale );
	return modelMat;
}