#pragma once

#include "WindowSize.h"

#include <string>
#include <string_view>
#include <vector>
#include <fstream>
#include <limits>

namespace utils
{

std::string getFileContent( std::string_view path ) noexcept( false );

void setFileContent( std::string_view path, std::string_view data ) noexcept( false );

void setFileContent( std::string_view path, void* p, size_t size ) noexcept( false );

std::vector< WindowSize > getAllSupportedWndSizes();

std::ifstream readFile( std::string_view path );

bool isEqual( float n1, float n2, float E = std::numeric_limits< float >::epsilon() );

}