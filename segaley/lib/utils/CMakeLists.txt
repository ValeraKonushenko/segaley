
add_library(
	utils STATIC
	include/utils.h
	sources/utils.cpp
)

target_link_libraries(
	utils PUBLIC
	gl-environment
)