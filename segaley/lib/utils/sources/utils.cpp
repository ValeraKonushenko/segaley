#include "utils.h"

#include "Thrower.h"

#include <stdexcept>
#include <fstream>
#include <stdio.h>
#include <Windows.h>


namespace utils
{

std::string getFileContent( std::string_view path ) noexcept( false )
{
	std::ifstream file( path.data() );
	if ( !file.is_open() )
		MAKE_THROW( std::string( "File: '" ) + path.data() + "' can't be is opened" );

	std::string buffer( ( std::istreambuf_iterator<char>( file ) ), std::istreambuf_iterator<char>() );

	return std::move( buffer );
}

void setFileContent( std::string_view path, std::string_view data ) noexcept( false )
{
	std::fstream file( path.data(), std::ios::out );
	if( !file.is_open() )
		MAKE_THROW( std::string( "File: '" ) + path.data() + "' can't be is opened" );

	file.write( data.data(), data.size() );

	file.close();
}

void setFileContent( std::string_view path, void* p, size_t size ) noexcept( false )
{
	std::fstream file( path.data(), std::ios::out | std::ios::binary );
	if( !file.is_open() )
		MAKE_THROW( std::string( "File: '" ) + path.data() + "' can't be is opened" );
	
	file.write( static_cast< char* >( p ), size );

	file.close();
}

std::vector< WindowSize > getAllSupportedWndSizes()
{
	std::vector< WindowSize > data;
	DEVMODEA mode;
	mode.dmSize = sizeof( mode );

	for( DWORD i = 0; EnumDisplaySettings( nullptr, i, &mode ); i++ )
	{
		WindowSize tmp = {
			static_cast< unsigned short >( mode.dmPelsWidth ),
			static_cast< unsigned short >( mode.dmPelsHeight )
		};
		
		if( i != 0 ){
			if( data.back() != tmp )
				data.push_back( tmp );
		}
		else
			data.push_back( tmp );
	}

	return data;
}

std::ifstream readFile( std::string_view path )
{
	std::ifstream file( path.data() );
	if ( !file.is_open() )
		MAKE_THROW( std::string( "Such file couldn't be found: " ) + path.data() );
	return std::move( file );
}

bool isEqual( float n1, float n2, float E )
{
	return fabs( n1 - n2 ) < E;
}

}