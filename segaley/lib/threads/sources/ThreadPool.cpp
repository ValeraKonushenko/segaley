#include "ThreadPool.h"

#include "Thrower.h"

ThreadPool::ThreadPool()
{
}

ThreadPool* ThreadPool::instance()
{
	static std::unique_ptr< ThreadPool > p = nullptr;

	if ( !p )
	{
		p = std::unique_ptr< ThreadPool >( new ThreadPool );
	}

	return p.get();
}

ThreadPool::~ThreadPool()
{
	closeAllThreads();
}

void ThreadPool::closeAllThreads()
{
	for ( auto& thread : threads_ )
	{
		thread->stop();
	}
}

size_t ThreadPool::pushAndStart( std::function<void()> function )
{	
	threads_.push_back( std::make_unique< ThreadWrapper >( function ) );
	threads_[ threads_.size() - 1u ]->start();
	return threads_.size() - 1u;
}

size_t ThreadPool::size()
{
	return threads_.size();
}

void ThreadPool::update() noexcept( false )
{
	bool isRecheck = false;
	do
	{
		isRecheck = false;
		for ( auto i = threads_.begin(); i < threads_.end(); i++ )
		{
			if ( ( *i )->isHasError() )
				MAKE_THROW( ( *i )->getExceptionString() );
			if ( ( *i )->isCanBeFinished() )
			{
				( *i )->stop();
				threads_.erase( i );
				isRecheck = true;
				break;
			}
		}
	} while ( isRecheck );
}

ThreadWrapper& ThreadPool::at( size_t i ) noexcept( false )
{
	if ( i >= size() )
		MAKE_THROW( "Impossible to get a thread at not existing index" );
	return *threads_[ i ];
}
