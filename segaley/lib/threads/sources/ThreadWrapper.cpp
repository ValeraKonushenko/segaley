#include "ThreadWrapper.h"

#include "Thrower.h"

#include <type_traits>

ThreadWrapper::ThreadWrapper( StopMethod stopMethod ) :
	status_( Status::Stopped ),
	stopMethod_( stopMethod ),
	isCanBeFinished_( false )
{
}

ThreadWrapper::ThreadWrapper( std::function<void()> callback, StopMethod stopMethod ) noexcept( false ) :
	status_( Status::Stopped ),
	stopMethod_( stopMethod ),
	isCanBeFinished_( false )
{
	*this = callback;
}

ThreadWrapper::~ThreadWrapper()
{
	stop();
}

ThreadWrapper& ThreadWrapper::operator=( std::function<void()> callback ) noexcept( false )
{
	setCallback( callback );
	return *this;
}

void ThreadWrapper::setCallback( std::function< void() > callback ) noexcept( false )
{
	if ( isActive() )
		MAKE_THROW( "Impossible to put the callback to the running thread" );

	callback_ = callback;
}

void ThreadWrapper::start() noexcept( false )
{
	if ( isActive() )
		MAKE_THROW( "Impossible to start already the running thread" );

	if ( !callback_ )
		MAKE_THROW( "Impossible to create new thread without a callback function." );

	realCallback_ = [ this ]()
	{
		try
		{
			callback_();
			isCanBeFinished_ = true;
			if ( finishCallback_ )
				finishCallback_();
		}
		catch ( const std::runtime_error& ex )
		{
			using namespace std::string_literals;
			
			size_t threadId = std::hash< decltype( getId() ) >{}( getId() );
			std::lock_guard lg( mutexExceptionString_ );
			exceptionString_ = "Error at thread #"s + std::to_string( threadId ) + ": " + ex.what();
		}
	};
	isCanBeFinished_ = false;
	thread_ = std::make_unique< std::thread >( realCallback_ );
	status_ = Status::Running;
}

bool ThreadWrapper::stop()
{
	if ( !isActive() )//analog of thread_->joinable() or 
		return false;

	switch ( stopMethod_ )
	{
		case StopMethod::Join: thread_->join(); break;
		case StopMethod::Detach: thread_->detach(); break;
	}	

	isCanBeFinished_ = false;
	thread_.release();	
	status_ = Status::Stopped;

	std::lock_guard lg( mutexExceptionString_ );
	exceptionString_.clear();
	return true;
}

bool ThreadWrapper::isActive() const
{
	return status_ == Status::Running;
}

bool ThreadWrapper::isCanBeFinished() const
{
	return isCanBeFinished_;
}

void ThreadWrapper::setFinishCallback( std::function< void() > callback )
{
	finishCallback_ = callback;
}

ThreadWrapper::Status ThreadWrapper::getStatus() const
{
	return status_;
}

void ThreadWrapper::setStopMethod( StopMethod stopMethod ) noexcept( false )
{
	if ( stopMethod != StopMethod::Detach && stopMethod != StopMethod::Join )
		MAKE_THROW( "Invalid stop method" );

	stopMethod_ = stopMethod;
}

std::thread::id ThreadWrapper::getId() const noexcept( false )
{
	if ( !isActive() )
		MAKE_THROW( "Impossible to get ID in the stopped(or empty) thread." );

	return thread_->get_id();
}

bool ThreadWrapper::isHasError() const
{
	std::lock_guard lg( mutexExceptionString_ );
	return !exceptionString_.empty();
}

std::string ThreadWrapper::getExceptionString() const
{
	std::lock_guard lg( mutexExceptionString_ );
	return exceptionString_;
}
