#pragma once

#include "ThreadWrapper.h"

#include <vector>

class ThreadPool final
{
public:
	static ThreadPool* instance();

	~ThreadPool();
	ThreadPool( const ThreadPool& ) = delete;
	ThreadPool( ThreadPool&& ) = delete;
	ThreadPool& operator=( const ThreadPool& ) = delete;
	ThreadPool& operator=( ThreadPool&& ) = delete;

	void closeAllThreads();
	size_t pushAndStart( std::function< void() > function );
	size_t size();
	void update() noexcept( false );
	ThreadWrapper& at( size_t i ) noexcept( false );

private:
	ThreadPool();
	std::vector< std::unique_ptr< ThreadWrapper > > threads_;
};

