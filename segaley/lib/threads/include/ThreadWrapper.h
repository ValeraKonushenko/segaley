#pragma once

#include <functional>
#include <atomic>
#include <memory>
#include <thread>
#include <mutex>

class ThreadWrapper final
{
public:
	enum class Status
	{
		Running,
		Stopped
	};

	enum class StopMethod
	{
		Join,
		Detach
	};

public:
	ThreadWrapper( StopMethod stopMethod = StopMethod::Join );
	ThreadWrapper( std::function< void() > callback, StopMethod stopMethod = StopMethod::Join ) noexcept( false );
	~ThreadWrapper();
	ThreadWrapper( const ThreadWrapper& ) = delete;
	ThreadWrapper( ThreadWrapper&& ) = delete;
	ThreadWrapper& operator=( const ThreadWrapper& ) = delete;
	ThreadWrapper& operator=( ThreadWrapper&& ) = delete;

	ThreadWrapper& operator=( std::function< void() > callback ) noexcept( false );
	void setCallback( std::function< void() > callback ) noexcept( false );
	void start() noexcept( false );
	bool stop();
	bool isActive() const;
	bool isCanBeFinished() const;	
	void setFinishCallback( std::function< void() > callback );
	Status getStatus() const;
	void setStopMethod( StopMethod stopMethod ) noexcept( false );
	std::thread::id getId() const noexcept( false );
	bool isHasError() const;
	std::string getExceptionString() const;

private:
	std::atomic< Status > status_;
	std::atomic< StopMethod > stopMethod_;
	std::atomic_bool isCanBeFinished_;

	std::function< void() > finishCallback_;
	std::function< void() > callback_;
	std::function< void() > realCallback_;

	std::unique_ptr< std::thread > thread_;

	mutable std::mutex mutexExceptionString_;
	std::string exceptionString_;	
};