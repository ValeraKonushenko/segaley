#include "DebugConsole.h"

#include "Thrower.h"
#include "glm/glm.hpp"

#include <memory>
#include <iomanip>

DebugConsole* DebugConsole::instance() noexcept( false )
{
	static std::unique_ptr< DebugConsole > obj;
	if ( !obj )
	{
		obj = std::unique_ptr< DebugConsole >( new DebugConsole() );		
	}
	return obj.get();
}

void DebugConsole::print( std::ostream& out ) const noexcept( false )
{
	if ( out.bad() || out.fail() )
		MAKE_THROW( "Was recieved failed stream. Impossible to work with the broken stream." );

	if ( clearCallback_ )
		clearCallback_();

	printFps( out );
	printCamera( out );
	printLigting( out );
}

void DebugConsole::setClearCallback( std::function< void() > clearCallback )
{
	clearCallback_ = clearCallback;
}

void DebugConsole::printFps( std::ostream& out ) const
{
	out << "FPS: " << fps_.value << std::endl;
	out << "Avg FPS: " << fps_.avg << std::endl;
	out << "Min FPS: " << fps_.min << std::endl;
	out << "Max FPS: " << fps_.max << std::endl;
	out << std::endl;
}

void DebugConsole::printCamera( std::ostream& out ) const
{
	out << "======C A M E R A======" << std::endl;
	out << "FOW: " << glm::degrees( cameraProperty_.fow ) << std::endl;
	out << "Position: " << std::endl
		<< "\tX: " << cameraProperty_.cameraLocation.position.x << std::endl
		<< "\tY: " << cameraProperty_.cameraLocation.position.y << std::endl
		<< "\tZ: " << cameraProperty_.cameraLocation.position.z << std::endl;
	out << "Rotate: " << std::endl
		<< "\tPitch: " << glm::degrees( cameraProperty_.cameraLocation.rotate.pitch ) << std::endl
		<< "\tYaw:   " << glm::degrees( cameraProperty_.cameraLocation.rotate.yaw ) << std::endl;
	out << std::setprecision( 7 );
	out << "Speed: " << cameraProperty_.currentSpeed << std::endl;
	out << std::setprecision( 4 );
	out << std::endl;
}

void DebugConsole::printLigting( std::ostream& out ) const
{
	out << "======L I G H T I N G======" << std::endl;
	out << "Ambient:" << std::endl;
	out << "\tIntensity: " << lighting_.ambient.intensity << std::endl;
	out << "Diffuse:" << std::endl;
	out << "\tColor: RGB( " 
		<< lighting_.diffuse.color.r
		<< ", " << lighting_.diffuse.color.g
		<< ", " << lighting_.diffuse.color.b
		<< ")" << std::endl;
	out << "\tDirection: " << std::endl;
	out << "\t\tX: " << lighting_.diffuse.direction.x << std::endl;
	out << "\t\tY: " << lighting_.diffuse.direction.y << std::endl;
	out << "\t\tZ: " << lighting_.diffuse.direction.z << std::endl;
	out << "\tIntensity: " << lighting_.diffuse.intensity << std::endl;
	out << "\tMin lighting: " << lighting_.diffuse.minLighting << std::endl;
	out << std::endl;
}
