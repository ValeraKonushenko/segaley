#include "DebugActions.h"

#include "Thrower.h"

#include <filesystem>

void DebugActions::copyAssetsToBinaryDir() noexcept( false )
{
	using namespace std::string_literals;

	static const std::filesystem::path src = ASSETS_SRC_PATH;
	static const std::filesystem::path dst = "./"s + ASSETS_DIR_NAME + "/";

	if ( !std::filesystem::exists( src ) )
		MAKE_THROW( "The directory with assets can not be found" );

	std::filesystem::create_directory( dst );
	std::filesystem::copy(
							src,
							dst,
							std::filesystem::copy_options::recursive |
							std::filesystem::copy_options::overwrite_existing
	);
}

void DebugActions::copyConfigsToBinaryDir() noexcept( false )
{
	using namespace std::string_literals;

	static const std::filesystem::path src = CONFIGS_SRC_PATH;
	static const std::filesystem::path assetsDirName = "./"s + ASSETS_DIR_NAME + "/";
	static const std::filesystem::path dst = "./"s + CONFIGS_DIR_NAME + "/";

	if ( !std::filesystem::exists( src ) )
		MAKE_THROW( "The directory with configs can not be found" );

	std::filesystem::create_directory( assetsDirName );
	std::filesystem::copy(
							src,
							dst,
							std::filesystem::copy_options::recursive |
							std::filesystem::copy_options::overwrite_existing
	);
}

void DebugActions::launch() noexcept( false )
{
	copyAssetsToBinaryDir();
	copyConfigsToBinaryDir();
}
