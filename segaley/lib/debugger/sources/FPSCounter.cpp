#include "FPSCounter.h"
#include "Thrower.h"

#include <iostream>

FPSCounter::FPSCounter( double frequent, func_t action_f ) noexcept( false )
{
    frequentMinMaxUpdate_ = 10.0;
    setAction( action_f );
    setFrequent( frequent );
}

void FPSCounter::setAction( func_t action_f )
{
    callActionFunc_ = action_f;
}

void FPSCounter::setFrequent( double frequent ) noexcept( false )
{
    if( frequent <= 0.00001 )
        MAKE_THROW( "The frequent of calculate of FPS can't be less the 0.00001 per second" );
    frequent_ = frequent;
}

double FPSCounter::getFrequent() const
{
    return frequent_;
}

void FPSCounter::execute() noexcept( false )
{
    if( !callActionFunc_ )
        MAKE_THROW( "You should set a function-event-listener" );

    using namespace std::chrono;
    
    duration< double > duration = system_clock::now() - last_;

    if( duration.count() >= frequent_ )
    {
        callActionFunc_( calculateFps() );
        last_ = system_clock::now();
        fps_.value = 0u;
    }

    fps_.value++;
}

void FPSCounter::setFrequentMinMaxUpdate( double frequent ) noexcept( false )
{
    if ( frequent <= 0.00001 )
        MAKE_THROW( "The frequent of calculate of FPS can't be less the 0.00001 per second" );

    frequentMinMaxUpdate_ = frequent;
}

double FPSCounter::getFrequentMinMaxUpdate() const
{
    return frequentMinMaxUpdate_;
}

Fps FPSCounter::calculateFps() const
{
    Fps out;

    out.value = static_cast< size_t >( fps_.value / frequent_ );

    calculateMinMax( out );
    calculateAvg( out );   

    return out;
}

void FPSCounter::calculateMinMax( Fps& fps ) const
{
    using namespace std::chrono;
    static size_t minFps = ~0u;
    static size_t maxFps = 0u;

    static auto lastUpdate = system_clock::now();
    duration< double > duration = system_clock::now() - lastUpdate;
    if ( duration.count() >= frequentMinMaxUpdate_ )
    {
        minFps = ~0u;
        maxFps = 0u;
        lastUpdate = system_clock::now();
    }

    if ( fps.value < minFps )
        minFps = fps.value;
    fps.min = minFps;

    if ( fps.value > maxFps )
        maxFps = fps.value;
    fps.max = maxFps;
}

void FPSCounter::calculateAvg( Fps& fps ) const
{
    static unsigned long long accum = 0ull;
    static unsigned long long counter = 0ull;
    if ( accum >= ( ~0ull - 10'000 ) )
        accum = 0ull, counter = 0ull;

    accum += fps.value;
    counter++;

    fps.avg = accum / counter;
}
