#pragma once

#include <chrono>
#include <functional>
#include <stdexcept>

struct Fps
{
    size_t value = 0u;
    size_t avg = 0u;
    size_t max = 0u;
    size_t min = 0u;
};

class FPSCounter final
{
public:
    using func_t = std::function<void( Fps )>;
    using time_point = std::chrono::system_clock::time_point;

    FPSCounter( double frequent = 1., func_t action_f = nullptr ) noexcept( false );

    FPSCounter( const FPSCounter& ) = default;
    FPSCounter( FPSCounter&& ) = default;
    FPSCounter& operator=( const FPSCounter& ) = default;
    FPSCounter& operator=( FPSCounter&& ) = default;

    ~FPSCounter() = default;

    void        setAction( func_t action_f );
    void        setFrequent( double frequent ) noexcept( false );
    double      getFrequent() const;
    void        execute() noexcept( false );
    void        setFrequentMinMaxUpdate( double frequent ) noexcept( false );
    double      getFrequentMinMaxUpdate() const;

private:
    Fps calculateFps() const;
    void calculateMinMax( Fps& fps ) const;
    void calculateAvg( Fps& fps ) const;

    func_t callActionFunc_;
    time_point last_;
    Fps fps_;
    double frequent_;
    double frequentMinMaxUpdate_;
};