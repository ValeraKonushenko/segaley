#pragma once
#include "FPSCounter.h"
#include "Camera.h"
#include "Lighting.h"

#include <ostream>
#include <functional>

class DebugConsole final
{
public:
	~DebugConsole() = default;
	DebugConsole( const DebugConsole& ) = delete;
	DebugConsole( DebugConsole&& ) = delete;
	DebugConsole& operator=( const DebugConsole& ) = delete;
	DebugConsole& operator=( DebugConsole&& ) = delete;

	static DebugConsole* instance() noexcept( false );
	Fps fps_;
	Camera::Property cameraProperty_;
	Lighting lighting_;
	void print( std::ostream& out ) const noexcept( false );

	void setClearCallback( std::function< void() > clearCallback );

private:
	DebugConsole() = default;
	void printFps( std::ostream& out ) const;
	void printCamera( std::ostream& out ) const;
	void printLigting( std::ostream& out ) const;

	std::function< void() > clearCallback_;
};