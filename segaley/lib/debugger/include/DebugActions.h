#pragma once

class DebugActions final
{
public:
	DebugActions() = delete;

	static void copyAssetsToBinaryDir()noexcept( false );
	static void copyConfigsToBinaryDir()noexcept( false );
	static void launch() noexcept( false );
private:

};