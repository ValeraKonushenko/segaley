#pragma once
#include "glm/glm.hpp"
#include "Color.h"

#include <string>

class Material
{
public:
	Material() = default;
	virtual ~Material() = default;
	Material( const Material& ) = default;
	Material( Material&& ) = default;
	Material& operator=( const Material& ) = default;
	Material& operator=( Material&& ) = default;

	static void throwIfInvalid( const Material& material ) noexcept( false );

	struct Specular final
	{
		static bool isValidPowder( float powder );
		static bool isValidIntensity( float intensity );

		float powder = 0.f;
		float intensity = 0.f;
		RGBf color;
	} specular;

	std::string name;

	bool isEmpty();
	void setMaterial( const char* name ) noexcept( false );
};