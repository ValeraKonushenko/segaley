#pragma once

#include "Material.h"

#include <string>
#include <map>

class MaterialManager final
{
public:
	static const std::string pathToMaterialsPresets;

public:
	~MaterialManager() = default;
	MaterialManager( const MaterialManager& ) = delete;
	MaterialManager( MaterialManager&& ) = delete;
	MaterialManager& operator=( const MaterialManager& ) = delete;
	MaterialManager& operator=( MaterialManager&& ) = delete;

	static MaterialManager* instance();
	void loadMaterials() noexcept( false );

	Material& operator[]( const char* name ) noexcept( false );
	Material operator[]( const char* name ) const noexcept( false );

private:
	MaterialManager() = default;

	std::map< std::string, Material > materials_;
};