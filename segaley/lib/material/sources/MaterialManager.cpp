#include "MaterialManager.h"

#include "json.hpp"
#include "Thrower.h"
#include "utils.h"

#include <fstream>
#include <memory>
#include <string>

const std::string MaterialManager::pathToMaterialsPresets = "configs/materialPresets.json";//TODO: move to settings

MaterialManager* MaterialManager::instance()
{
	static std::unique_ptr< MaterialManager > ptr = nullptr;

	if ( !ptr )
		ptr = std::unique_ptr< MaterialManager >( new MaterialManager );

	return ptr.get();
}

void MaterialManager::loadMaterials() noexcept( false )
{
	auto file = utils::readFile( MaterialManager::pathToMaterialsPresets );
	auto json = nlohmann::json::parse( file );
	
	for ( auto& item : json.items() )
	{
		Material material;
		material.name = item.key();
		material.specular.powder = json[ item.key() ][ "specular" ][ "powder" ];
		material.specular.intensity = json[ item.key() ][ "specular" ][ "intensity" ];
		{
			RGB rgb;
			rgb.r = json[ item.key() ][ "specular" ][ "color" ][ "r" ];
			rgb.g = json[ item.key() ][ "specular" ][ "color" ][ "g" ];
			rgb.b = json[ item.key() ][ "specular" ][ "color" ][ "b" ];
			material.specular.color = Color::convertToF( rgb );
		}
		Material::throwIfInvalid( material );
		materials_.insert( std::pair( item.key(), material ) );
	}
}

Material& MaterialManager::operator[]( const char* name ) noexcept( false )
{
	auto material = materials_.find( name );
	if ( material == materials_.end() )
		MAKE_THROW( "No such element in material presets" );

	return ( *material ).second;
}

Material MaterialManager::operator[]( const char* name ) const noexcept( false )
{
	auto material = materials_.find( name );
	if ( material == materials_.end() )
		MAKE_THROW( "No such element in material presets" );

	return ( *material ).second;
}
