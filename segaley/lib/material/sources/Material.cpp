#include "Material.h"

#include "GLM/gtx/string_cast.hpp"
#include "Thrower.h"
#include "MaterialManager.h"

bool Material::Specular::isValidPowder( float powder )
{
	if ( powder < 0.f )
		return false;

	return true;
}

bool Material::Specular::isValidIntensity( float intensity )
{
	if ( intensity < 0.f )
		return false;

	return true;
}

void Material::throwIfInvalid( const Material& material ) noexcept( false )
{
	if ( !Material::Specular::isValidPowder( material.specular.powder ) )
		MAKE_THROW( "Invalid material data at:  " + material.name + "->specular->powder value: " + 
			std::to_string( material.specular.powder ) );

	if ( !Material::Specular::isValidIntensity( material.specular.intensity ) )
		MAKE_THROW( "Invalid material data at:  " + material.name + "->specular->intensity value: " + 
			std::to_string( material.specular.intensity ) );

	if ( !material.specular.color.isValid() )
		MAKE_THROW( "Invalid material data at: " + material.name + "->specular->color value: " + 
			Color::toString( material.specular.color ) );
}

bool Material::isEmpty()
{
	return name.empty();
}

void Material::setMaterial( const char* name ) noexcept( false )
{
	auto& m = *MaterialManager::instance();
	*this = m[ name ];
}
