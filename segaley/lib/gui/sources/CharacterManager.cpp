#include "CharacterManager.h"

#include "FontFaceManager.h"
#include "Gl.h"
#include "Thrower.h"
#include "Shaders.h"
#include "Window.h"

#include <memory>

namespace gui
{

CharacterManager& CharacterManager::instance() noexcept( false )
{
	static std::unique_ptr< CharacterManager > obj;
	if ( !obj )
	{
		obj = std::unique_ptr< CharacterManager >( new CharacterManager );
		obj->init();
	}

	return *obj;
}

void CharacterManager::init() noexcept( false )
{
	auto& ft = FontFaceManager::instance();
	auto font = ft.createFont( "assets/fonts/Roboto-Regular.ttf" );
	font.setSize( 100 );
    generatePull( font );
}

Character& CharacterManager::get( char ch ) noexcept( false )
{
    return chars_.at( ch );
}

const Character& CharacterManager::get( char ch ) const noexcept( false )
{
    return chars_.at( ch );
}

Character& CharacterManager::operator[]( char ch ) noexcept( false )
{
    return get( ch );
}

const Character& CharacterManager::operator[]( char ch ) const noexcept( false )
{
    return get( ch );
}

void CharacterManager::generateOneChar( FontFace& font, char ch )
{
    font.loadChar( ch );
    auto glyph = font.glyph();
    
    Texture texture;
    texture.setIsDestroyAtEnd( false );
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RED,
        glyph->bitmap.width,
        glyph->bitmap.rows,
        0,
        GL_RED,
        GL_UNSIGNED_BYTE,
        glyph->bitmap.buffer
    );
    // set texture options
    Texture::setMagFilter( Texture::MagFilter::Linear );
    Texture::setMinFilter( Texture::MinFilter::Linear );
    Texture::setWrapS( Texture::Wrap::Clamp2Edge );
    Texture::setWrapT( Texture::Wrap::Clamp2Edge );
    // now store character for later use
    gui::Character character = {
        texture.data(),
        glm::ivec2( glyph->bitmap.width, glyph->bitmap.rows ),
        glm::ivec2( glyph->bitmap_left, glyph->bitmap_top ),
        static_cast< unsigned int >( glyph->advance.x )
    };
    
    chars_.insert( { font.getLoadedChar(), character } );
}

void CharacterManager::generatePull( FontFace& font )
{
    // disable byte-alignment restriction
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    for ( int ch = 0; ch < 128; ch++ )
        generateOneChar( font, static_cast< char >( ch ) );
    
    Gl::Vao::generate( 1, &Vao );
    Gl::Vbo::generate( 1, &VBO );
    Gl::Vao::bind( Vao );
    Gl::Vbo::bind( GL_ARRAY_BUFFER, VBO );
    Gl::Vbo::data( GL_ARRAY_BUFFER, sizeof( float ) * 6 * 4, NULL, GL_DYNAMIC_DRAW );
    Gl::enableVertexAttribArray( 0 );
    Gl::vertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof( float ), 0 );
    Gl::Vbo::reset( GL_ARRAY_BUFFER );
    Gl::Vao::reset();
}

void CharacterManager::drawText( const Text& str, const Window& window, ShaderPack& shaderPack, glm::vec2 position, RGB color ) noexcept( false )
{
    auto width = static_cast< float >( window.getSize().width );
    auto height = static_cast< float >( window.getSize().height );
    auto projection = glm::ortho( 0.0f, width, 0.0f, height );

    auto scale = str.calcScale();

    auto& shader = shaderPack.gui;

    shader.use();
    shader.shader.uniform( "uTextColor", Color::convertToF( color ) );
    shader.shader.uniform( "uProjection", projection );

    Texture::active( 0 );
    Gl::Vao::bind( CharacterManager::instance().Vao );

    for ( auto& c : str )
    {
        auto& ch = CharacterManager::instance().get( c );

        float xpos = position.x + ch.bearing.x * scale;
        float ypos = position.y - ( ch.size.y - ch.bearing.y ) * scale;

        float w = ch.size.x * scale;
        float h = ch.size.y * scale;
        // update VBO for each character
        float vertices[ 6 ][ 4 ] = {
            { xpos,     ypos + h,   0.0f, 0.0f },
            { xpos,     ypos,       0.0f, 1.0f },
            { xpos + w, ypos,       1.0f, 1.0f },

            { xpos,     ypos + h,   0.0f, 0.0f },
            { xpos + w, ypos,       1.0f, 1.0f },
            { xpos + w, ypos + h,   1.0f, 0.0f }
        };
        // render glyph texture over quad
        glBindTexture( GL_TEXTURE_2D, ch.textureId );
        // update content of VBO memory
        Gl::Vbo::bind( GL_ARRAY_BUFFER, CharacterManager::instance().VBO );
        Gl::Vbo::subData( GL_ARRAY_BUFFER, 0, sizeof( vertices ), vertices );
        Gl::Vbo::reset( GL_ARRAY_BUFFER );
        // render quad
        glDrawArrays( GL_TRIANGLES, 0, 6 );
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        position.x += ( ch.offset >> 6 ) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }
    Gl::Vao::reset();
    glBindTexture( GL_TEXTURE_2D, 0 );
}

}