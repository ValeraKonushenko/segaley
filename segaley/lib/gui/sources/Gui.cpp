#include "Gui.h"

#include "Thrower.h"
#include "FontFaceManager.h"
#include "Gl.h"
#include "Window.h"

#include <memory>
#include <filesystem>

namespace gui
{

Gui::Gui( Window& window ) :
    window_( window )
{
}

void Gui::prepare() noexcept( false )
{
	CharacterManager::instance();
}

}