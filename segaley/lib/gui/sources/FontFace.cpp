#include "FontFace.h"

#include "FontFaceManager.h"
#include "Thrower.h"
#include "Gl.h"
#include "CharacterManager.h"

#include <memory>

namespace
{

void throwIfHasError( FT_Error error, const std::string& message ) noexcept( false )
{
	if ( static_cast< unsigned int >( error ) )
	{
		auto* msg = FT_Error_String( error );
		MAKE_THROW( message + " Details: " + msg );
	}
}

}

FontFace::~FontFace()
{
	release();//TODO: process an possible exception
}

FontFace::FontFace( FontFace&& other ) noexcept( false )
{
	*this = std::move( other );
}

FontFace& FontFace::operator=( FontFace&& other ) noexcept( false )
{
	face_ = other.face_;
	width_ = other.width_;
	height_ = other.height_;

	other.face_ = nullptr;
	other.height_ = 0u;
	other.width_ = 0u;

	return *this;
}

void FontFace::setSize( size_t width, size_t height ) noexcept( false )
{
	width_ = width;
	height_ = height;

	auto error = FT_Set_Pixel_Sizes( face_, static_cast< FT_UInt >( width ), static_cast< FT_UInt >( height ) );
	throwIfHasError( error, "ERROR::FREETYPE: Failed to set pixel size." );
}

void FontFace::setSize( size_t height ) noexcept( false )
{
	setSize( 0, height );
}

size_t FontFace::getWidth() const
{
	return width_;
}

size_t FontFace::getHeight() const
{
	return height_;
}

void FontFace::loadChar( char ch ) noexcept( false )
{
	auto error = FT_Load_Char( face_, ch, FT_LOAD_RENDER );
	throwIfHasError( error, "ERROR::FREETYTPE: Failed to load Glyph." );
	loadedChar_ = ch;
}

const FT_GlyphSlot FontFace::glyph() const noexcept( false )
{
	if ( isEmpty() )
		MAKE_THROW( "ERROR:FREETYPE: Impossible to get a glyph from the empty object." );

	return face_->glyph;
}

bool FontFace::isEmpty() const
{
	return !face_;
}

char FontFace::getLoadedChar() const
{
	return loadedChar_;
}

FontFace::FontFace( std::filesystem::path path ) noexcept( false )
{
	initialize( path );
}

void FontFace::initialize( std::filesystem::path path ) noexcept( false )
{
	auto& fontManager = FontFaceManager::instance();

	if ( face_ )
		MAKE_THROW( "ERROR::FREETYPE: Impossible to reinit the font-face." );

	auto error = FT_New_Face( fontManager.library_, path.string().c_str(), 0, &face_ );
	throwIfHasError( error, "ERROR::FREETYPE: Failed to load font." );
}

void FontFace::release() noexcept( false )
{
	if ( !face_ )
		return;

	auto error = FT_Done_Face( face_ );
	throwIfHasError( error, "ERROR::FREETYTPE: Failed to done a face." );
	loadedChar_ = 0;
	width_ = height_ = 0u;
	face_ = nullptr;
}