#include "FontFaceManager.h"

#include "Thrower.h"

#include <memory>

FontFaceManager& FontFaceManager::instance() noexcept( false )
{
	static std::unique_ptr< FontFaceManager > obj;

	if ( !obj )
	{
		obj = std::unique_ptr< FontFaceManager >( new FontFaceManager );
		obj->initialization();
	}

	return *obj;
}

FontFaceManager::~FontFaceManager()
{
	release();
}

FontFace FontFaceManager::createFont( std::filesystem::path path )
{
	FontFace font( path );
	return std::move( font );
}

void FontFaceManager::initialization() noexcept( false )
{
	if ( library_ )
		MAKE_THROW( "ERROR::FREETYPE: Impossible to reinit the FT library." );

	if ( auto error = FT_Init_FreeType( &library_ ); static_cast< unsigned int >( error ) )
	{
		std::string msg = FT_Error_String( error );
		MAKE_THROW( "ERROR::FREETYPE: Could not init FreeType Library. Details: " + msg );
	}
}

void FontFaceManager::release()
{
	if ( !library_ )
		MAKE_THROW( "ERROR::FREETYPE: Impossible to rerelease the FT library." );

	if ( auto error = FT_Done_FreeType( library_ ); static_cast< unsigned int >( error ) )
	{
		std::string msg = FT_Error_String( error );
		MAKE_THROW( "ERROR::FREETYTPE: Failed to done the free type library. Details: " + msg );
	}
}