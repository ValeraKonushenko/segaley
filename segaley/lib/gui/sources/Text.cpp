#include "Text.h"

#include "CharacterManager.h"

namespace gui
{

Text::Text( const char* str ) noexcept( false ) :
	std::string( str )
{
}

size_t Text::getWidth() const
{
	size_t width = 0u;

	auto& ch = CharacterManager::instance();

	

	return width;
}

void Text::setFontSize( size_t size )
{
	fontSize_ = size;
}

size_t Text::getFontSize() const
{
	return fontSize_;
}

float Text::calcScale( const Text& text )
{
	return static_cast< float >( text.getFontSize() ) / 100.f;
}

float Text::calcScale() const
{
	return calcScale( *this );
}

}