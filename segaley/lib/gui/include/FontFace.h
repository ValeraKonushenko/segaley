#pragma once

#include <filesystem>

#include "ft2build.h"
#include FT_FREETYPE_H
#include "CharacterManager.h"

class FontFace
{
public:
	~FontFace();
	FontFace( FontFace&& ) noexcept( false );
	FontFace& operator=( FontFace&& ) noexcept( false );
	FontFace( const FontFace& ) = delete;
	FontFace& operator=( const FontFace& ) = delete;

	void setSize( size_t height ) noexcept( false );
	size_t getWidth() const;
	size_t getHeight() const;

	void loadChar( char ch ) noexcept( false );

	const FT_GlyphSlot glyph() const noexcept( false );
	bool isEmpty() const;
	char getLoadedChar() const;

private:
	void setSize( size_t width, size_t height ) noexcept( false );
	FontFace( std::filesystem::path path ) noexcept( false );
	void initialize( std::filesystem::path path ) noexcept( false );
	void release() noexcept( false );

private:
	char loadedChar_ = 0;
	size_t width_ = 0u;
	size_t height_ = 24u;
	FT_Face face_ = nullptr;
	friend class FontFaceManager;
};