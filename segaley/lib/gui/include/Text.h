#pragma once

#include <string>

namespace gui
{

class Text final : public std::string
{
public:
	Text() = default;
	Text( const char* str ) noexcept( false );
	~Text() = default;
	Text( Text&& ) = default;
	Text& operator=( Text&& ) = default;

	size_t getWidth() const;
	void setFontSize( size_t size );
	size_t getFontSize() const;

	static float calcScale( const Text& text );
	float calcScale() const;

private:
	size_t fontSize_ = 24u;
};

}