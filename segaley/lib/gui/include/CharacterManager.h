#pragma once

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Texture.h"
#include "Color.h"
#include "Text.h"

#include <map>
#include <vector>

class FontFace;
class ShaderPack;
class Window;

namespace gui
{

struct Character
{
	GLuint textureId = Texture::invalidId;
	glm::ivec2 size = glm::ivec2( 0, 0 );
	glm::ivec2 bearing = glm::ivec2( 0, 0 );
	unsigned int offset = 0u;
};

class CharacterManager
{
public:
	CharacterManager( Character&& ) = delete;
	CharacterManager& operator=( CharacterManager&& ) = delete;

	static CharacterManager& instance() noexcept( false );
	Character& get( char ch ) noexcept( false );
	const Character& get( char ch ) const noexcept( false );
	Character& operator[]( char ch ) noexcept( false );
	const Character& operator[]( char ch ) const noexcept( false );

	void generateOneChar( FontFace& font, char ch );
	void generatePull( FontFace& font );

	void drawText( const Text& str, const Window& window, ShaderPack& shaderPack, glm::vec2 position, RGB color = { 0 } ) noexcept( false );

private:
	unsigned int Vao, VBO;
	void init() noexcept( false );

	std::map< char, Character > chars_;

	CharacterManager() = default;
};

}