#pragma once

#include "ft2build.h"
#include FT_FREETYPE_H
#include "FontFace.h"

class FontFaceManager final
{
public:
	static FontFaceManager& instance() noexcept( false );

	~FontFaceManager();
	FontFaceManager( const FontFaceManager&& ) = delete;
	FontFaceManager& operator=( const FontFaceManager&& ) = delete;

	FontFace createFont( std::filesystem::path path );

private:
	FontFaceManager() = default;
	void initialization() noexcept( false );
	void release();

private:
	FT_Library library_ = nullptr;

	friend class FontFace;
};