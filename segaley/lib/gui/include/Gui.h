#pragma once

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Shaders.h"
#include "Texture.h"
#include "Text.h"

#include <map>

class Window;

namespace gui
{

class Gui final
{
public:
	Gui( Window& window );

	Gui( Gui&& ) = delete;
	Gui& operator=( Gui&& ) = delete;

	void drawText( const Text& str, ShaderPack& shaderPack, glm::vec2 position, RGB color = { 0 } ) noexcept( false );
	void prepare() noexcept( false );
private:
	Window& window_;
};

}