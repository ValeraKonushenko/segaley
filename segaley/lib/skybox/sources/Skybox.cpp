#include "Skybox.h"

#include "Settings.h"
#include "ShaderProgram.h"
#include "ThreadPool.h"

Skybox::Skybox() :
	glTexture_( Texture::Target::CubeMap )
{
}

void Skybox::prepareGLObjects() noexcept( false )
{
    const float size = 1.0f;
    std::vector< VertexData > skyboxVertices = {
        //front
        VertexData( { -size,  size, -size } ),
        VertexData( { -size, -size, -size } ),
        VertexData( {  size, -size, -size } ),
        VertexData( {  size, -size, -size } ),
        VertexData( {  size,  size, -size } ),
        VertexData( { -size,  size, -size } ),
        //left
        VertexData( { -size, -size,  size } ),
        VertexData( { -size, -size, -size } ),
        VertexData( { -size,  size, -size } ),
        VertexData( { -size,  size, -size } ),
        VertexData( { -size,  size,  size } ),
        VertexData( { -size, -size,  size } ),
        //right
        VertexData( {  size, -size, -size } ),
        VertexData( {  size, -size,  size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( {  size,  size, -size } ),
        VertexData( {  size, -size, -size } ),
        //back
        VertexData( { -size, -size,  size } ),
        VertexData( { -size,  size,  size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( {  size, -size,  size } ),
        VertexData( { -size, -size,  size } ),
        //top
        VertexData( { -size,  size, -size } ),
        VertexData( {  size,  size, -size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( {  size,  size,  size } ),
        VertexData( { -size,  size,  size } ),
        VertexData( { -size,  size, -size } ),
        //bottom
        VertexData( { -size, -size, -size } ),
        VertexData( { -size, -size,  size } ),
        VertexData( {  size, -size, -size } ),
        VertexData( {  size, -size, -size } ),
        VertexData( { -size, -size,  size } ),
        VertexData( {  size, -size,  size } )
    };

    skybox_.prepare();
    skybox_.setVertices( skyboxVertices );
    skybox_.setPosition( glm::vec3( size * -0.5f ) );
}

void Skybox::loadData() noexcept( false )
{
    auto settings = Settings::instance();

    texture_.loadImage( settings->paths.skybox.toPx );
    texture_.loadToGpu( Texture::Target::CubeMapPositiveX );
    texture_.loadImage( settings->paths.skybox.toNx );
    texture_.loadToGpu( Texture::Target::CubeMapNegativeX );

    texture_.loadImage( settings->paths.skybox.toNy );
    texture_.loadToGpu( Texture::Target::CubeMapPositiveY );
    texture_.loadImage( settings->paths.skybox.toPy );
    texture_.loadToGpu( Texture::Target::CubeMapNegativeY );
    
    texture_.loadImage( settings->paths.skybox.toPz );
    texture_.loadToGpu( Texture::Target::CubeMapPositiveZ );
    texture_.loadImage( settings->paths.skybox.toNz );
    texture_.loadToGpu( Texture::Target::CubeMapNegativeZ );
}

void Skybox::preSetUpShader() noexcept( false )
{
    Texture::setMagFilter( Texture::MagFilter::Linear, Texture::Target::CubeMap );
    Texture::setMinFilter( Texture::MinFilter::Linear, Texture::Target::CubeMap );
    Texture::setWrapS( Texture::Wrap::Clamp2Edge, Texture::Target::CubeMap );
    Texture::setWrapT( Texture::Wrap::Clamp2Edge, Texture::Target::CubeMap );
    Texture::setWrapR( Texture::Wrap::Clamp2Edge, Texture::Target::CubeMap );
}

void Skybox::prepare()
{
    prepareGLObjects();
    loadData();
    preSetUpShader();
}

void Skybox::draw( DrawData drawData ) noexcept( false )
{
    drawData.shaderProgram.use();

    //TODO: move it to shader data and store it there
    static auto uSkybox = drawData.shaderProgram.getUniformLocation( "uSkybox" );

    drawData.shaderProgram.uniform( uSkybox, 0 );
    glCullFace( GL_BACK );

    glDepthFunc( GL_LEQUAL );//???????????????????
    skybox_.draw( drawData );
    glDepthFunc( GL_LESS );

    glCullFace( GL_FRONT );//Back old value
}
