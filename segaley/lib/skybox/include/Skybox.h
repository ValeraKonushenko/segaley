#pragma once

#include "DrawableObj.h"
#include "Texture.h"
#include "Texture2D.h"

class Skybox final
{
public:
	Skybox();
	~Skybox() = default;
	Skybox( const Skybox& ) = delete;
	Skybox( Skybox&& ) = delete;

	void prepare();

	void draw( DrawData drawData ) noexcept( false );

private:
	void prepareGLObjects() noexcept( false );
	void loadData() noexcept( false );
	void preSetUpShader() noexcept( false );

	DrawableObj skybox_;
	Texture glTexture_;//TODO: unit it to common class
	Texture2D texture_;//TODO: unit it to common class
};
