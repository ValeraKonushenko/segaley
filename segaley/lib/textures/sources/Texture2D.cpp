#include "Texture2D.h"
#include "Texture2D.h"
#include "Thrower.h"
#include "Image.h"

Texture2D::Format Texture2D::fromChannel2Format( Image::Channel channel ) noexcept( false )
{
	if ( channel == Image::Channel::RGB )
		return Format::RGB;
	if ( channel == Image::Channel::RGBA )
		return Format::RGBA;

	MAKE_THROW( "Invalid channel or just GL does not support such format" );
	
	return Format::None;
}

Texture2D::Texture2D( std::string_view path ) noexcept( false )
{
	loadImage( path );
}

Texture2D::~Texture2D()
{
	clear();
}

void Texture2D::resetImage()
{
	image_.reset();
}

void Texture2D::setImage( Image&& image ) noexcept( false )
{
	if ( image_ )
		image_->clear();
	image_ = std::move( image );
}

Image& Texture2D::getImage() noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible to return not existing value. The image hadn't been set or was reseted" );

	return *image_;
}

const Image& Texture2D::getImage() const
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible to return not existing value. The image hadn't been set or was reseted" );

	return *image_;
}

int Texture2D::getWidth() const noexcept( false )
{
	if ( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->getWidth();
}

int Texture2D::getHeight() const noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->getHeight();
}

glm::ivec2 Texture2D::getSize() const noexcept( false )
{
	if ( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->getSize();
}

Image::Channel Texture2D::getChannel() const noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->getChannel();
}

unsigned char* Texture2D::getImageData() noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->data();
}

const unsigned char* Texture2D::getImageData() const noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible get any data from NULL image. Set image and try again" )

	return image_->data();
}

void Texture2D::loadImage( std::string_view path ) noexcept( false )
{
	Image image;
	image.loadImage( path );
	setImage( std::move( image ) );
}

void Texture2D::clear()
{
	if ( image_ )
		image_->clear();
}

bool Texture2D::isEmpty() const
{
	if ( !image_ )
		return true;
	return ( *image_ ).isEmpty();
}

void Texture2D::loadToGpu( Texture::Target target, GLint lvl, Format internalFormat, Format format ) noexcept( false )
{
	if( !image_.has_value() )
		MAKE_THROW( "Impossible to put not existing texture to the GPU."
					" The image hadn't been set or was reseted" );

	glTexImage2D( 
					static_cast< GLenum >( target ), 
					lvl, 
					static_cast< GLint >( internalFormat ),
					image_->getWidth(),
					image_->getHeight(),
					0,
					static_cast< GLenum >( Texture2D::fromChannel2Format( image_->getChannel() ) ),
					GL_UNSIGNED_BYTE,
					image_->data() 
				);
	glGenerateMipmap( GL_TEXTURE_2D );
}
