#include <Windows.h>

#include "TextureAtlas.h"

#include "Image.h"
#include "Settings.h"
#include "ThreadPool.h"

#include <iostream>

#define OPTIMIZATION 2

void TextureAtlas::setUpShaderForDiffuseMap( ShaderProgram& shaderProgram ) noexcept( false )
{
	shaderProgram.uniform( "textureAtlas", 0 );

#if OPTIMIZATION == 1
	if ( diffuseAtlas_.isEmpty() )
		MAKE_THROW( "Impossible to get a size of an empty texture" );

	shaderProgram.uniform( "uAtlasSize", diffuseAtlas_.getSize() );
#else if OPTIMIZATION == 2
	if ( diffuseAtlas_.isEmpty() )
		shaderProgram.uniform( "uAtlasSize", glm::ivec2( 0, 0 ) );
	else
		shaderProgram.uniform( "uAtlasSize", diffuseAtlas_.getSize() );
#endif
}

#undef OPTIMIZATION

void TextureAtlas::setUpShaderForSpecularMap( ShaderProgram& shaderProgram ) noexcept( false )
{
	shaderProgram.uniform( "textureSpecularAtlas", 1 );
}

void TextureAtlas::asyncLoadData() noexcept( false )
{
	asyncLoadSpecularMap();
	asyncLoadDiffuseMap();
}

void TextureAtlas::asyncLoadDiffuseMap() noexcept( false )
{
	auto threadPool = ThreadPool::instance();
	threadPool->pushAndStart( [ this ]()
	{
		loadDiffuseMap();
	} );
}

void TextureAtlas::asyncLoadSpecularMap() noexcept( false )
{
	auto threadPool = ThreadPool::instance();
	threadPool->pushAndStart( [ this ]()
	{
		loadSpecularMap();
	} );
}

void TextureAtlas::tryToLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false )
{
	shaderProgram.use();
	tryDiffuseMapLoadToGpu( shaderProgram );
	trySpecularMapLoadToGpu( shaderProgram );
}

void TextureAtlas::tryDiffuseMapLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false )
{
	if ( isReadyDiffuseMap_ )
	{
		loadDiffuseMapToGpu();
		setUpShaderForDiffuseMap( shaderProgram );
	}
}

void TextureAtlas::trySpecularMapLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false )
{
	if ( isReadySpecularMap_ )
	{
		loadSpecularMapToGpu();				
		setUpShaderForSpecularMap( shaderProgram );
	}
}

void TextureAtlas::loadDiffuseMap() noexcept( false )
{
	auto settings = Settings::instance();
	isReadyDiffuseMap_ = false;
	diffuseAtlas_.loadImage( settings->paths.toAtlas );
	isReadyDiffuseMap_ = true;
}

void TextureAtlas::loadDiffuseMapToGpu() noexcept( false )
{
	if ( diffuseAtlas_.isEmpty() )
		MAKE_THROW( "Impossible to load an empty texture" );

	Texture::active( 0u );
	diffuseAtlas_.loadToGpu();
	isReadyDiffuseMap_ = false;
}

void TextureAtlas::loadSpecularMap() noexcept( false )
{
	auto settings = Settings::instance();
	isReadySpecularMap_ = false;
	specularAtlas_.loadImage( settings->paths.toAtlasSpecularMap );
	isReadySpecularMap_ = true;
}

void TextureAtlas::loadSpecularMapToGpu() noexcept( false )
{
	if ( specularAtlas_.isEmpty() )
		MAKE_THROW( "Impossible to load an empty texture" );

	Texture::active( 1u );
	specularAtlas_.loadToGpu();
	isReadySpecularMap_ = false;
}
