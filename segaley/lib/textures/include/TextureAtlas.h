#pragma once

#include "Texture2D.h"
#include "Texture.h"
#include "ShaderProgram.h"
#include "ThreadWrapper.h"

#include <future>
#include <atomic>
#include <memory>
#include <optional>

class TextureAtlas final
{
public:
	TextureAtlas() = default;
	~TextureAtlas() = default;
	TextureAtlas( const TextureAtlas& ) = delete;
	TextureAtlas( TextureAtlas&& ) = delete;
	TextureAtlas& operator=( const TextureAtlas& ) = delete;
	TextureAtlas& operator=( TextureAtlas&& ) = delete;

	void setUpShaderForDiffuseMap( ShaderProgram& shaderProgram ) noexcept( false );
	void setUpShaderForSpecularMap( ShaderProgram& shaderProgram ) noexcept( false );

	void asyncLoadData() noexcept( false );
	void asyncLoadDiffuseMap() noexcept( false );
	void asyncLoadSpecularMap() noexcept( false );

	void tryToLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false );
	void tryDiffuseMapLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false );
	void trySpecularMapLoadToGpu( ShaderProgram& shaderProgram ) noexcept( false );

	void loadDiffuseMap() noexcept( false );
	void loadDiffuseMapToGpu() noexcept( false );

	void loadSpecularMap() noexcept( false );
	void loadSpecularMapToGpu() noexcept( false );

private:
	Texture glIdDiffuseAtlas_;
	Texture2D diffuseAtlas_;

	Texture glIdSpecularAtlas_;
	Texture2D specularAtlas_;

	std::atomic_bool isReadyDiffuseMap_ = false;
	std::atomic_bool isReadySpecularMap_ = false;
};