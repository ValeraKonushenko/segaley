#pragma once
#include "Image.h"
#include "GLFW.h"
#include "Texture.h"

#include <optional>

class Texture2D final
{
public:
	enum class Format
	{
		None,
		RGB = GL_RGB,
		RGBA = GL_RGBA
	};

	static Format fromChannel2Format( Image::Channel channel ) noexcept( false );

public:
	Texture2D() = default;
	Texture2D( std::string_view path ) noexcept( false );

	Texture2D( const Texture2D& ) = default;
	Texture2D( Texture2D&& ) = default;
	Texture2D& operator=( const Texture2D& ) = default;
	Texture2D& operator=( Texture2D&& ) = default;

	~Texture2D();
	
	void resetImage();
	void setImage( Image&& image ) noexcept( false );
	Image& getImage() noexcept( false );
	const Image& getImage() const;
	int getWidth() const noexcept( false );
	int getHeight() const noexcept( false );
	glm::ivec2 getSize() const noexcept( false );
	Image::Channel getChannel() const noexcept( false );
	unsigned char* getImageData() noexcept( false );
	const unsigned char* getImageData() const noexcept( false );
	void loadImage( std::string_view path ) noexcept( false );
	void clear();
	bool isEmpty() const;

	void loadToGpu( Texture::Target target = Texture::Target::Texture2D, 
					GLint lvl = 0, Format internalFormat = Format::RGBA, 
					Format format = Format::RGBA ) noexcept( false );

private:
	std::optional< Image > image_;
};