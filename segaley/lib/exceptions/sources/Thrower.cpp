#include "Thrower.h"

#include <string>

void makeThrow( std::string_view msg, std::string_view func, std::string_view file, int line )noexcept( false )
{
	std::string res;
	res += file;
	res += "\n--> Function: ";
	res += func;
	res += "\n--> Line: ";
	res += std::to_string( line );
	res += "\n--> Description: ";
	res += msg;

	throw std::runtime_error( res );
}
