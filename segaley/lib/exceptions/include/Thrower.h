#pragma once

#include <stdexcept>
#include <string_view>

void makeThrow( std::string_view msg, std::string_view func, std::string_view file, int line ) noexcept( false );

#define MAKE_THROW( msg ) makeThrow( msg, __FUNCTION__, __FILE__, __LINE__ );
