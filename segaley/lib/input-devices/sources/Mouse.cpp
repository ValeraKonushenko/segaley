#include "Mouse.h"

#include "Thrower.h"
#include "Settings.h"

#include <Windows.h>

float Mouse::mouseSensetivity_ = 350.f;
glm::ivec2 Mouse::lastPosition_;
std::function< void( glm::fvec2 pos ) > Mouse::mouseMoveDeltaCallback_;

glm::ivec2 Mouse::getPosition()
{
	POINT p{};
	GetCursorPos( &p );
	return glm::ivec2( static_cast< int >( p.x ), static_cast< int >( p.y ) );
}

glm::ivec2 Mouse::getPosition( Window& wnd )
{
	POINT p{};
	GetCursorPos( &p );
	ScreenToClient( wnd.getHwnd(), &p );
	return glm::ivec2( static_cast< int >( p.x ), static_cast< int >( p.y ) );
}

bool Mouse::isKeyDown( Key key )
{
	if ( GetKeyState( static_cast< int >( key ) ) < 0 )
		return true;
	return false;
}

bool Mouse::isKeyUp( Key key )
{
	if ( GetKeyState( static_cast< int >( key ) ) > 0 )
		return true;
	return false;
}

void Mouse::update( Window& wnd )
{
	auto currentPosition = getPosition( wnd );
	
	glm::fvec2 delta = currentPosition - lastPosition_;
	delta /= mouseSensetivity_;
	if( mouseMoveDeltaCallback_ )
		mouseMoveDeltaCallback_( delta );	

	lastPosition_ = currentPosition;
}

void Mouse::setMouseMoveDeltaCallback( std::function<void( glm::fvec2 pos )> callback )
{
	mouseMoveDeltaCallback_ = callback;
}

void Mouse::resetMouseMoveDeltaCallback()
{
	mouseMoveDeltaCallback_ = nullptr;
}

void Mouse::setMouseSensativity( float sensativity ) noexcept( false )
{
	if ( sensativity <= 0.f )
		MAKE_THROW( "The mouse sensativity can't be less of equal of 0" );

	mouseSensetivity_ = sensativity;
}

void Mouse::showCursor()
{
	ShowCursor( true );
}

void Mouse::hideCursor()
{
	ShowCursor( false );
}

void Mouse::loadSettings() noexcept( false )
{
	auto settings = Settings::instance();
	Mouse::mouseSensetivity_ = settings->inputDevices.general.mouseSensetivity;
}
