#pragma once
#include "glm/glm.hpp"
#include "Window.h"

#include <functional>

class Mouse final
{
public:
	enum class Key
	{
		Left = VK_LBUTTON,
		Right = VK_RBUTTON,
		Center = VK_MBUTTON
	};

	Mouse() = delete;

	static glm::ivec2 getPosition();
	static glm::ivec2 getPosition( Window& wnd );

	static bool isKeyDown( Key key );
	static bool isKeyUp( Key key );
	
	static void update( Window& wnd );

	static void setMouseMoveDeltaCallback( std::function< void( glm::fvec2 pos ) > callback );
	static void resetMouseMoveDeltaCallback();

	static void setMouseSensativity( float sensativity ) noexcept( false );

	static void showCursor();
	static void hideCursor();

	static void loadSettings() noexcept( false );
private:
	static float mouseSensetivity_;
	static glm::ivec2 lastPosition_;
	static std::function< void( glm::fvec2 pos ) > mouseMoveDeltaCallback_;
};