
add_library(
	input-devices STATIC
	include/Keyboard.h
	include/Mouse.h
	sources/Keyboard.cpp
	sources/Mouse.cpp
)