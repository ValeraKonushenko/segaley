#pragma once
#include "Camera.h"
#include "Texture.h"
#include "Lighting.h"
#include "Color.h"

#include <fstream>
#include <string>

class Settings final
{
public:
	Settings( const Settings& ) = delete;
	Settings( Settings&& ) = delete;
	Settings& operator=( const Settings& ) = delete;
	Settings& operator=( Settings&& ) = delete;

	~Settings() = default;

	static Settings* instance();

	struct InputDevices final
	{
		struct General final 
		{
			float mouseSensetivity = 0.f;
			Keyboard::Key close = Keyboard::Key::None;
			Keyboard::Key reloadData = Keyboard::Key::None;
		} general;

		::Camera::ContolKeys camera;
	} inputDevices;

	struct General final
	{
		struct ViewBox final
		{
			unsigned short width = 0;
			unsigned short height = 0;
			float maxDeep = 0;
			float minDeep = 0.f;
		} viewBox;

		RGBAf screenClearColor;

		size_t freqencyOfSystemUpdates = 0u;
		size_t freqencyOfConsoleUpdates = 0u;
	} general;

	struct Paths final
	{
		std::string toAtlas;
		std::string toAtlasSpecularMap;
		std::string toMainFragmentShader;
		std::string toMainVertexShader;
		std::string toCubeMapFragmentShader;
		std::string toCubeMapVertexShader;
		std::string toGrassInstancingFragmentShader;
		std::string toGrassInstancingVertexShader;
		std::string toGuiFragmentShader;
		std::string toGuiVertexShader;
		
		std::string toRawModels;
		
		struct Skybox final
		{
			std::string toPx;
			std::string toNx;
			std::string toPy;
			std::string toNy;
			std::string toPz;
			std::string toNz;
		} skybox;
	} paths;

	struct Camera final 
	{
		float fow = 0.f;
		float translateSmooth = 0.f;
		float translateSpeed = 0.f;
		float translateTopSpeed = 0.f;
		float translateBoostSpeed = 0.f;
		float translateSlowSpeed = 0.f;
		::Camera::Location defaultLocation;
	} camera;

	struct Graphics final
	{
		Texture::MagFilter magFilter = Texture::MagFilter::None;
		Texture::MinFilter minFilter = Texture::MinFilter::None;

		Lighting::Ambient ambient;
		Lighting::Diffuse diffuse;
		Lighting::Specular specular;

		Lighting::Fog fog;

		Lighting::Sun sun;
	} graphics;

	void loadSettings() noexcept( false );
	void saveSettings() noexcept( false );

	static const std::string pathToConfigFolder;
	static const std::string fileNameGeneral;
	static const std::string fileNameInputDevices;
	static const std::string fileNamePath;
	static const std::string fileNameCamera;
	static const std::string fileNameGraphics;
private:
	Settings() = default;

	void loadGeneralSettings() noexcept( false );
	void loadInputDevicesSettings() noexcept( false );
	void loadPathsSettings() noexcept( false );
	void loadCameraSettings() noexcept( false );
	void loadGraphicsSettings() noexcept( false );
};