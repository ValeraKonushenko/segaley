#include "Settings.h"

#include "Thrower.h"
#include "json.hpp"
#include "glm/glm.hpp"
#include "utils.h"

#include <memory>
#include <filesystem>

const std::string Settings::pathToConfigFolder = "configs";
const std::string Settings::fileNameGeneral = "general.json";
const std::string Settings::fileNameInputDevices = "inputDevices.json";
const std::string Settings::fileNamePath = "paths.json";
const std::string Settings::fileNameCamera = "camera.json";
const std::string Settings::fileNameGraphics = "graphics.json";

Settings* Settings::instance()
{
	static std::unique_ptr< Settings > ptr;
	if ( !ptr )	
		ptr = std::unique_ptr< Settings >( new Settings );	

	return ptr.get();
}

void Settings::loadSettings() noexcept( false )
{
	loadGeneralSettings();
	loadPathsSettings();
	loadInputDevicesSettings();
	loadCameraSettings();
	loadGraphicsSettings();
}

void Settings::loadGeneralSettings() noexcept( false )
{
	auto file = utils::readFile( "./" + Settings::pathToConfigFolder + "/" + Settings::fileNameGeneral );	
	auto json = nlohmann::json::parse( file );

	general.viewBox.width = json[ "viewBox" ][ "width" ];
	general.viewBox.height = json[ "viewBox" ][ "height" ];
	general.viewBox.maxDeep = json[ "viewBox" ][ "maxDeep" ];
	general.viewBox.minDeep = json[ "viewBox" ][ "minDeep" ];

	{
		RGBA rgba;
		rgba.r = json[ "screenClearColor" ][ "R" ];
		rgba.g = json[ "screenClearColor" ][ "G" ];
		rgba.b = json[ "screenClearColor" ][ "B" ];
		rgba.a = json[ "screenClearColor" ][ "A" ];
		general.screenClearColor = Color::convertToF( rgba );
	}

	general.freqencyOfSystemUpdates = json[ "freqencyOfSystemUpdates" ];
	general.freqencyOfConsoleUpdates = json[ "freqencyOfConsoleUpdates" ];
}

void Settings::loadInputDevicesSettings() noexcept( false )
{
	auto file = utils::readFile( "./" + Settings::pathToConfigFolder + "/" + Settings::fileNameInputDevices );
	auto json = nlohmann::json::parse( file );

	inputDevices.general.mouseSensetivity = json[ "general" ][ "mouseSensetivity" ];
	inputDevices.general.close = Keyboard::stringToKey( json[ "general" ][ "close" ] );
	inputDevices.general.reloadData = Keyboard::stringToKey( json[ "general" ][ "reloadData" ] );

	inputDevices.camera.boostMode = Keyboard::stringToKey( json[ "camera" ][ "boostMode" ] );
	inputDevices.camera.slowMode = Keyboard::stringToKey( json[ "camera" ][ "slowMode" ] );
	inputDevices.camera.toRight = Keyboard::stringToKey( json[ "camera" ][ "toRight" ] );
	inputDevices.camera.toLeft = Keyboard::stringToKey( json[ "camera" ][ "toLeft" ] );
	inputDevices.camera.toUp = Keyboard::stringToKey( json[ "camera" ][ "toUp" ] );
	inputDevices.camera.toDown = Keyboard::stringToKey( json[ "camera" ][ "toDown" ] );
	inputDevices.camera.toForward = Keyboard::stringToKey( json[ "camera" ][ "toForward" ] );
	inputDevices.camera.toBackward = Keyboard::stringToKey( json[ "camera" ][ "toBackward" ] );
	inputDevices.camera.reset = Keyboard::stringToKey( json[ "camera" ][ "reset" ] );
}

void Settings::loadPathsSettings() noexcept( false )
{
	//TODO: compare boost::property_tree and nlohman::json and, may be, change to boost::property_tree
	auto file = utils::readFile( "./" + Settings::pathToConfigFolder + "/" + Settings::fileNamePath );
	auto json = nlohmann::json::parse( file );

	paths.toAtlas = json[ "toAtlas" ];
	paths.toAtlasSpecularMap = json[ "toAtlasSpecularMap" ];
	paths.toMainFragmentShader = json[ "toMainFragmentShader" ];
	paths.toMainVertexShader = json[ "toMainVertexShader" ];
	paths.toCubeMapFragmentShader = json[ "toCubeMapFragmentShader" ];
	paths.toCubeMapVertexShader = json[ "toCubeMapVertexShader" ];
	paths.toGrassInstancingFragmentShader = json[ "toGrassInstancingFragmentShader" ];
	paths.toGrassInstancingVertexShader = json[ "toGrassInstancingVertexShader" ];
	paths.toGuiVertexShader = json[ "toGuiVertexShader" ];
	paths.toGuiFragmentShader = json[ "toGuiFragmentShader" ];

	paths.toRawModels = json[ "toRawModels" ];

	paths.skybox.toPx = json[ "toSkyboxData" ][ "toPx" ];
	paths.skybox.toNx = json[ "toSkyboxData" ][ "toNx" ];
	paths.skybox.toPy = json[ "toSkyboxData" ][ "toPy" ];
	paths.skybox.toNy = json[ "toSkyboxData" ][ "toNy" ];
	paths.skybox.toPz = json[ "toSkyboxData" ][ "toPz" ];
	paths.skybox.toNz = json[ "toSkyboxData" ][ "toNz" ];
}

void Settings::loadCameraSettings() noexcept( false )
{
	auto file = utils::readFile( "./" + Settings::pathToConfigFolder + "/" + Settings::fileNameCamera );
	auto json = nlohmann::json::parse( file );

	camera.fow = glm::radians( static_cast< float >( json[ "fow" ] ) );
	camera.translateSmooth = json[ "translateSmooth" ];
	camera.translateSpeed = json[ "translateSpeed" ];
	camera.translateTopSpeed = json[ "translateTopSpeed" ];
	camera.translateBoostSpeed = json[ "translateBoostSpeed" ];
	camera.translateSlowSpeed = json[ "translateSlowSpeed" ];
		
	camera.defaultLocation.position.x = json[ "default" ][ "position" ][ "x" ];
	camera.defaultLocation.position.y = json[ "default" ][ "position" ][ "y" ];
	camera.defaultLocation.position.z = json[ "default" ][ "position" ][ "z" ];
	camera.defaultLocation.rotate.pitch = glm::radians( static_cast< float >( json[ "default" ][ "rotate" ][ "pitch" ] ) );
	camera.defaultLocation.rotate.yaw = glm::radians( static_cast< float >( json[ "default" ][ "rotate" ][ "yaw" ] ) );
}

void Settings::loadGraphicsSettings() noexcept( false )
{
	auto file = utils::readFile( "./" + Settings::pathToConfigFolder + "/" + Settings::fileNameGraphics );
	auto json = nlohmann::json::parse( file );

	graphics.magFilter = Texture::stringToMagFilter( json[ "textures" ][ "magFilter" ] );
	graphics.minFilter = Texture::stringToMinFilter( json[ "textures" ][ "minFilter" ] );

	graphics.ambient.intensity = json[ "lighting" ][ "ambient" ][ "intensity" ];
	graphics.diffuse.minLighting = json[ "lighting" ][ "diffuse" ][ "minLighting" ];
	graphics.diffuse.intensity = json[ "lighting" ][ "diffuse" ][ "intensity" ];
	graphics.diffuse.direction.x = json[ "lighting" ][ "diffuse" ][ "direction" ][ "x" ];
	graphics.diffuse.direction.y = json[ "lighting" ][ "diffuse" ][ "direction" ][ "y" ];
	graphics.diffuse.direction.z = json[ "lighting" ][ "diffuse" ][ "direction" ][ "z" ];

	{
		RGB rgb;
		rgb.r = json[ "lighting" ][ "diffuse" ][ "color" ][ "r" ];
		rgb.g = json[ "lighting" ][ "diffuse" ][ "color" ][ "g" ];
		rgb.b = json[ "lighting" ][ "diffuse" ][ "color" ][ "b" ];
		graphics.diffuse.color = Color::convertToF( rgb );
	}

	graphics.specular.pow = json[ "lighting" ][ "specular" ][ "pow" ];
	graphics.specular.intensity = json[ "lighting" ][ "specular" ][ "intensity" ];
	{
		RGB rgb;
		rgb.r = json[ "lighting" ][ "specular" ][ "color" ][ "r" ];
		rgb.g = json[ "lighting" ][ "specular" ][ "color" ][ "g" ];
		rgb.b = json[ "lighting" ][ "specular" ][ "color" ][ "b" ];
		graphics.specular.color = Color::convertToF( rgb );
	}

	graphics.sun.position.x = json[ "lighting" ][ "sunPosition" ][ "x" ];
	graphics.sun.position.y = json[ "lighting" ][ "sunPosition" ][ "y" ];
	graphics.sun.position.z = json[ "lighting" ][ "sunPosition" ][ "z" ];

	graphics.fog.minDistance = json[ "fog" ][ "minDistance" ];
	graphics.fog.maxDistance = json[ "fog" ][ "maxDistance" ];
	{
		RGB rgb;
		rgb.r = json[ "fog" ][ "color" ][ "r" ];
		rgb.g = json[ "fog" ][ "color" ][ "g" ];
		rgb.b = json[ "fog" ][ "color" ][ "b" ];
		graphics.fog.color = Color::convertToF( rgb );
	}
}
