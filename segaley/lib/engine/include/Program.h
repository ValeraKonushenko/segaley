#pragma once
#include "Camera.h"
#include "Gui.h"
#include "Lighting.h"
#include "Model.h"
#include "Repeater.h"
#include "Scene.h"
#include "ShaderProgram.h"
#include "Shaders.h"
#include "Texture.h"
#include "TextureAtlas.h"
#include "ViewBoxSpace.h"
#include "Window.h"

class Program final : public Model 
{
public:
	Program();
	
	Program( const Program& ) = delete;
	Program( Program&& ) = delete;
	Program& operator=( const Program& ) = delete;
	Program& operator=( Program&& ) = delete;

	~Program();

	void launch() noexcept( false );
	void close();

	void loadSettings() noexcept( false );
	void saveSettings() noexcept( false );

private:
	void setUpShaderPack() noexcept( false );
	void setUpDebug() noexcept( false );
	void preDraw() noexcept( false );
	void postDraw() noexcept( false );
	void startDraw() noexcept( false );
	void updateGeneralKeyCommands() noexcept( false );

	ShaderPack shaderPack_;
	Window window_;
	ViewBoxSpace viewBoxSpace_;
	Camera camera_;
	Repeater consoleUpdater_;
	Repeater systemUpdater_;
	TextureAtlas textureAtlas_;	
	Scene scene_;
	gui::Gui gui_;
};