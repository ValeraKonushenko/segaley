#pragma once

#include <vector>

class Model;

class ModelManager final
{
public:
	void loadAllSettings() noexcept( false );
	void saveAllSettings() noexcept( false );

	void registry( Model& model ) noexcept( false );
	void remove( Model& model ) noexcept( false );

	static ModelManager* instance();
private:
	ModelManager() = default;

	std::vector< Model* > models_;
};