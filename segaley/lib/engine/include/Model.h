#pragma once

class Model 
{
public:
	Model();
	virtual ~Model();

	virtual void loadSettings() noexcept( false ) = 0;
	virtual void saveSettings() noexcept( false ) = 0;

private:
};