#include "Model.h"
#include "ModelManager.h"

Model::Model()
{
	ModelManager::instance()->registry( *this );
}

Model::~Model()
{
	ModelManager::instance()->remove( *this );
}
