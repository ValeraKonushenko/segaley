#include "ModelManager.h"

#include "Model.h"
#include "Settings.h"
#include "Thrower.h"
#include "Mouse.h"

#include <memory>

void ModelManager::loadAllSettings() noexcept( false )
{
	Settings::instance()->loadSettings();

	Mouse::loadSettings();

	for ( auto obj : models_ )
		obj->loadSettings();
}

void ModelManager::registry( Model& model ) noexcept( false )
{
	if ( std::find( models_.begin(), models_.end(), &model ) != models_.end() )
		MAKE_THROW( "Such object already has been registered. You can't register an object twice" );

	models_.push_back( &model );
}

void ModelManager::remove( Model& model ) noexcept( false )
{
	std::erase_if( models_, [&model]( auto ptr )
	{
		return ptr == &model;
	} );
}

ModelManager* ModelManager::instance()
{
	static std::unique_ptr< ModelManager > obj = nullptr;
	if ( !obj )
	{
		obj = std::unique_ptr< ModelManager >( new ModelManager );
	}

	return obj.get();
}
