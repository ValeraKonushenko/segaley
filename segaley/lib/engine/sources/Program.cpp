#include "Program.h"

#include "DebugActions.h"
#include "DebugConsole.h"
#include "Figures.h"
#include "glm/gtc/matrix_transform.hpp"
#include "Keyboard.h"
#include "MaterialManager.h"
#include "ModelManager.h"
#include "Mouse.h"
#include "Program.h"
#include "Settings.h"
#include "Texture.h"
#include "Texture2D.h"
#include "ThreadPool.h"
#include "CharacterManager.h"

#include "stb_image.h"

#include <iostream>

Program::Program() :
    shaderPack_( textureAtlas_, camera_ ),
    gui_( window_ )
{
}

Program::~Program()
{
    close();
}

void Program::launch() noexcept( false )
{
    ModelManager::instance()->loadAllSettings();

    auto settings = Settings::instance();
    MaterialManager::instance()->loadMaterials();

    window_.setSize( { settings->general.viewBox.width, settings->general.viewBox.height } );
    viewBoxSpace_.setViewBoxSize( ViewBox( window_.getSize(), settings->general.viewBox.maxDeep ) );
    consoleUpdater_.setFreq( 1.0 / static_cast< double >( settings->general.freqencyOfConsoleUpdates ) );
    
    gui_.prepare();
    scene_.prepare();

    setUpShaderPack();
    setUpDebug();
    systemUpdater_.setCallback( [ this ]()
    {
        camera_.update();
        updateGeneralKeyCommands();

        textureAtlas_.tryToLoadToGpu( shaderPack_.main );
    } );
    systemUpdater_.setFreq( 1. / settings->general.freqencyOfSystemUpdates );

    camera_.resetLocation();

    window_.setMousePosEventCallback( [ this ]( double x, double y )
    {
        static auto settings = Settings::instance();        

        static glm::vec2 lastPos = { x, y };
        
        auto pitch = static_cast< float >( x - lastPos.x ) / settings->inputDevices.general.mouseSensetivity;
        auto yaw = static_cast< float >( y - lastPos.y ) / settings->inputDevices.general.mouseSensetivity;
        lastPos = { x, y };

        camera_.pitch( pitch );
        camera_.yaw( yaw );
    } );

    glfwSetInputMode( window_.data(), GLFW_CURSOR, GLFW_CURSOR_DISABLED );

    startDraw();
}

void Program::close()
{
    window_.close();
    ThreadPool::instance()->closeAllThreads();
}

void Program::loadSettings() noexcept( false )
{
    auto settings = Settings::instance();

    Texture::setWrapS( Texture::Wrap::Repeat );
    Texture::setWrapT( Texture::Wrap::Repeat );
    Texture::setMagFilter( settings->graphics.magFilter );
    Texture::setMinFilter( settings->graphics.minFilter );
}

void Program::saveSettings() noexcept( false )
{
    MAKE_THROW( "not finished" );
}

void Program::setUpShaderPack() noexcept( false )
{
    auto settings = Settings::instance();

    shaderPack_.main.afterLoad = [ this ]()
    {
        textureAtlas_.asyncLoadData();
    };
    shaderPack_.main.load( settings->paths.toMainFragmentShader, settings->paths.toMainVertexShader );
    shaderPack_.cubeMap.load( settings->paths.toCubeMapFragmentShader, settings->paths.toCubeMapVertexShader );
    shaderPack_.grassInstancing.load( settings->paths.toGrassInstancingFragmentShader, settings->paths.toGrassInstancingVertexShader );
    shaderPack_.gui.load( settings->paths.toGuiFragmentShader, settings->paths.toGuiVertexShader );
}

void Program::setUpDebug() noexcept( false )
{
    auto settings = Settings::instance();
    auto console = DebugConsole::instance();

    consoleUpdater_.setFreq( 1.0 / static_cast< double >( settings->general.freqencyOfConsoleUpdates ) );
    consoleUpdater_.setCallback( [ this ]()
    {
        auto console = DebugConsole::instance();
        console->cameraProperty_ = camera_.getProperty();
        console->print( std::cout );
    } );
}

void Program::preDraw() noexcept( false )
{
    static auto settings = Settings::instance();
    static FPSCounter fps( 1.0 / static_cast< double >( settings->general.freqencyOfConsoleUpdates ), []( Fps fps )
    {
        auto console = DebugConsole::instance();
        console->fps_ = fps;
    } );
    fps.execute();
    Mouse::update( window_ );
    consoleUpdater_.update();

  
    window_.clearScreen( 
        Window::Buffer::Color | Window::Buffer::Depth, 
        settings->general.screenClearColor 
    );
}

void Program::postDraw() noexcept( false )
{
    window_.swapBuffers();
    window_.pollEvents();
    
    systemUpdater_.update();
}

void Program::startDraw() noexcept( false )
{
    gui::Text text( "Hello" );
    text.setFontSize( 100 );
    auto sz = text.getWidth();

    while ( window_.isOpen() )
    {
        preDraw();
        
        auto v = camera_.getViewMatrix( window_.getSize() );
        auto p = camera_.getPerspective( window_.getSize().getRatio() );
        
        scene_.draw( shaderPack_, v, p, camera_.getPosition() );
        gui::CharacterManager::instance().drawText(
            text, window_, shaderPack_, { 0,0 }
        );
        
        postDraw();
    }
}

void Program::updateGeneralKeyCommands() noexcept( false )
{
    auto settings = Settings::instance();

    if ( Keyboard::isKeyDown( settings->inputDevices.general.close ) )
        close();
    if ( Keyboard::isKeyDown( settings->inputDevices.general.reloadData ) )
    {
        DebugActions::launch();
        ModelManager::instance()->loadAllSettings();
        MaterialManager::instance()->loadMaterials();
        setUpShaderPack();
    }
}
