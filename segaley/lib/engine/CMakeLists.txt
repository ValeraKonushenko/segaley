
add_library(
	engine STATIC
	include/Model.h
	include/ModelManager.h
	include/Program.h
	sources/Model.cpp
	sources/ModelManager.cpp
	sources/Program.cpp
)