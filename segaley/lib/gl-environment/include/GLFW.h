#pragma once
#include <GLAD/glad.h>
#include <GLFW/glfw3.h>

class GLFW final
{
public:
	static GLFW& instance(
		int vMajor,
		int vMinor,
		int profile = GLFW_OPENGL_CORE_PROFILE,
		int forwardCompat = true ) noexcept( false );

	GLFW( const GLFW& ) = delete;
	GLFW( GLFW&& ) = delete;
	GLFW& operator=( const GLFW& ) = delete;
	GLFW& operator=( GLFW&& ) = delete;

	~GLFW();
private:
	GLFW( int vMajor, int vMinor, int profile, int forwardCompat );
};