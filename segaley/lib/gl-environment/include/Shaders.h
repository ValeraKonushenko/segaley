#pragma once
#include "Lighting.h"
#include "ShaderProgram.h"

#include <functional>

enum class ShaderName
{
	Main,
	CubeMap,
	GrassInstancing,
	Gui
};

class BasicShader
{
public:
	virtual ~BasicShader() = default;
	BasicShader( const BasicShader& ) = delete;
	BasicShader( BasicShader&& ) = delete;

	void load( std::string_view pathToFrag, std::string_view pathToVert ) noexcept( false );
	std::function< void() > afterLoad;
	std::function< void() > beforeLoad;	

	operator ShaderProgram&();

	virtual void use() noexcept( false );

	ShaderName getShaderName() noexcept( false );

public:
	ShaderProgram shader;

protected:
	BasicShader( ShaderName shaderName );

private:
	ShaderName shaderName_;
};

class Camera;

class MainShader final : public BasicShader
{
public:
	MainShader( Camera& camera );
	~MainShader() = default;

	Lighting lighting;	

	void use() noexcept( false ) override;
	void updateLighting() noexcept( false );

private:
	Camera& camera_;
};

class CubeMapShader final : public BasicShader 
{
public:
	CubeMapShader();
	~CubeMapShader() = default;
};

class TextureAtlas;

class GrassInstancingShader final : public BasicShader
{
public:
	void use() noexcept( false ) override;

	GrassInstancingShader( TextureAtlas& textureAtlas );
	~GrassInstancingShader() = default;

private:
	TextureAtlas& textureAtlas_;
};

class GuiShader final : public BasicShader
{
public:
	void use() noexcept( false ) override;

	GuiShader( TextureAtlas& textureAtlas );
	~GuiShader() = default;

private:
	TextureAtlas& textureAtlas_;
};

class ShaderPack final
{
public:
	ShaderPack( TextureAtlas& textureAtlas, Camera& camera );
	~ShaderPack() = default;
	ShaderPack( const ShaderPack& ) = delete;
	ShaderPack( ShaderPack&& ) = delete;

	MainShader main;
	CubeMapShader cubeMap;
	GrassInstancingShader grassInstancing;
	GuiShader gui;
};
 