#pragma once
#include "GLFW.h"
#include "VertexData.h"

#include <vector>

class Vbo final
{
public:
	static const GLuint invalidId;

	Vbo() = default;
	Vbo( bool isGenerate, bool isBind ) noexcept( false );

	Vbo( const std::vector< float >& data, GLenum usage = GL_STATIC_DRAW ) noexcept( false );
	Vbo( const std::vector< VertexData >& data, GLenum usage = GL_STATIC_DRAW ) noexcept( false );
	
	Vbo( const Vbo& ) = default;
	Vbo( Vbo&& ) noexcept;
	Vbo& operator=( const Vbo& ) = default;
	Vbo& operator=( Vbo&& ) noexcept;

	~Vbo();

	void generate() noexcept( false );
	void bind() noexcept( false );
	static void bind( GLuint id );
	void unbind() noexcept( false );
	void destroy();
	bool isEmpty() const;
	bool isBind() const;

	void setData( const std::vector< float >& data, GLenum usage = GL_STATIC_DRAW ) noexcept( false );
	void setData( const std::vector< VertexData >& data, GLenum usage = GL_STATIC_DRAW ) noexcept( false );

private:
	bool isBind_ = false;
	GLuint id_ = Vbo::invalidId;
};