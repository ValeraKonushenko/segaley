#pragma once
#include "GLFW.h"

class Vao final
{
public:
	static const GLuint invalidId;

	Vao() = default;
	Vao( bool isGenerate, bool isBind ) noexcept( false );//TODO: replace to enum class Prepare{ Fully,Generate,Bind };

	Vao( const Vao& ) = default;
	Vao( Vao&& other ) noexcept;
	Vao& operator=( const Vao& ) = default;
	Vao& operator=( Vao&& other ) noexcept;

	~Vao();

	void generate() noexcept( false );
	void bind() noexcept( false );
	static void bind( GLuint id );
	void unbind() noexcept( false );
	void destroy();
	bool isEmpty() const;
	bool isBind() const;

private:
	bool isBind_ = false;
	GLuint id_ = Vao::invalidId;
};