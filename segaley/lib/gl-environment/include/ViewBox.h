#pragma once
#include "WindowSize.h"

struct ViewBox
{
    WindowSize resolution;
    float deep{};
    bool operator==( const ViewBox& other )const;
    bool operator!=( const ViewBox& other )const;
};