#pragma once
#include <Windows.h>
#include "GLFW.h"
#include "WindowSize.h"
#include "glm/vec2.hpp"
#include "Color.h"

#include <string>
#include <functional>

enum class KeyAction
{
	Press	= GLFW_PRESS,
	Repeat	= GLFW_REPEAT,
	Release = GLFW_RELEASE
};

enum class MouseKey
{
	Button1,
	Button2,
	Button3,
	Button4,
	Button5,
	Button6,
	Button7,
	Button8,
	Last   = Button8,
	Left   = Button1,
	Right  = Button2,
	Middle = Button3
};

class Window final
{
public:
	using mousePosFuncT = std::function< void( float, float ) >;
	using mouseButtonFuncT = std::function< void( MouseKey, KeyAction, int ) >;
	using mouseScrollFuncT = std::function< void( float, float ) >;

public:
	enum Buffer
	{
		Depth = GL_DEPTH_BUFFER_BIT,
		Stencil = GL_STENCIL_BUFFER_BIT,
		Color = GL_COLOR_BUFFER_BIT
	};

	Window( unsigned short width = 1920, unsigned short height = 1080 )  noexcept( false );
	Window( const Window& ) = delete;
	Window( Window& ) = delete;
	Window& operator=( const Window& ) = delete;
	Window& operator=( Window& ) = delete;
	~Window();

	bool isEmpty() const;
	bool isOpen() const noexcept( false );
	GLFWwindow* data() noexcept( false );
	const GLFWwindow* data() const noexcept( false );

	void makeContextCurrent() noexcept( false );

	void swapBuffers() noexcept( false );

	void pollEvents() noexcept( false );

	void close() const;
	void clearScreen( GLbitfield mask, const RGBA& color ) const;
	void clearScreen( GLbitfield mask, const RGBAf& color ) const;

	void setSize( WindowSize size );
	WindowSize getSize() const;

	void setPosition( glm::ivec2 position );
	glm::ivec2 getPosition() const;

	void setTitle( std::string title );
	std::string getTitle() const;

	void setMousePosEventCallback( mousePosFuncT func );
	void resetMousePosEventCallback();
	void makeMousePosEvent( glm::vec2 pos );

	void setMouseButtonEventCallback( mouseButtonFuncT func );
	void resetMouseButtonEventCallback();
	void makeMouseButtonEvent( MouseKey button, KeyAction action, int mods );

	void setMouseScrollEventCallback( mouseScrollFuncT func );
	void resetMouseScrollEventCallback();
	void makeMouseScrollEvent( float xOffset, float yOffset );

	HWND getHwnd() noexcept( false );

private:
	void checkCreateStatus() noexcept( false );
	void initGlad() noexcept( false );
	void initGl() noexcept( false );
	void init() noexcept( false );

	glm::ivec2 position_;
	GLFWwindow* window_;
	WindowSize size_;
	std::string title_;
	mousePosFuncT mousePosEventCallback_;
	mouseButtonFuncT mouseButtonEventCallback_;
	mouseScrollFuncT mouseScrollEventCallback_;

	static Window* oneObject_;
	friend void MousePosCallback( GLFWwindow*, double, double );
	friend void MouseButtonCallback( GLFWwindow*, int, int, int );
	friend void MouseWheelCallback( GLFWwindow*, double, double );
};

void MousePosCallback( GLFWwindow* window, double x, double y );
void MouseButtonCallback( GLFWwindow* window, int button, int action, int mods );
void MouseWheelCallback( GLFWwindow* window, double xoffset, double yoffset );