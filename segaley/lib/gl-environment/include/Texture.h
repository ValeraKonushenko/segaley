#pragma once
#include "GLFW.h"
#include "stb_image.h"

#include <string_view>

class Texture final
{
public:
	enum class Target
	{
		None,
		Texture2D = GL_TEXTURE_2D,
		CubeMapPositiveX = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		CubeMapNegativeX = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		CubeMapPositiveY = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		CubeMapNegativeY = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		CubeMapPositiveZ = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		CubeMapNegativeZ = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		CubeMap = GL_TEXTURE_CUBE_MAP
	};
	enum class MagFilter
	{
		None,
		Linear = GL_LINEAR,
		Nearest = GL_NEAREST,
	};

	enum class MinFilter
	{
		None,
		Linear = GL_LINEAR,
		Nearest = GL_NEAREST,
		NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
		LinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST,
		NearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR,
		LinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR
	};

	enum class Wrap
	{	
		None,
		Clamp2Edge		 = GL_CLAMP_TO_EDGE,
		Clamp2Border	 = GL_CLAMP_TO_BORDER,
		MirroredRepeat	 = GL_MIRRORED_REPEAT,
		Repeat			 = GL_REPEAT,
		MirrorClamp2Edge = GL_MIRROR_CLAMP_TO_EDGE
	};

	static MagFilter stringToMagFilter( std::string_view filter );
	static MinFilter stringToMinFilter( std::string_view filter );

	static void setMinFilter( MinFilter filter, Target target = Target::Texture2D ) noexcept( false );
	static void setMagFilter( MagFilter filter, Target target = Target::Texture2D ) noexcept( false );
	static void setWrapS( Wrap wrap, Target target = Target::Texture2D ) noexcept( false );
	static void setWrapT( Wrap wrap, Target target = Target::Texture2D ) noexcept( false );
	static void setWrapR( Wrap wrap, Target target = Target::Texture2D ) noexcept( false );
	static void active( GLenum num ) noexcept( false );
	static size_t getMaxCountActiveTextures() noexcept( false );
	static const GLuint invalidId;

	Texture( Target target = Target::Texture2D ) noexcept( false );

	Texture( const Texture& ) = delete;
	Texture( Texture&& ) = delete;
	Texture& operator=( const Texture& ) = delete;
	Texture& operator=( Texture&& ) = delete;

	~Texture();

	void bind() const noexcept( false );
	void unbind() const noexcept( false );
	void destroy();
	bool isEmpty() const;
	GLuint data();

	bool isDestroyAtEnd() const;
	void setIsDestroyAtEnd( bool is );

private:
	GLuint id_ = Texture::invalidId;
	Target target_ = Target::None;
	bool isDestroyAtEnd_ = true;
};