#pragma once
#include "ViewBox.h"

class ViewBoxSpace
{
public:
	ViewBoxSpace() = default;

	ViewBoxSpace( const ViewBoxSpace & ) = default;
	ViewBoxSpace( ViewBoxSpace && ) = default;
	ViewBoxSpace& operator=( const ViewBoxSpace& ) = default;
	ViewBoxSpace& operator=( ViewBoxSpace&& ) = default;

	~ViewBoxSpace() = default;

	void setViewBoxSize( const ViewBox& size ) noexcept( false );
	ViewBox getSize()const;

protected:
	ViewBox viewBox_;
};