#pragma once
#include "Shader.h"

#include "glm/glm.hpp"
#include "Color.h"

#include <memory>

class ShaderProgram final
{
public:
	ShaderProgram();

	ShaderProgram( const ShaderProgram& ) = delete;
	ShaderProgram( ShaderProgram&& ) = delete;
	ShaderProgram& operator=( const ShaderProgram& ) = delete;
	ShaderProgram& operator=( ShaderProgram&& ) = delete;

	~ShaderProgram();

	bool isEmpty() const;
	bool isSetShader( Shader::Type type ) const;
	void attachShader( Shader& shader ) noexcept( false );
	void dettachShader( Shader::Type type ) noexcept( false );
	void link() noexcept( false );
	void use() noexcept( false );
	void create() noexcept( false );
	void free();

	void vertexAttribPtr( GLuint nLayout, GLint size, GLsizei stride, GLuint offset ) const;
	void vertexAttribPtrByName( std::string_view shaderVarName, GLint size, GLsizei stride, GLuint offset ) noexcept( false );
	GLint getAttribLocation( std::string_view shaderVarName ) const noexcept( false );
	GLint getUniformLocation( std::string_view uniformName ) const noexcept( false );

	void uniform( std::string_view uniformName, glm::fvec3 data ) noexcept( false );
	void uniform( std::string_view uniformName, glm::fvec2 data ) noexcept( false );
	void uniform( std::string_view uniformName, float data ) noexcept( false );
	void uniform( std::string_view uniformName, size_t data ) noexcept( false );
	void uniform( std::string_view uniformName, GLint data ) noexcept( false );
	void uniform( std::string_view uniformName, const glm::mat4& data ) noexcept( false );
	void uniform( std::string_view uniformName, const RGBf& data ) noexcept( false );

	void uniform( GLint uniform, glm::fvec3 data );
	void uniform( GLint uniform, glm::fvec2 data );
	void uniform( GLint uniform, float data );
	void uniform( GLint uniform, size_t data );
	void uniform( GLint uniform, GLint data );
	void uniform( GLint uniform, const glm::mat4& data ) noexcept( false );
	void uniform( GLint uniform, const RGBf& data ) noexcept( false );
private:
	/*
	 - GL_DELETE_STATUS
	 - GL_LINK_STATUS
	 - GL_VALIDATE_STATUS
	 - GL_INFO_LOG_LENGTH
	 - GL_ATTACHED_SHADERS
	 - GL_ACTIVE_ATTRIBUTES
	 - GL_ACTIVE_ATTRIBUTE_MAX_LENGTH
	 - GL_ACTIVE_UNIFORMS
	 - GL_ACTIVE_UNIFORM_MAX_LENGTH
	*/
	void checkForError( GLenum param ) noexcept( false );

	GLuint id_;
	Shader* vShaderP_;
	Shader* fShaderP_;
	Shader* gShaderP_;
};