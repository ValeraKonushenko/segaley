#pragma once
#include <string>
#include <GLAD/glad.h>


class Shader
{
public:
	enum class Type
	{
		Vertex, Fragment, Geometry
	};

	Shader( Type type );

	Shader( const Shader& ) = delete;
	Shader( Shader&& ) = delete;
	Shader& operator=( const Shader& ) = delete;
	Shader& operator=( Shader&& ) = delete;

	Type getType() const;
	bool isApplied() const;
	bool isEmpty() const;
	void loadFromFile( std::string_view path ) noexcept( false );
	void apply() noexcept( false );
	void free();
	GLuint data();

	~Shader();

private:
	/*
	* - GL_SHADER_TYPE
	* - GL_DELETE_STATUS
	* - GL_COMPILE_STATUS
	* - GL_INFO_LOG_LENGTH
	* - GL_SHADER_SOURCE_LENGTH
	*/
	void checkForError( GLenum param ) const noexcept( false );
	
	bool isApplied_;
	const Type type_;
	std::string source_;
	const GLchar* rawSource_;
	GLuint id_;
};