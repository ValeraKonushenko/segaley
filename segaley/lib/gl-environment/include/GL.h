#pragma once

#include "GLFW.h"

class Gl final
{
public:
	Gl() = delete;

	class Vao
	{
	public:
		static void generate( GLsizei n, GLuint* arrays );
		static void bind( GLuint array ) noexcept( false ) ;
		static void reset();
		static bool isBind();

	private:
		static GLuint id_;
	};

	class Vbo
	{
	public:
		static void generate( GLsizei n, GLuint* arrays );
		static void bind( GLenum target, GLuint buffer ) noexcept( false );
		static void reset( GLenum target );
		static bool isBind();
		static void data( GLenum target, GLsizeiptr size, const void* data, GLenum usage ) noexcept( false );
		static void subData( GLenum target, GLintptr offset, GLsizeiptr size, const void* data ) noexcept( false );

	private:
		static GLuint id_;
	};

	static void enableVertexAttribArray( GLuint index ) noexcept( false );
	static void disableVertexAttribArray( GLuint index ) noexcept( false );
	static void vertexAttribPointer( GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer ) noexcept( false );
};