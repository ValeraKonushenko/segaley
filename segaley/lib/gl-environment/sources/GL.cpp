#include "Gl.h"

#include "Vao.h"
#include "Vbo.h"
#include "Thrower.h"

GLuint Gl::Vao::id_ = ::Vao::invalidId;
GLuint Gl::Vbo::id_ = ::Vbo::invalidId;

void Gl::Vao::generate( GLsizei n, GLuint* arrays )
{
	if ( isBind() )
		MAKE_THROW( "You can not to generate new VAO buffer with a bound VAO. Reset VAO and try again." );

	glGenVertexArrays( n, arrays );
}

void Gl::Vao::bind( GLuint array )
{
	if ( isBind() )
		MAKE_THROW( "You try to bind a VAO again. The best way is to unbind a VAO after the last using or shader setting up." )
	
	glBindVertexArray( array );
	
	Gl::Vao::id_ = array;
}

void Gl::Vao::reset()
{
	glBindVertexArray( 0u );

	Gl::Vao::id_ = 0u;
}

bool Gl::Vao::isBind()
{
	return Gl::Vao::id_ != ::Vao::invalidId;
}

void Gl::Vbo::generate( GLsizei n, GLuint* arrays )
{
	glGenBuffers( n, arrays );
}

void Gl::Vbo::bind( GLenum target, GLuint buffer )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "Impossible to bind VBO without connected(bound) VAO" );

	glBindBuffer( target, buffer );

	Gl::Vbo::id_ = buffer;
}

void Gl::Vbo::reset( GLenum target )
{
	glBindBuffer( target, 0u );

	Gl::Vbo::id_ = 0u;
}

bool Gl::Vbo::isBind()
{
	return Gl::Vbo::id_ != ::Vbo::invalidId;
}

void Gl::Vbo::data( GLenum target, GLsizeiptr size, const void* data, GLenum usage ) noexcept( false )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "You try to set up a shader using a zero VAO. Try to bind a VAO and try again." );

	if ( !isBind() )
		MAKE_THROW( "Impossible to put data to unbound VBO." );

	glBufferData( target, size, data, usage );
}

void Gl::Vbo::subData( GLenum target, GLintptr offset, GLsizeiptr size, const void* data ) noexcept( false )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "You try to set up a shader using a zero VAO. Try to bind a VAO and try again." );

	if ( !isBind() )
		MAKE_THROW( "Impossible to put data to unbound VBO." );

	glBufferSubData( target, offset, size, data );
}

void Gl::enableVertexAttribArray( GLuint index ) noexcept( false )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "You try to set up a shader using a zero VAO. Try to bind a VAO and try again." );

	glEnableVertexAttribArray( index );
}

void Gl::disableVertexAttribArray( GLuint index ) noexcept( false )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "You try to set up a shader using a zero VAO. Try to bind a VAO and try again." );
	
	glDisableVertexAttribArray( index );
}

void Gl::vertexAttribPointer( GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer ) noexcept( false )
{
	if ( !Gl::Vao::isBind() )
		MAKE_THROW( "You try to set up a shader using a zero VAO. Try to bind a VAO and try again." );
	
	glVertexAttribPointer( index, size, type, normalized, stride, pointer );
}