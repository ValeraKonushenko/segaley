#include "Shaders.h"
#include "TextureAtlas.h"
#include "Camera.h"

MainShader::MainShader( Camera& camera ) :
    BasicShader( ShaderName::Main ),
    camera_( camera )
{
}

CubeMapShader::CubeMapShader() :
    BasicShader( ShaderName::CubeMap )
{
}

GuiShader::GuiShader( TextureAtlas& textureAtlas ) :
    BasicShader( ShaderName::Gui ),
    textureAtlas_( textureAtlas )
{
}

GrassInstancingShader::GrassInstancingShader( TextureAtlas& textureAtlas ) :
    BasicShader( ShaderName::GrassInstancing ),
    textureAtlas_( textureAtlas )
{
}

ShaderPack::ShaderPack( TextureAtlas& textureAtlas, Camera& camera ) :
    grassInstancing( textureAtlas ),
    main( camera ),
    gui( textureAtlas )
{
}

void MainShader::use() noexcept( false )
{
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );
    glCullFace( GL_FRONT );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    BasicShader::use();
    updateLighting();
}

void MainShader::updateLighting() noexcept( false )
{
    shader.uniform( "uAmbient.intensity", lighting.ambient.intensity );

    shader.uniform( "uDiffuse.minLighting", lighting.diffuse.minLighting );
    shader.uniform( "uDiffuse.intensity", lighting.diffuse.intensity );
    shader.uniform( "uDiffuse.direction", lighting.diffuse.direction );
    shader.uniform( "uDiffuse.color", lighting.diffuse.color );

    shader.uniform( "uSpecular.intensity", lighting.specular.intensity );
    shader.uniform( "uSpecular.powder", lighting.specular.pow );
    shader.uniform( "uSpecular.color", lighting.specular.color );

    shader.uniform( "uFog.minDistance", lighting.fog.minDistance );
    shader.uniform( "uFog.maxDistance", lighting.fog.maxDistance );
    shader.uniform( "uFog.color", lighting.fog.color );

    shader.uniform( "uViewPosition", camera_.getPosition() );

    shader.uniform( "uSunPosition", lighting.sun.position );
}

void BasicShader::load( std::string_view pathToFrag, std::string_view pathToVert ) noexcept( false )
{
    if ( beforeLoad )
        beforeLoad();

    Shader frag( Shader::Type::Fragment );
    Shader vert( Shader::Type::Vertex );

    frag.free();
    frag.loadFromFile( pathToFrag );
    frag.apply();

    vert.free();
    vert.loadFromFile( pathToVert );
    vert.apply();

    shader.free();
    shader.create();
    shader.attachShader( frag );
    shader.attachShader( vert );
    shader.link();
    shader.dettachShader( Shader::Type::Vertex );
    shader.dettachShader( Shader::Type::Fragment );
    shader.use();

    if ( afterLoad )
        afterLoad();
}

BasicShader::operator ShaderProgram& ( )
{
    return shader;
}

void BasicShader::use() noexcept( false )
{
    shader.use();
}

ShaderName BasicShader::getShaderName() noexcept( false )
{
    return shaderName_;
}

BasicShader::BasicShader( ShaderName shaderName ) :
    shaderName_( shaderName )
{
}

void GrassInstancingShader::use() noexcept( false )
{
    BasicShader::use();
    textureAtlas_.setUpShaderForDiffuseMap( shader );
}

void GuiShader::use() noexcept( false )
{
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    BasicShader::use();
    glCullFace( GL_BACK );
}