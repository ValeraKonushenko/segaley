#include "Window.h"
#include "Thrower.h"
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>

Window* Window::oneObject_ = nullptr;

Window::Window( unsigned short width, unsigned short height ) noexcept( false ) :
	position_( 100, 100 ),
	window_( nullptr ),
	size_{ width, height },
	title_( "Application" )
{	
	init();
}

Window::~Window()
{
	close();
}

bool Window::isEmpty() const
{
	return !window_;
}

bool Window::isOpen() const noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to give not existing data" );

	return !glfwWindowShouldClose( window_ );
}

void Window::makeContextCurrent() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to set GLAD context to not existing window" );

	glfwMakeContextCurrent( window_ );
}

void Window::swapBuffers() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to give not existing data" );

	glfwSwapBuffers( window_ );
}

void Window::pollEvents() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to give not existing data" );

	glfwPollEvents();
}

void Window::close() const
{
	if ( !oneObject_ )
		return;

	glfwDestroyWindow( window_ );
	oneObject_ = nullptr;
}

void Window::clearScreen( GLbitfield mask, const RGBA& color ) const
{
	clearScreen( mask, Color::convertToF( color ) );
}

void Window::clearScreen( GLbitfield mask, const RGBAf& color ) const
{
	glClearColor( color.r, color.g, color.b, color.a );
	glClear( mask );
}

void Window::setSize( WindowSize size )
{
	size_ = size;
	glfwSetWindowSize( window_, size_.width, size_.height );
	glViewport( 0, 0, size_.width, size_.height );
}

WindowSize Window::getSize() const
{
	return size_;
}

void Window::setPosition( glm::ivec2 position )
{
	position_ = position;
	glfwSetWindowPos( window_, position_.x, position_.y );
}

glm::ivec2 Window::getPosition() const
{
	return position_;
}

void Window::setTitle( std::string title )
{
	title_ = title;
	glfwSetWindowTitle( window_, title_.c_str() );
}

std::string Window::getTitle() const
{
	return title_;
}

void Window::setMousePosEventCallback( mousePosFuncT func )
{
	mousePosEventCallback_ = func;
}

void Window::resetMousePosEventCallback()
{
	mousePosEventCallback_ = nullptr;
}

void Window::makeMousePosEvent( glm::vec2 pos )
{
	if( mousePosEventCallback_ )
		mousePosEventCallback_( pos.x, pos.y );
}

void Window::setMouseButtonEventCallback( mouseButtonFuncT func )
{
	mouseButtonEventCallback_ = func;
}

void Window::resetMouseButtonEventCallback()
{
	mouseButtonEventCallback_ = nullptr;
}

void Window::makeMouseButtonEvent( MouseKey button, KeyAction action, int mods )
{
	if( mouseButtonEventCallback_ )
		mouseButtonEventCallback_( button, action, mods );
}

void Window::setMouseScrollEventCallback( mouseScrollFuncT func )
{
	mouseScrollEventCallback_ = func;
}

void Window::resetMouseScrollEventCallback()
{
	mouseScrollEventCallback_ = nullptr;
}

void Window::makeMouseScrollEvent( float xOffset, float yOffset )
{
	if( mouseScrollEventCallback_ )
		mouseScrollEventCallback_( xOffset, yOffset );
}

HWND Window::getHwnd() noexcept( false )
{
	return glfwGetWin32Window( window_ );
}

GLFWwindow* Window::data() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to give not existing data" );

	return window_;
}

const GLFWwindow* Window::data() const noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "Impossible to give not existing data" );

	return window_;
}

void Window::checkCreateStatus() noexcept( false )
{
	if( !window_ )
	{
		MAKE_THROW( "Failed to create GLFW window" );
		glfwTerminate();
	}
}

void Window::initGlad() noexcept( false )
{
	if( !gladLoadGLLoader( reinterpret_cast< GLADloadproc >( glfwGetProcAddress ) ) )
		MAKE_THROW( "Failed to initialize GLAD" );
}

void Window::initGl() noexcept(false)
{
	if( !gladLoadGL() )
		MAKE_THROW( "Failed to initialize GL" );
}

void Window::init() noexcept( false )
{
	GLFW::instance( 3, 3 );

	window_ = glfwCreateWindow( size_.width, size_.height, title_.c_str(), nullptr, nullptr );
	checkCreateStatus();
	makeContextCurrent();
	initGlad();
	initGl();

	glfwSetCursorPosCallback( window_, MousePosCallback );
	glfwSetMouseButtonCallback( window_, MouseButtonCallback );
	glfwSetScrollCallback( window_, MouseWheelCallback );

	glViewport( 0, 0, size_.width, size_.height );

	setPosition( position_ );

	oneObject_ = this;
}

void MousePosCallback( GLFWwindow* window, double x, double y )
{
	Window::oneObject_->makeMousePosEvent( glm::vec2( static_cast< float >( x ), static_cast< float >( y ) ) );
}

void MouseButtonCallback( GLFWwindow* window, int button, int action, int mods )
{
	Window::oneObject_->makeMouseButtonEvent( static_cast< MouseKey >( button ), static_cast< KeyAction >( action ), mods );
}

void MouseWheelCallback( GLFWwindow* window, double xOffset, double yOffset )
{
	Window::oneObject_->makeMouseScrollEvent( static_cast< float >( xOffset ), static_cast< float >( yOffset ) );
}
