#include "ViewBoxSpace.h"
#include "utils.h"
#include "Thrower.h"

void ViewBoxSpace::setViewBoxSize( const ViewBox& size ) noexcept( false )
{
	auto supportedRes = utils::getAllSupportedWndSizes();
	WindowSize ws = { size.resolution.width, size.resolution.height };
	if( std::find( supportedRes.begin(), supportedRes.end(), ws ) == supportedRes.end() )
		MAKE_THROW( "Such resolution doesn't supported: " + std::to_string( size.resolution.width ) + "x" + std::to_string( size.resolution.height ) );
		
	viewBox_ = { size.resolution, size.deep };
}

ViewBox ViewBoxSpace::getSize() const
{
	return viewBox_;
}
