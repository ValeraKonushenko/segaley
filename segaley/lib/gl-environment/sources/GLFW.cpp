#include "GLFW.h"
#include <mutex>
#include <memory>

GLFW& GLFW::instance( int vMajor, int vMinor, int profile, int forwardCompat )
{
	static std::unique_ptr< GLFW > glfw;
	static std::mutex mutex;

	if( !glfw )
	{
		std::lock_guard lg( mutex );
		if( !glfw )
			glfw = std::unique_ptr< GLFW >( new GLFW( vMajor, vMinor, profile, forwardCompat ) );
	}

	return *glfw;
}

GLFW::GLFW( int vMajor, int vMinor, int profile, int forwardCompat )
{
	glfwInit();
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, vMajor );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, vMinor );
	glfwWindowHint( GLFW_OPENGL_PROFILE, profile );
	glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, forwardCompat );

	#ifdef __APPLE__
	glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, forwardCompat );
	#endif
}

GLFW::~GLFW()
{
	glfwTerminate();
}