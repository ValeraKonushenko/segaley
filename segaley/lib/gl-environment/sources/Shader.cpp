#include "Shader.h"
#include "Shader.h"
#include "Shader.h"
#include "Thrower.h"
#include "utils.h"

Shader::Shader( Type type ) :
    isApplied_( false ),
	type_( type ),
    rawSource_( nullptr ),
    id_( 0u )
{
}

Shader::Type Shader::getType() const
{
    return type_;
}

bool Shader::isApplied() const
{
    return isApplied_;
}

bool Shader::isEmpty() const
{
    return !source_.c_str();
}

void Shader::loadFromFile( std::string_view path ) noexcept( false )
{
    source_ = utils::getFileContent( path );
}

void Shader::apply() noexcept( false )
{
    rawSource_ = source_.c_str();
    switch( type_ )
    {
        case Type::Vertex: 
            id_ = glCreateShader( GL_VERTEX_SHADER ); 
            break;
        case Type::Fragment: 
            id_ = glCreateShader( GL_FRAGMENT_SHADER );
            break;
        case Type::Geometry: 
            id_ = glCreateShader( GL_GEOMETRY_SHADER );
            break;
        default: MAKE_THROW( "Ivalid shader type" ); break;
    }

    glShaderSource( id_, 1, &rawSource_, nullptr );
    glCompileShader( id_ );
    checkForError( GL_COMPILE_STATUS );
    isApplied_ = true;
}

void Shader::free()
{
    if ( !isEmpty() )
    {
        glDeleteShader( id_ );
        id_ = 0u;
    }
}

GLuint Shader::data()
{
    return id_;
}

Shader::~Shader()
{
    free();
}

void Shader::checkForError( GLenum param ) const noexcept( false )
{
    constexpr GLsizei logSize = 512;
    GLint status{};
    glGetShaderiv( id_, param, &status );
    if( !status )
    {
        char infoLog[ logSize ]{};
        glGetShaderInfoLog( id_, logSize, nullptr, infoLog );
        MAKE_THROW( std::string( "Shader error: " ) + infoLog );
    }
}
