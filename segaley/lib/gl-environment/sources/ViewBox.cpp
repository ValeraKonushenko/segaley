#include "ViewBox.h"

bool ViewBox::operator==( const ViewBox& other ) const
{
	return deep == other.deep && resolution == other.resolution;
}

bool ViewBox::operator!=( const ViewBox& other ) const
{
	return !( *this == other );
}