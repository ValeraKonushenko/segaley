#include "ShaderProgram.h"
#include "Thrower.h"
#include "glm/gtc/type_ptr.hpp"
#include "Gl.h"

ShaderProgram::ShaderProgram() :
    id_( 0 ),
    vShaderP_( nullptr ),
    fShaderP_( nullptr ),
    gShaderP_( nullptr )
{
}

ShaderProgram::~ShaderProgram()
{
    if ( !isEmpty() )
        free();
}

bool ShaderProgram::isEmpty() const
{
	return !id_;
}

bool ShaderProgram::isSetShader( Shader::Type type ) const
{
	switch( type )
    {
        case Shader::Type::Vertex: return vShaderP_;
        case Shader::Type::Fragment: return fShaderP_;
        case Shader::Type::Geometry: return gShaderP_;
        default: MAKE_THROW( "Ivalid shader type" ); break;
    }
    return false;
}

void ShaderProgram::attachShader( Shader& shader ) noexcept( false )
{
    if( isEmpty() )
        MAKE_THROW( "You can't attach shader to invalid program." );

    if( shader.isEmpty() )
        MAKE_THROW( "The shader can't be empty" );

    if( !shader.isApplied() )
        MAKE_THROW( "The shader wasn't been applied" );


    switch( shader.getType() )
    {
        case Shader::Type::Vertex: 
            vShaderP_ = &shader;
            glAttachShader( id_, vShaderP_->data() );
            break;
        case Shader::Type::Fragment:
            fShaderP_ = &shader;
            glAttachShader( id_, fShaderP_->data() );
            break;
        case Shader::Type::Geometry:
            gShaderP_ = &shader;
            glAttachShader( id_, gShaderP_->data() );
            break;
        default: 
            MAKE_THROW( "Ivalid shader type" ); 
            break;
    }

    checkForError( GL_ATTACHED_SHADERS );
}

void ShaderProgram::dettachShader( Shader::Type type ) noexcept( false )
{
    switch( type )
    {
        case Shader::Type::Vertex: 
            glDetachShader( id_, vShaderP_->data() );
            vShaderP_ = nullptr;
            break;
        case Shader::Type::Fragment:
            glDetachShader( id_, fShaderP_->data() );
            fShaderP_ = nullptr;
            break;
        case Shader::Type::Geometry:
            glDetachShader( id_, gShaderP_->data() );
            gShaderP_ = nullptr;
            break;
        default: 
            MAKE_THROW( "Ivalid shader type" ); 
            break;
    }
}

void ShaderProgram::link() noexcept( false )
{
    if( isEmpty() )
        MAKE_THROW( "You can't link invalid program." );

    glLinkProgram( id_ );

    checkForError( GL_LINK_STATUS );
}

void ShaderProgram::use() noexcept( false )
{
    if( isEmpty() )
        MAKE_THROW( "You can't use invalid shader program" );

    glUseProgram( id_ );
}

void ShaderProgram::create() noexcept( false )
{
    id_ = glCreateProgram();

    if( !id_ )
        MAKE_THROW( "Undefined error. The shader program can't be created" );
}

void ShaderProgram::free()
{
    if ( !isEmpty() )
    {
        glDeleteProgram( id_ );
        id_ = 0u;
    }
}

void ShaderProgram::vertexAttribPtr( GLuint nLayout, GLint size, GLsizei stride, GLuint offset ) const
{
    GLsizei localTotal = stride * sizeof( GL_FLOAT );
    auto localOffset = offset * sizeof( GL_FLOAT );

    Gl::vertexAttribPointer( nLayout,
                           size,
                           GL_FLOAT,
                           GL_FALSE,
                           localTotal,
                           reinterpret_cast< void* >( localOffset )
    );

    Gl::enableVertexAttribArray( nLayout );
}

void ShaderProgram::vertexAttribPtrByName( std::string_view shaderVarName, GLint size, GLsizei stride, GLuint offset ) noexcept( false )
{
    auto attrVar = getAttribLocation( shaderVarName );
    vertexAttribPtr( attrVar, size, stride, offset );
}

GLint ShaderProgram::getAttribLocation( std::string_view shaderVarName ) const noexcept( false )
{
    auto attrVar = glGetAttribLocation( id_, shaderVarName.data() );
    if( attrVar == -1 )
        MAKE_THROW( std::string( "No such variable '" ) + shaderVarName.data() + "' inside the shader" );

    return attrVar;
}

GLint ShaderProgram::getUniformLocation( std::string_view uniformName ) const noexcept( false )
{
    auto uniform = glGetUniformLocation( id_, uniformName.data() );
    //if( uniform == -1 )
    //    MAKE_THROW( std::string( "No such uniform '" ) + uniformName.data() + "' inside the shader" );

    return uniform;
}

void ShaderProgram::uniform( std::string_view uniformName, glm::fvec3 data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    glUniform3f( uniformLocation, data.x, data.y, data.z );
}

void ShaderProgram::uniform( std::string_view uniformName, glm::fvec2 data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    glUniform2f( uniformLocation, data.x, data.y );
}

void ShaderProgram::uniform( std::string_view uniformName, float data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    glUniform1f( uniformLocation, data );
}

void ShaderProgram::uniform( std::string_view uniformName, size_t data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    glUniform1ui( uniformLocation, static_cast< GLuint >( data ) );
}

void ShaderProgram::uniform( std::string_view uniformName, GLint data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    glUniform1i( uniformLocation, data );
}

void ShaderProgram::uniform( std::string_view uniformName, const glm::mat4& data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    uniform( uniformLocation, data );
}

void ShaderProgram::uniform( std::string_view uniformName, const RGBf& data ) noexcept( false )
{
    auto uniformLocation = getUniformLocation( uniformName );
    uniform( uniformLocation, data );
}

void ShaderProgram::uniform( GLint uniform, glm::fvec3 data )
{
    glUniform3f( uniform, data.x, data.y, data.z );
}

void ShaderProgram::uniform( GLint uniform, glm::fvec2 data )
{
    glUniform2f( uniform, data.x, data.y );
}

void ShaderProgram::uniform( GLint uniform, float data )
{
    glUniform1f( uniform, data );
}

void ShaderProgram::uniform( GLint uniform, size_t data )
{
    glUniform1ui( uniform, static_cast< GLuint >( data ) );
}

void ShaderProgram::uniform( GLint uniform, GLint data )
{
    glUniform1i( uniform, data );
}

void ShaderProgram::uniform( GLint uniform, const glm::mat4& data ) noexcept( false )
{
    glUniformMatrix4fv( uniform, 1, GL_FALSE, glm::value_ptr( data ) );
}

void ShaderProgram::uniform( GLint uniform, const RGBf& data ) noexcept( false )
{
    glUniform3f( uniform, data.r, data.g, data.b );
}

void ShaderProgram::checkForError( GLenum param ) noexcept( false )
{
    constexpr GLsizei logSize = 512;
    GLint status{};
    glGetProgramiv( id_, param, &status );
    if( !status )
    {
        char infoLog[ logSize ]{};
        glGetProgramInfoLog( id_, logSize, nullptr, infoLog );
        MAKE_THROW( std::string( "Program error: " ) + infoLog );
    }
}
