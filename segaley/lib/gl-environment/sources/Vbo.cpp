#include "Vbo.h"

#include "Thrower.h"
#include "Gl.h"

#include <iostream>

const GLuint Vbo::invalidId = 0u;

Vbo::Vbo( bool isGenerate, bool isBind ) noexcept( false )
{
    if ( isGenerate )
        generate();
    if ( isBind )
        bind();
}

Vbo::Vbo( const std::vector< float >& data, GLenum usage ) noexcept( false )
{
    generate();
    bind();
    setData( data, usage );
}

Vbo::Vbo( const std::vector< VertexData >& data, GLenum usage ) noexcept( false )
{
    generate();
    bind();
    setData( data, usage );
}

Vbo::Vbo( Vbo&& other ) noexcept
{
    *this = std::move( other );
}

Vbo& Vbo::operator=( Vbo&& other ) noexcept
{
    id_ = other.id_;
    isBind_ = other.isBind_;

    other.isBind_ = false;
    other.id_ = Vbo::invalidId;

    return *this;
}

Vbo::~Vbo()
{
    destroy();
}

void Vbo::generate() noexcept( false )
{
    if ( isEmpty() )
    {
        glGenBuffers( 1, &id_ );
        if ( isEmpty() )
            MAKE_THROW( "VBO can't be generate by not defined reasons" );
    }
}

void Vbo::bind() noexcept( false )
{
    if ( isEmpty() )
        MAKE_THROW( "VBO is empty. Try to regenerate and try again." );

    Gl::Vbo::bind( GL_ARRAY_BUFFER, id_ );
    isBind_ = true;
}

void Vbo::bind( GLuint id )
{
    Gl::Vbo::bind( GL_ARRAY_BUFFER, id );
}

void Vbo::unbind() noexcept( false )
{
    if ( !isEmpty() )
        MAKE_THROW( "VBO is empty. Try to regenerate and try again." );

    Gl::Vbo::reset( GL_ARRAY_BUFFER );
    isBind_ = false;
}

void Vbo::destroy()
{
    glDeleteBuffers( 1, &id_ );
    id_ = invalidId;
}

bool Vbo::isEmpty() const
{
    return id_ == invalidId;
}

bool Vbo::isBind() const
{
    return isBind_;
}

void Vbo::setData( const std::vector<float>& data, GLenum usage ) noexcept( false )
{
    if( isEmpty() )
        MAKE_THROW( "VBO is empty. Try to regenerate and try again." );

    if ( !isBind() )
        MAKE_THROW( "You try to put data to unbinded VBO. Bind it and try again" );

    Gl::Vbo::data( GL_ARRAY_BUFFER, sizeof( float ) * data.size(), data.data(), GL_STATIC_DRAW );
}

void Vbo::setData( const std::vector< VertexData >& data, GLenum usage ) noexcept( false )
{
    if ( isEmpty() )
        MAKE_THROW( "VBO is empty. Try to regenerate and try again." );

    if ( !isBind() )
        MAKE_THROW( "You try to put data to unbinded VBO. Bind it and try again" );

    Gl::Vbo::data( GL_ARRAY_BUFFER, VertexData::bsize() * data.size(), data.data(), GL_STATIC_DRAW );
}

