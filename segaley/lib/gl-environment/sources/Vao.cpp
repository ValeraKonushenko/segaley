#include "Vao.h"

#include "Thrower.h"
#include "Gl.h"

const GLuint Vao::invalidId = 0u;

Vao::Vao( bool isGenerate, bool isBind ) noexcept( false )
{
	if ( isGenerate )
		generate();
	if ( isBind )
		bind();
}

Vao::Vao( Vao&& other ) noexcept
{
	*this = std::move( other );
}

Vao& Vao::operator=( Vao&& other ) noexcept
{
	id_ = other.id_;
	isBind_ = other.isBind_;

	other.id_ = Vao::invalidId;
	other.isBind_ = false;

	return *this;
}

Vao::~Vao()
{
	destroy();
}

void Vao::generate() noexcept( false )
{
	if ( isEmpty() )
	{
		glGenVertexArrays( 1, &id_ );
		if ( isEmpty() )
			MAKE_THROW( "Vao can't be generate by not defined reasons" );
	}
}

void Vao::bind() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "The VAO is invalid" );

	Gl::Vao::bind( id_ );
	isBind_ = true;
}

void Vao::bind( GLuint id )
{
	Gl::Vao::bind( id );
}

void Vao::unbind() noexcept( false )
{
	if( isEmpty() )
		MAKE_THROW( "The VBO has unbinded" );

	Gl::Vao::reset();
	isBind_ = false;
}

void Vao::destroy()
{
	if ( !isEmpty() )
		glDeleteVertexArrays( 1, &id_ );

	id_ = invalidId;
	isBind_ = false;
}

bool Vao::isEmpty() const
{
	return id_ == invalidId;
}

bool Vao::isBind() const
{
	return isBind_;
}
