#include "Texture.h"

#include "Thrower.h"
#include "GLFW.h"

#include <string>

const GLuint Texture::invalidId = 0u;

Texture::MagFilter Texture::stringToMagFilter( std::string_view filter )
{
	if ( filter == "None" ) return MagFilter::None;
	else if ( filter == "Linear" ) return MagFilter::Linear;
	else if ( filter == "Nearest" ) return MagFilter::Nearest;
	
	return MagFilter::None;
}

Texture::MinFilter Texture::stringToMinFilter( std::string_view filter )
{
	if ( filter == "None" ) return MinFilter::None;
	else if ( filter == "Linear" ) return MinFilter::Linear;
	else if ( filter == "Nearest" ) return MinFilter::Nearest;
	else if ( filter == "NearestMipmapNearest" ) return MinFilter::NearestMipmapNearest;
	else if ( filter == "LinearMipmapNearest" ) return MinFilter::LinearMipmapNearest;
	else if ( filter == "NearestMipmapLinear" ) return MinFilter::NearestMipmapLinear;
	else if ( filter == "LinearMipmapLinear" ) return MinFilter::LinearMipmapLinear;

	return MinFilter::None;
}

void Texture::setMinFilter( MinFilter filter, Target target ) noexcept( false )
{
	if ( filter == MinFilter::None )
		MAKE_THROW( "Min filter is none. Try to use correct filter value." );
	if ( target == Target::None )
		MAKE_THROW( "Target is none. Try to use correct target value." );

	glTexParameteri( static_cast< GLenum >( target ), GL_TEXTURE_MIN_FILTER, static_cast< GLint >( filter ) );
}

void Texture::setMagFilter( MagFilter filter, Target target ) noexcept( false )
{
	if ( filter == MagFilter::None )
		MAKE_THROW( "Mag filter is none. Try to use correct filter value." );
	if ( target == Target::None )
		MAKE_THROW( "Target is none. Try to use correct target value." );

	glTexParameteri( static_cast< GLenum >( target ), GL_TEXTURE_MAG_FILTER, static_cast< GLint >( filter ));
}

void Texture::setWrapS( Wrap wrap, Target target ) noexcept( false )
{
	if ( wrap == Wrap::None )
		MAKE_THROW( "Wrap is none. Try to use correct wrap value." );
	if ( target == Target::None )
		MAKE_THROW( "Target is none. Try to use correct target value." );

	glTexParameteri( static_cast< GLenum >( target ), GL_TEXTURE_WRAP_S, static_cast< GLint >( wrap ) );
}

void Texture::setWrapT( Wrap wrap, Target target ) noexcept( false )
{
	if ( wrap == Wrap::None )
		MAKE_THROW( "Wrap is none. Try to use correct wrap value." );
	if ( target == Target::None )
		MAKE_THROW( "Target is none. Try to use correct target value." );

	glTexParameteri( static_cast< GLenum >( target ), GL_TEXTURE_WRAP_T, static_cast< GLint >( wrap ) );
}

void Texture::setWrapR( Wrap wrap, Target target ) noexcept( false )
{
	if ( wrap == Wrap::None )
		MAKE_THROW( "Wrap is none. Try to use correct wrap value." );
	if ( target == Target::None )
		MAKE_THROW( "Target is none. Try to use correct target value." );

	glTexParameteri( static_cast< GLenum >( target ), GL_TEXTURE_WRAP_R, static_cast< GLint >( wrap ) );
}

Texture::Texture( Target target ) noexcept( false ) :
	target_( target )
{
	glGenTextures( 1, &id_ );
	bind();
}

void Texture::active( GLenum num ) noexcept( false )
{
	static size_t maxCountActiveTextures = Texture::getMaxCountActiveTextures();
	using namespace std::string_literals;
	if ( num >= maxCountActiveTextures )
		MAKE_THROW( "Your device does not support such count of textures. Impossible to active "s + std::to_string( num ) + "th texture." );

	glActiveTexture( GL_TEXTURE0 + num );
}

size_t Texture::getMaxCountActiveTextures() noexcept( false )
{
	int total_units;
	glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &total_units );
	return static_cast< size_t >( total_units );
}

Texture::~Texture()
{
	if ( isDestroyAtEnd_ )
		destroy();
}

void Texture::bind() const noexcept( false )
{
	if( id_ == invalidId )
		MAKE_THROW( "The Texture's id(handle) is invalid" );

	glBindTexture( static_cast< GLenum >( target_ ), id_);
}

void Texture::unbind() const noexcept( false )
{
	if( id_ == invalidId )
		MAKE_THROW( "The Texture's id(handle) had unbinded" );

	glBindTexture( static_cast< GLenum >( target_ ), 0 );
}

void Texture::destroy()
{
	glDeleteTextures( 1, &id_ );
	id_ = Texture::invalidId;
	target_ = Target::None;
	isDestroyAtEnd_ = true;
}

bool Texture::isEmpty() const
{
	return id_ == invalidId;
}

GLuint Texture::data()
{
	return id_;
}

bool Texture::isDestroyAtEnd() const
{
	return isDestroyAtEnd_;
}

void Texture::setIsDestroyAtEnd( bool is )
{
	isDestroyAtEnd_ = is;
}