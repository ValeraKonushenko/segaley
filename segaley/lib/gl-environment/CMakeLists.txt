
add_library(
	gl-environment STATIC
	include/Gl.h
	include/GLFW.h
	include/Shader.h
	include/ShaderProgram.h
	include/Shaders.h
	include/Texture.h
	include/Vao.h
	include/Vbo.h
	include/ViewBox.h
	include/ViewBoxSpace.h
	include/Window.h
	include/WindowSize.h
	sources/Gl.cpp
	sources/GLFW.cpp
	sources/Shader.cpp
	sources/ShaderProgram.cpp
	sources/Shaders.cpp
	sources/Texture.cpp
	sources/Vao.cpp
	sources/Vbo.cpp
	sources/ViewBox.cpp
	sources/ViewBoxSpace.cpp
	sources/Window.cpp
	sources/WindowSize.cpp
)

target_link_libraries(
	gl-environment PUBLIC
	exceptions
)