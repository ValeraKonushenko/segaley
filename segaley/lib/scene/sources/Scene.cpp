#include "Scene.h"

#include "ObjLoader.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <random>

void Scene::draw( ShaderPack& shaderPack, glm::mat4& view, glm::mat4& proj, glm::vec3 cameraPosition ) noexcept( false )
{
    skybox_.draw( { shaderPack.cubeMap, view, proj } );

    shaderPack.main.use();
    cube_.draw( { shaderPack.main, view, proj } );
    rectangle_.draw( { shaderPack.main, view, proj } );
    obj_.draw( { shaderPack.main, view, proj } );

    shaderPack.grassInstancing.use();
    size_t count = 100u;
    if ( static bool isInit = false; !isInit )
    {       
        spawnGrass( bush_, shaderPack.grassInstancing, count, 10, { 100, 0 }, { 500, 500 }, 100, 0.9, 1.0 );
        isInit = true;
    }
    bush_.animation( clock() );
    bush_.instanceDraw( { shaderPack.grassInstancing, view, proj }, count );
}

void Scene::prepare() noexcept( false )
{
    skybox_.prepare();

    cube_.prepare();
    FFRect r;
    r.position = { 0.f, 3000.f };
    r.size = { 1000.f, 1000.f };
    cube_.setTextureRect( r );
    cube_.setSize( { 300.f, 300.f, 300.f } );

    rectangle_.prepare();
    rectangle_.move( { 1000, 0, 1000 } );
    rectangle_.setTextureRect( FFRect( { 0.f, 3000.f }, { 1000.f, 1000.f } ) );

    bush_.prepare();
    bush_.setTextureRect( { { 0.f, 1000.f }, { 1000.f, 1000.f } } );
    bush_.setSize( { 50, 50 } );
    bush_.setForceRebuild();

    obj_.load( "assets/raw-models/alien.obj" );
    obj_.prepare();
    for ( auto& oneObj : obj_ )
    {
        oneObj.move( { -1000, 0, 0 } );
        oneObj.setUv( { 0,3000 } );
        oneObj.setTextureSize( { 1000, 1000 } );
        oneObj.material.setMaterial( "chrome" );
    }
}

void Scene::spawnGrass( figure::Bush& grass,
                        BasicShader& shader,
                        size_t count, 
                        int grassDensity, 
                        glm::ivec2 from,
                        glm::ivec2 fieldSize,
                        float y,
                        float minGrassScaleK,
                        float maxGrassScaleK ) noexcept( false )
{
    grass.setPosition( { from.x, y, from.y } );
    
    //TODO: add checking uniform the array's size in the .frag shader
    std::default_random_engine randomEnginge;
    std::uniform_real_distribution< float > scaleRand( minGrassScaleK, maxGrassScaleK );
    std::uniform_int_distribution< int > xRand( from.x, from.x + fieldSize.x / grassDensity );
    std::uniform_int_distribution< int > zRand( from.y, from.y + fieldSize.y / grassDensity );
    std::uniform_real_distribution< float > rotateRand( 0, static_cast< float >( M_PI ) );

    size_t index = 0u;
    for ( size_t i = 0; i < count; i++ )
    {
        auto model = glm::mat4( 1.f );

        float x = static_cast< float >( xRand( randomEnginge ) ) * static_cast< float >( grassDensity );
        float z = static_cast< float >( zRand( randomEnginge ) ) * static_cast< float >( grassDensity );
        model = glm::translate( model, glm::vec3( x, 0.f, z ) );

        float rotate = rotateRand( randomEnginge );
        model = glm::rotate( model, rotate, { 0.f, 1.f, 0.f } );

        model = glm::scale( model, glm::vec3( scaleRand( randomEnginge ) ) );
        shader.shader.uniform( "uOffsets[" + std::to_string( index ) + "]", model );
        index++;
    }
}
