#pragma once

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Skybox.h"
#include "Shaders.h"
#include "Cube.h"
#include "Bush.h"
#include "Lighting.h"
#include "ObjectContainer.h"
#include "ObjectCollection.h"

#include <vector>

class Scene final
{
public:
	Scene() = default;

	Scene( Scene && ) = delete;
	Scene& operator=( Scene&& ) = delete;

	~Scene() = default;

	void draw( ShaderPack& shaderPack, glm::mat4& view, glm::mat4& proj, glm::vec3 cameraPosition ) noexcept( false );
	void prepare() noexcept( false );

	void spawnGrass( figure::Bush& grass,
					 BasicShader& shaderPack,
					 size_t count, 
					 int grassDensity, 
					 glm::ivec2 from, 
					 glm::ivec2 fieldSize,
					 float y,
					 float minGrassScaleK,
					 float maxGrassScaleK ) noexcept( false );

private:	
	Skybox skybox_;

	ObjectCollection obj_;
	figure::Cube cube_;
	figure::Rectangle rectangle_;
	figure::Bush bush_;
};