#version 440 core

//Out
out vec4 oFragColor;

//In
in vec2 ioTextRectSize;
in vec3 ioNormal;
in vec2 ioUv;
in vec3 ioFragColorPos;

//Textures
uniform sampler2D textureAtlas;

vec2 calcUv()
{
    return vec2( ioTextRectSize.x + ioUv.x, ioTextRectSize.y + ioUv.y );
}

void main()
{
    vec2 uv = calcUv();

    vec4 color = texture( textureAtlas, uv );
    if ( color.a < 0.5 )
        discard;
    oFragColor = color;
}