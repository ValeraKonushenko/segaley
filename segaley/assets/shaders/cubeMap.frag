#version 440 core

struct Material
{
    float specularPowder;
    float specularIntensity;
    vec3 specularColor;
};

//Out
out vec4 oFragColor;

in vec3 ioTexCoords;

uniform samplerCube uSkybox;
uniform Material uMaterial;

void main()
{    
    oFragColor = texture( uSkybox, vec3( 1.f, -1.f, 1.f ) * ioTexCoords );
}