#version 440 core
layout ( location = 0 ) in vec3 aPos;
layout ( location = 1 ) in vec3 aNormal;
layout ( location = 2 ) in vec2 aTextRectSize;
layout ( location = 3 ) in vec2 aUv;

uniform vec2 uAtlasSize;
uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

out vec2 ioUv;
out vec2 ioTextRectSize;
out vec3 ioNormal;
out vec3 ioFragColorPos;

uniform mat4 uOffsets[ 100 ];

void main()
{
    ioUv = aUv / uAtlasSize;                     //calculate texture position at the atlas by according to atlas size
    ioTextRectSize = aTextRectSize / uAtlasSize; //calculate texture size by according to atlas size
    
    ioNormal = mat3( transpose( inverse( uModelMatrix ) ) ) * aNormal;
    ioFragColorPos = vec3( uModelMatrix * vec4( aPos, 1.0 ) );

    mat4 modelMatrix = uModelMatrix * uOffsets[ gl_InstanceID ];
    gl_Position = ( uProjectionMatrix * uViewMatrix * modelMatrix ) * vec4( aPos , 1.0 );
}