#version 330 core
out vec4 color;

in vec2 ioTexCoords;

uniform sampler2D uText;
uniform vec3 uTextColor;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(uText, ioTexCoords).r);
    color = vec4(uTextColor, 1.0) * sampled;
}  